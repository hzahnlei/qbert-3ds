#include "colored_vertex_renderer.hpp"

#include "colored_vertex_shader_shbin.h"

namespace qbert
{

        Colored_Vertex_Renderer::Colored_Vertex_Renderer(C3D_Mtx &model_view, C3D_RenderTarget *target_left,
                                                         C3D_Mtx &projection_matrix_left,
                                                         C3D_RenderTarget *target_right,
                                                         C3D_Mtx &projection_matrix_right,
                                                         const Vertex_Count max_vertex_count)
            : Abstract_Vertex_Renderer{
                  model_view,      target_left, projection_matrix_left, target_right, projection_matrix_right,
                  max_vertex_count}
        {
                initialize_shader();
                allocate_vertex_buffer();
        }

        //*********************************************************************
        // Implementation details
        //*********************************************************************

        auto Colored_Vertex_Renderer::load_vertex_shader() -> void
        {
                m_shader_dvlb = DVLB_ParseFile((u32 *)colored_vertex_shader_shbin, colored_vertex_shader_shbin_size);
                shaderProgramInit(&m_shader_program);
                shaderProgramSetVsh(&m_shader_program, &m_shader_dvlb->DVLE[0]);
        }

        auto Colored_Vertex_Renderer::set_texture_environment() -> void
        {
                auto env{C3D_GetTexEnv(0)};
                C3D_TexEnvInit(env);
                C3D_TexEnvSrc(env, C3D_Both, GPU_PRIMARY_COLOR, GPU_PRIMARY_COLOR, GPU_PRIMARY_COLOR);
                C3D_TexEnvFunc(env, C3D_Both, GPU_REPLACE);
        }

        auto Colored_Vertex_Renderer::set_shader_attribute_info() -> void
        {
                m_shader_attribute_info = C3D_GetAttrInfo();
                AttrInfo_Init(m_shader_attribute_info);
                AttrInfo_AddLoader(m_shader_attribute_info, 0, GPU_FLOAT, 3); // v0=position (x,y,z)
                AttrInfo_AddLoader(m_shader_attribute_info, 1, GPU_FLOAT, 4); // v1=color (r,g,b,a)
        }

} // namespace qbert
