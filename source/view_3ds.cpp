#include "view_3ds.hpp"

#include "color.hpp"
#include "screen_dimensions.hpp"
#include "title_screen.h"
#include "transformations.hpp"
#include <3ds.h>
#include <cstring>

namespace qbert
{

        using std::make_unique;
        using std::memcpy;

        constexpr auto DISPLAY_TRANSFER_FLAGS{GX_TRANSFER_FLIP_VERT(0) | GX_TRANSFER_OUT_TILED(0) |
                                              GX_TRANSFER_RAW_COPY(0) | GX_TRANSFER_IN_FORMAT(GX_TRANSFER_FMT_RGBA8) |
                                              GX_TRANSFER_OUT_FORMAT(GX_TRANSFER_FMT_RGB8) |
                                              GX_TRANSFER_SCALING(GX_TRANSFER_SCALE_NO)};

        View_3DS::View_3DS()
        {
                initialize_graphics_unit();
                initialize_render_targets();
                set_perspective(0.0f, 0.0f, 0.0f);
                adjust_interocular_distance(1.0f);

                m_color_painter = make_unique<Colored_Vertex_Renderer>(
                    m_model_view, m_target_left, m_projection_matrix_left, m_target_right, m_projection_matrix_right);

                m_texture_painter = make_unique<Textured_Vertex_Renderer>(
                    m_model_view, m_target_left, m_projection_matrix_left, m_target_right, m_projection_matrix_right);

                m_overlay_painter = make_unique<Textured_Colored_Vertex_Renderer>(
                    m_overlay_view, m_target_left, m_overlay_projection_matrix_left, m_target_right,
                    m_overlay_projection_matrix_right);
        }

        View_3DS::~View_3DS()
        {
                deinitialize_render_targets();
                deinitialize_graphics_unit();
        }

        auto View_3DS::set_perspective(const Degrees angle_x, const Degrees angle_y, const Degrees angle_z) -> void
        {
                Mtx_Identity(&m_model_view);
                Mtx_Translate(&m_model_view, 0.0, 0.0, -4.0, true);
                Mtx_RotateX(&m_model_view, radians(angle_x), true);
                Mtx_RotateY(&m_model_view, radians(angle_y), true);
                Mtx_RotateZ(&m_model_view, radians(angle_z), true);
                Mtx_Identity(&m_overlay_view);
        }

        void mtx_ortho_tilt(C3D_Mtx *mtx, const float left, const float right, const float bottom, const float top,
                            const float near, const float far, const float iod)
        {
                Mtx_Zeros(mtx);

                mtx->r[0].y = 2.0f / (top - bottom);
                mtx->r[0].w = (bottom + top) / (bottom - top);

                mtx->r[1].x = 2.0f / (left - right);
                mtx->r[1].z = iod * (far + near) / (near - far) - iod;
                mtx->r[1].w = (left + right) / (right - left);

                mtx->r[2].z = 1.0f / (far - near);
                mtx->r[2].w = 0.5f * (near + far) / (near - far) - 0.5f;

                mtx->r[3].w = 1.0f;
        }

        auto View_3DS::adjust_interocular_distance(const float interocular_distance) -> void
        {
                const auto effective_iod{interocular_distance / 25.0f};
                Mtx_PerspStereoTilt(&m_projection_matrix_left, radians(40.0_deg), C3D_AspectRatioTop, 0.01f, 1000.0f,
                                    effective_iod, 2.0f, false);
                Mtx_PerspStereoTilt(&m_projection_matrix_right, radians(40.0_deg), C3D_AspectRatioTop, 0.01f, 1000.0f,
                                    -effective_iod, 2.0f, false);
                // Instead of using Citro3D's projection, I am using my own, that allows depth.
                // Mtx_OrthoTilt(&m_overlay_projection_matrix_left, 0.0f, 400.0f, 0.0f, 240.0f, 100.0f, -100.0f, true);
                // Mtx_OrthoTilt(&m_overlay_projection_matrix_right, 0.0f, 400.0f, 0.0f, 240.0f, 100.0f, -100.0f, true);
                mtx_ortho_tilt(&m_overlay_projection_matrix_left, 0.0f, static_cast<float>(screen::top::WIDTH), 0.0f,
                               static_cast<float>(screen::top::HEIGHT), 2.0f, -2.0f, 0.0f);
                mtx_ortho_tilt(&m_overlay_projection_matrix_right, 0.0f, static_cast<float>(screen::top::WIDTH), 0.0f,
                               static_cast<float>(screen::top::HEIGHT), 2.0f, -2.0f,
                               -(interocular_distance / (screen::top::WIDTH / 2.0f)));
        }

        auto View_3DS::render() -> void
        {
                C3D_FrameBegin(C3D_FRAME_SYNCDRAW);
                {
                        clear_render_targets();
                        draw_color_plane();
                        draw_texture_plane();
                        draw_overlay_plane();
                }
                C3D_FrameEnd(0);
                // gspWaitForVBlank();
        }

        static circlePosition circle_pos;

        auto View_3DS::player_input() const noexcept -> Player_Input
        {
                hidScanInput();
                hidCircleRead(&circle_pos);
                return Player_Input{.pressed = static_cast<Buttons>(hidKeysDown()),
                                    .held = static_cast<Buttons>(hidKeysHeld()),
                                    .released = static_cast<Buttons>(hidKeysUp()),
                                    .c_pad =
                                        {
                                            .dx = circle_pos.dx,
                                            .dy = circle_pos.dy,
                                        },
                                    .stereoscopic_3d_slider = osGet3DSliderState()};
        }

        //*********************************************************************
        // Implementation details
        //*********************************************************************

        //---- Construction ----

        auto draw_title_screen()
        {
                auto frame_buffer{gfxGetFramebuffer(GFX_BOTTOM, GFX_LEFT, nullptr, nullptr)};
                memcpy(frame_buffer, title_screen_bgr, title_screen_bgr_size);
        }

        auto View_3DS::initialize_graphics_unit() -> void
        {
                gfxInitDefault();
                gfxSet3D(true);
                C3D_Init(C3D_DEFAULT_CMDBUF_SIZE);
                gfxSetDoubleBuffering(GFX_BOTTOM, false);
                // Vertices in front of other vertices are hiding those vertices in the background.
                C3D_DepthTest(true, GPU_GEQUAL, GPU_WRITE_ALL);
                // Make vertices in the back shine through "holes" of vertices in the front.
                C3D_AlphaTest(true, GPU_GEQUAL, GPU_WRITE_ALL);
                draw_title_screen();
        }

        auto View_3DS::initialize_render_targets() -> void
        {
                m_target_left = C3D_RenderTargetCreate(screen::top::HEIGHT, screen::top::WIDTH, GPU_RB_RGBA8,
                                                       GPU_RB_DEPTH24_STENCIL8);
                C3D_RenderTargetSetOutput(m_target_left, GFX_TOP, GFX_LEFT, DISPLAY_TRANSFER_FLAGS);
                m_target_right = C3D_RenderTargetCreate(screen::top::HEIGHT, screen::top::WIDTH, GPU_RB_RGBA8,
                                                        GPU_RB_DEPTH24_STENCIL8);
                C3D_RenderTargetSetOutput(m_target_right, GFX_TOP, GFX_RIGHT, DISPLAY_TRANSFER_FLAGS);
        }

        //---- Deconstruction ----

        auto View_3DS::deinitialize_render_targets() -> void
        {
                C3D_RenderTargetDelete(m_target_left);
                C3D_RenderTargetDelete(m_target_right);
        }

        auto View_3DS::deinitialize_graphics_unit() -> void
        {
                C3D_Fini();
                gfxExit();
        }

} // namespace qbert