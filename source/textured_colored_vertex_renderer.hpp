#pragma once

#include "abstract_vertex_renderer.hpp"
#include "sprite_textures_t3x.h"
#include "textured_colored_vertex.hpp"
#include <tex3ds.h>

namespace qbert
{

        using std::size_t;

        class Textured_Colored_Vertex_Renderer final : public Abstract_Vertex_Renderer<Textured_Colored_Vertex>
        {
            private:
                C3D_Tex texture_map;

            public:
                Textured_Colored_Vertex_Renderer(C3D_Mtx &model_view, C3D_RenderTarget *target_left,
                                                 C3D_Mtx &projection_matrix_left, C3D_RenderTarget *target_right,
                                                 C3D_Mtx &projection_matrix_right,
                                                 const Vertex_Count max_vertex_count = DEFAULT_MAX_VERTEX_COUNT);

            private:
                //---- Construction ----
                auto load_vertex_shader() -> void override;
                auto load_texture(const void *data, const size_t size) -> void;
                //---- Rendering ----
                auto set_shader_attribute_info() -> void override;
                auto set_texture_environment() -> void override;
        };

} // namespace qbert