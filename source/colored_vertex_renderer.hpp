#pragma once

#include "abstract_vertex_renderer.hpp"
#include "colored_vertex.hpp"

namespace qbert
{

        class Colored_Vertex_Renderer final : public Abstract_Vertex_Renderer<Colored_Vertex>
        {

            public:
                Colored_Vertex_Renderer(C3D_Mtx &model_view, C3D_RenderTarget *target_left,
                                        C3D_Mtx &projection_matrix_left, C3D_RenderTarget *target_right,
                                        C3D_Mtx &projection_matrix_right,
                                        const Vertex_Count max_vertex_count = DEFAULT_MAX_VERTEX_COUNT);

            private:
                auto load_vertex_shader() -> void override;
                auto set_texture_environment() -> void override;
                auto set_shader_attribute_info() -> void override;
        };

} // namespace qbert