#pragma once

#include "colored_vertex_renderer.hpp"
#include "textured_colored_vertex_renderer.hpp"
#include "textured_vertex_renderer.hpp"
#include "view.hpp"
#include <citro3d.h>
#include <memory>

namespace qbert
{

        using std::unique_ptr;

        constexpr auto CLEAR_COLOR{0x000000FF};

        class View_3DS final : public View
        {
            private:
                C3D_Mtx m_projection_matrix_left;
                C3D_Mtx m_projection_matrix_right;
                C3D_Mtx m_overlay_projection_matrix_left;
                C3D_Mtx m_overlay_projection_matrix_right;
                C3D_RenderTarget *m_target_left;
                C3D_RenderTarget *m_target_right;
                C3D_Mtx m_model_view;
                C3D_Mtx m_overlay_view;
                unique_ptr<Colored_Vertex_Renderer> m_color_painter;
                unique_ptr<Textured_Vertex_Renderer> m_texture_painter;
                unique_ptr<Textured_Colored_Vertex_Renderer> m_overlay_painter;

            public:
                View_3DS();
                ~View_3DS();

            public:
                auto set_perspective(const Degrees angle_x, const Degrees angle_y, const Degrees angle_z)
                    -> void override;
                auto adjust_interocular_distance(const float) -> void override;

                inline auto colored_vertices() const noexcept -> Vertex_Pool<Colored_Vertex> & override;
                inline auto textured_vertices() const noexcept -> Vertex_Pool<Textured_Vertex> & override;
                inline auto overlay_vertices() const noexcept -> Vertex_Pool<Textured_Colored_Vertex> & override;

                auto render() -> void override;

                inline auto main_loop_should_run() const noexcept -> bool override;
                auto player_input() const noexcept -> Player_Input override;

            private:
                //---- Construction ----
                auto initialize_graphics_unit() -> void;
                auto initialize_render_targets() -> void;
                //---- Deconstruction ----
                auto deinitialize_render_targets() -> void;
                auto deinitialize_graphics_unit() -> void;
                //---- Rendering ----
                inline auto clear_render_targets() -> void;
                inline auto draw_color_plane() -> void;
                inline auto draw_texture_plane() -> void;
                inline auto draw_overlay_plane() -> void;
        };

        auto View_3DS::colored_vertices() const noexcept -> Vertex_Pool<Colored_Vertex> &
        {
                return *m_color_painter;
        }

        auto View_3DS::textured_vertices() const noexcept -> Vertex_Pool<Textured_Vertex> &
        {
                return *m_texture_painter;
        }

        auto View_3DS::overlay_vertices() const noexcept -> Vertex_Pool<Textured_Colored_Vertex> &
        {
                return *m_overlay_painter;
        }

        //---- Rendering ----

        auto View_3DS::clear_render_targets() -> void
        {
                C3D_RenderTargetClear(m_target_left, C3D_CLEAR_ALL, CLEAR_COLOR, 0);
                C3D_RenderTargetClear(m_target_right, C3D_CLEAR_ALL, CLEAR_COLOR, 0);
        }

        auto View_3DS::draw_color_plane() -> void
        {
                C3D_FrameDrawOn(m_target_left);
                m_color_painter->render_to_left_target();
                C3D_FrameDrawOn(m_target_right);
                m_color_painter->render_to_right_target();
        }

        auto View_3DS::draw_texture_plane() -> void
        {
                C3D_FrameDrawOn(m_target_left);
                m_texture_painter->render_to_left_target();
                C3D_FrameDrawOn(m_target_right);
                m_texture_painter->render_to_right_target();
        }

        auto View_3DS::draw_overlay_plane() -> void
        {
                C3D_FrameDrawOn(m_target_left);
                m_overlay_painter->render_to_left_target();
                C3D_FrameDrawOn(m_target_right);
                m_overlay_painter->render_to_right_target();
        }

        //---- Player input ----

        auto View_3DS::main_loop_should_run() const noexcept -> bool
        {
                return aptMainLoop();
        }

} // namespace qbert