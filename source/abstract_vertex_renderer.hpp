#pragma once

#include "vertex.hpp"
#include "vertex_pool.hpp"
#include <citro3d.h>
#include <stdexcept>

namespace qbert
{

        using std::runtime_error;

        template <typename VERTEX> class Abstract_Vertex_Renderer : public Vertex_Pool<VERTEX>
        {

            public:
                static constexpr Vertex_Count DEFAULT_MAX_VERTEX_COUNT{1024};

            private:
                C3D_Mtx &m_model_view;
                C3D_RenderTarget *m_target_left = nullptr;
                C3D_Mtx &m_projection_matrix_left;
                C3D_RenderTarget *m_target_right = nullptr;
                C3D_Mtx &m_projection_matrix_right;

            protected:
                DVLB_s *m_shader_dvlb = nullptr;
                shaderProgram_s m_shader_program;
                C3D_AttrInfo *m_shader_attribute_info = nullptr;

            private:
                int m_uloc_projection = -1;
                int m_uloc_model_view = -1;

                Vertex_Count m_vertex_count = 0;
                const Vertex_Count m_max_vertex_count;
                VERTEX *m_vertex_buffer = nullptr;
                C3D_BufInfo *m_vertex_buffer_info = nullptr;

            public:
                Abstract_Vertex_Renderer(C3D_Mtx &model_view, C3D_RenderTarget *target_left,
                                         C3D_Mtx &projection_matrix_left, C3D_RenderTarget *target_right,
                                         C3D_Mtx &projection_matrix_right,
                                         const Vertex_Count max_vertex_count = DEFAULT_MAX_VERTEX_COUNT)
                    : m_model_view{model_view}, m_target_left{target_left},
                      m_projection_matrix_left{projection_matrix_left}, m_target_right{target_right},
                      m_projection_matrix_right{projection_matrix_right}, m_max_vertex_count{max_vertex_count}
                {
                        // Descendant of this class have to do this (these methods are pure virtual and cannot be called
                        // from within the constructor): initialize_shader(); allocate_vertex_buffer();
                }

                virtual ~Abstract_Vertex_Renderer() override
                {
                        release_vertex_buffer();
                        deinitialize_shader();
                }

            public:
                auto acquire_vertices(const Vertex_Count vertex_count) -> VERTEX * override
                {
                        if (vertices_are_available(vertex_count))
                        {
                                const auto vertices{&m_vertex_buffer[m_vertex_count]};
                                m_vertex_count += vertex_count;
                                return vertices;
                        }
                        else
                        {
                                throw runtime_error{"No more vertices available."};
                        }
                }

                auto release_all_vertices() -> void override
                {
                        m_vertex_count = 0;
                }

                auto render_to_left_target() -> void
                {
                        if (m_vertex_count > 0)
                        {
                                bind_scene();
                                update_uniforms_for_left_target();
                                draw_vertex_buffer_object();
                        }
                }

                auto render_to_right_target() -> void
                {
                        if (m_vertex_count > 0)
                        {
                                update_uniforms_for_right_target();
                                draw_vertex_buffer_object();
                        }
                }

            protected:
                //---- Construction ----

                auto initialize_shader() -> void
                {
                        load_vertex_shader();
                        determine_location_of_uniforms();
                }

            private:
                virtual auto load_vertex_shader() -> void = 0;

                auto determine_location_of_uniforms() -> void
                {
                        m_uloc_projection =
                            shaderInstanceGetUniformLocation(m_shader_program.vertexShader, "projection");
                        m_uloc_model_view =
                            shaderInstanceGetUniformLocation(m_shader_program.vertexShader, "transformation");
                }

            protected:
                auto allocate_vertex_buffer() -> void
                {
                        m_vertex_buffer = static_cast<VERTEX *>(linearAlloc(VERTEX::byte_count(m_max_vertex_count)));
                }

            private:
                //---- Vertex acquisition ---

                inline auto vertices_are_available(const Vertex_Count vertex_count) const noexcept -> bool
                {
                        return m_vertex_count + vertex_count < DEFAULT_MAX_VERTEX_COUNT;
                }

                //---- Deconstruction ----

                auto release_vertex_buffer() -> void
                {
                        linearFree(m_vertex_buffer);
                }

                auto deinitialize_shader() -> void
                {
                        shaderProgramFree(&m_shader_program);
                        DVLB_Free(m_shader_dvlb);
                }

                //---- Rendering ----

                virtual auto set_shader_attribute_info() -> void = 0;

                auto set_vertex_buffer_info() -> void
                {
                        m_vertex_buffer_info = C3D_GetBufInfo();
                        BufInfo_Init(m_vertex_buffer_info);
                        BufInfo_Add(m_vertex_buffer_info, m_vertex_buffer, VERTEX::byte_count(),
                                    VERTEX::component_count(), 0x210);
                }

                virtual auto set_texture_environment() -> void = 0;

                auto bind_scene() -> void
                {
                        C3D_BindProgram(&m_shader_program);
                        set_shader_attribute_info();
                        set_vertex_buffer_info();
                        set_texture_environment();
                }

                inline auto update_uniforms_for_left_target() -> void
                {
                        C3D_FVUnifMtx4x4(GPU_VERTEX_SHADER, m_uloc_projection, &m_projection_matrix_left);
                        C3D_FVUnifMtx4x4(GPU_VERTEX_SHADER, m_uloc_model_view, &m_model_view);
                }

                static constexpr auto FROM_FIRST_VERTEX{0U};

                inline auto draw_vertex_buffer_object() -> void
                {
                        C3D_DrawArrays(GPU_TRIANGLES, FROM_FIRST_VERTEX, m_vertex_count);
                }

                inline auto update_uniforms_for_right_target() -> void
                {
                        C3D_FVUnifMtx4x4(GPU_VERTEX_SHADER, m_uloc_projection, &m_projection_matrix_right);
                        C3D_FVUnifMtx4x4(GPU_VERTEX_SHADER, m_uloc_model_view, &m_model_view);
                }
        };

} // namespace qbert