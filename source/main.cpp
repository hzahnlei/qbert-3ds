/*
 * 3DS Template
 */

#include "controller.hpp"
#include "disc.hpp"
#include "game_model_delegating_impl.hpp"
#include "triangular_playfield.hpp"
#include "view_3ds.hpp"
#include <3ds.h>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

using std::exception;
using std::make_unique;
using std::string;
using std::vector;

auto open_console()
{
        gfxInitDefault();
        consoleInit(GFX_BOTTOM, nullptr);
}

auto wait_for_select_pressed()
{
        printf("\nPress SELECT to abort.\n");
        while (true)
        {
                hidScanInput();
                if (hidKeysDown() & KEY_SELECT)
                {
                        break;
                }
        }
}

auto report_error(char const *const message = "unknown")
{
        open_console();
        printf("An error has occurred: %s\n", message);
        wait_for_select_pressed();
}

auto main() -> int
{
        try
        {
                qbert::View_3DS view;
                qbert::Game_Model_Delegating_Impl model;
                qbert::Controller controller{model, view};
                controller.game_loop();
                return 0; // to hbmenu
        }
        catch (const exception &cause)
        {
                report_error(cause.what());
                return -1; // to hbmenu
        }
        catch (...)
        {
                report_error();
                return -2; // to hbmenu
        }
}
