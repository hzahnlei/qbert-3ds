#include "textured_vertex_renderer.hpp"

#include "textured_vertex_shader_shbin.h"

namespace qbert
{

        using std::runtime_error;

        Textured_Vertex_Renderer::Textured_Vertex_Renderer(C3D_Mtx &model_view, C3D_RenderTarget *target_left,
                                                           C3D_Mtx &projection_matrix_left,
                                                           C3D_RenderTarget *target_right,
                                                           C3D_Mtx &projection_matrix_right,
                                                           const Vertex_Count max_vertex_count)
            : Abstract_Vertex_Renderer{
                  model_view,      target_left, projection_matrix_left, target_right, projection_matrix_right,
                  max_vertex_count}
        {
                initialize_shader();
                allocate_vertex_buffer();
                load_texture(
                    sprite_textures_t3x,
                    sprite_textures_t3x_size); // TODO pass in texture map as an argument rather than loading here
        }

        //*********************************************************************
        // Implementation details
        //*********************************************************************

        //---- Construction ----

        auto Textured_Vertex_Renderer::load_vertex_shader() -> void
        {
                m_shader_dvlb = DVLB_ParseFile((u32 *)textured_vertex_shader_shbin, textured_vertex_shader_shbin_size);
                shaderProgramInit(&m_shader_program);
                shaderProgramSetVsh(&m_shader_program, &m_shader_dvlb->DVLE[0]);
        }

        static constexpr auto NO_CUBE{nullptr};

        auto Textured_Vertex_Renderer::load_texture(const void *data, const size_t size) -> void
        {
                auto t3x{Tex3DS_TextureImport(data, size, &texture_map, NO_CUBE, false)};
                if (t3x)
                {
                        // Delete the t3x object since we don't need it
                        Tex3DS_TextureFree(t3x);
                }
                else
                {
                        throw runtime_error{"Failed to load texture data."};
                }
        }

        //---- Rendering ----

        auto Textured_Vertex_Renderer::set_shader_attribute_info() -> void
        {
                m_shader_attribute_info = C3D_GetAttrInfo();
                AttrInfo_Init(m_shader_attribute_info);
                AttrInfo_AddLoader(m_shader_attribute_info, 0, GPU_FLOAT, 3); // v0=position (x,y,z)
                AttrInfo_AddLoader(m_shader_attribute_info, 1, GPU_FLOAT, 2); // v1=texture coordinate (u,v)
        }

        auto Textured_Vertex_Renderer::set_texture_environment() -> void
        {
                C3D_TexSetFilter(&texture_map, GPU_LINEAR, GPU_NEAREST);
                C3D_TexBind(0, &texture_map);
                // Configure the first fragment shading substage to blend the texture color with
                // the vertex color (calculated by the vertex shader using a lighting algorithm)
                // See https://www.opengl.org/sdk/docs/man2/xhtml/glTexEnv.xml for more insight
                auto env{C3D_GetTexEnv(0)};
                C3D_TexEnvInit(env);
                C3D_TexEnvSrc(env, C3D_Both, GPU_TEXTURE0, GPU_PRIMARY_COLOR, GPU_PRIMARY_COLOR);
                C3D_TexEnvFunc(env, C3D_Both, GPU_MODULATE);
                C3D_TexEnvOpAlpha(env, GPU_TEVOP_A_SRC_ALPHA, GPU_TEVOP_A_SRC_ALPHA, GPU_TEVOP_A_SRC_ALPHA);
        }

} // namespace qbert
