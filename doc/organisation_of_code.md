# Organisation of code

The code is bundled in two directories:

*  `game-logic` contains technology agnostic game logic
*  `source` contains 3DS specific code (platform)

Why did I separate the code into these two directories?
I want the game logic to be technology agnostic.
This eases testing.
The software can be designed to be testable, maintainable, readable, extensible etc.
I consider it very efficient to compile and run tests and navigate to failed tests directly from within the IDE.

`game-logic` contains its own `CMakeLists.txt`.
It can be build on command line or from within the IDE (Visual Studio Code in my case).
The code gets compiled for the host (my development computer).
A suite of test cases can be run on the host.
Large parts of the game can be tested.

The code in `source` "marries" the game logic with the 3DS hardware.
It has its own `CMakeLists.txt` too.
The complete code, platform dependant `source`and platform independent `game-logic`, is cross-compiled for the target.
