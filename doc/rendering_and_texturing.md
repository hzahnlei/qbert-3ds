# Rendering vertices and texturing

## Triangles

Everything is a triangle.
It is made up of three points in a 3D space (vector with x, y and z components).

For a triangle to become visible when it is facing you, the points that constitude it need to be given in counter-clockwise order.

More complex shapes are to be composed of multiple triangles.

The following diagram shows a square on the x-y axis (z=0).
It is made up of two triangles (blue and red).
The triangles share two points.
This stitches them together to form a square without a gap.
The gap in the diagram is only there to visually separate the points and the triangles.
I wanted to clearly show that its six points and not just four.

![Rendering triangles](doc/rendering_triangles_export.png "Rendering triangles")

## Textures

Depending on your shader program a triangle can be filled with solid colors.
Each corner can have its indivisual corner.
The colors are then blendet together by the shader program.

Triangles can also be "painted" with textures, that is bitmaps.
The process of texturing roughly works like this:

1.  You provide a bitmap:
    *  That bitmap can also be called texture map.
    *  The bitmap is two dimensional.
       We call those dimensions for no good reason u and v.
       (Where points use x, y and z.)
    *  Each side of the bitmap needs to be a power of two, e.g. 64x64 or 128x256 etc.
2.  For each corner of each triangle you need to provide the shader with u-v coordinates.
    *  The shader uses these u-v coordinates to "cut out" a portion of the texture map and "paint" the triangle with it.
    *  Note that the u-v-coordinats are float values.
       So, you need to translate the integer dimensions of the bitmap to floating point u-v coordinates of your texture map.
       (The good thing here is, that powers of two translate very well into floating point numbers (`float`) inside the CPU/GPU.)

The following diagram shows a texture map.
Say it is 32x32 pixels.
Furtheremore it contains 2x2=4 textures (each 16x16 pixels).
When we now want to texture our square (made of two triangles) from the above exmplw with Coily, then we could use floating point u-v coordinates as depicted in the diagram.

![U-V mapping](doc/u_v_mapping_export.png "U-V mapping")

Now, when we exchange the u-v coordinats, we can flip the image over and have Coily watch to the right.

Here are the coordinates that maintain Coilys orientation from the texture map.

| Point order | U | V |
|------------:|--:|--:|
| 1 | 0.0 | 0.0 |
| 2 | 0.5 | 0.5 |
| 3 | 0.0 | 0.5 |
| 4 | 0.0 | 0.0 |
| 5 | 0.5 | 0.0 |
| 6 | 0.5 | 0.5 |

And here are the coordinates that mirrors Coilys orientation.

| Point order | U | V |
|------------:|--:|--:|
| 1 | 0.5 | 0.0 |
| 2 | 0.0 | 0.5 |
| 3 | 0.5 | 0.5 |
| 4 | 0.5 | 0.0 |
| 5 | 0.0 | 0.0 |
| 6 | 0.0 | 0.5 |

## Texture map

![Texture map](doc/texture_map_export.png "Texture map")
