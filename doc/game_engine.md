# How the game engine works

On a very high level the game is an implementation of the Model-View-Controller design pattern.
The controller and the view represent the hardware dependant part, located in the `source` folder.
The model represents the hardware independent part, located under `game-logic`.

The point here is, that I tried to separate different tasks/concerns into different (smaller) classes/modules.
This is supposed to achieve readability, maintainability and testabolity.

All parts are interacting with each other like depicted below (not all components are shown for the sake of brevity):

![Sequence diagram MVC game engine](doc/seq_diagram_game_engine_export.png "Sequence diagram MVC game engine")


## Controller

The controller creates all objects required and connects them whith each other.
In short: The controller initializes the game, that is hardware and software.

## Game Model

The Game Model is mainly kind of a View Model.
It holds some additional information such as score and level etc.
Despite of that most other data and behavior is sourced out into Game Objects.
These Game Objects can take user input (only the player object) or react automatically (Coily, Slick, Sam etc.).

The Game Objects are interacting with each other.
For example:

*  If Q*bert hops onto a cube (part of the Playfield Game Object), then that cube will change color.
*  If Q*bert is hit by a red ball, then he dies.

## View

The view is kind of a hardware abstraction layer (HAL).
Its purpos is to bundle all interactions with the 3DS hardware in one place.

### Model to Scene Mapper

Please note the Model to Scene Mapper.
It is still considered platform agnostic because it produces a vertice bufferer that is independent from the hardware.
The buffer itself is allocated, rendered and deallocated by the View.
The buffer is provide to the Mapper by the View.
The buffer is just an array (continuous block of memory) of vertices which in turn are just data structures made from `float` values.
It is in no way tied to the 3DS platform or the Citro3D framework, which is used by the View.

### Renderers

The View uses what I call Renderers.
In fact, each Rendere is a pool of vertices and knows how to render these.
So the View is actually delegating things away to other components.
There are different sorts of Renderers:

*  One only renders colored vertices.
   So the data structure only holds 3D coordinates and a color value (RGBA).
*  One only renders textured vertices.
   So the data structure only holds 3D coordinates and UV coordinates.
*  A third one renders textured vertices with an additional color.
   The structure used is bigger than the first ones.
   It holds 3D coordinates, UV coordinates and a color value (RGBA).
   If the color is plain white, then the texture will be rendered as is.
   For other color values the texture will appear differently becaus its colors are getting blendet with the color value.
   This renderer can emulate the behavior of the color-only renderer (use a solid white texture) and the texture-only renderer (use a texture and white color value).

## Coordinate systems

The cubes of the playfield are logically organized in a flat 2D grid.
The coordinates are row and column.
I call those R-C coordinates playfield coordinates.

However, since the playfield appears - of cause - three dimensional on screen we need to transform R-C coordinates to spatial 3D X-Y-Z coordinates.
But not only for rendering.
Also the characters, like balls, Coily and Q*bert are moving within a 3D space.
This X-Y-Z cooridnate system is called game coordinates (or world coordinates).
Playfield cubes, discs and characters are all placed/moving within these game coordinates.
The coordinates are quantized as the cube structure suggests.
All characters always move within that grid of unit volumes.

The game coordinates are later transformed to vertex coordinates (screen coordinates) for rendering.

Since the playfield is organized in 2D coordinates we need to compute 3D coordinates for cubes in case we want to determin whether a character is colliding with a cube (hopping onto it).
The row hereby automatically implies the Y coordinate.
Each row goes one unit down (Y).

![Mapping 2D playfield coordinates to 3D game coordinates](doc/playfield_coordinate_mapping_export.png "Mapping 2D playfield coordinates to 3D game coordinates")
