/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <iostream>
#include <stdexcept>
// #include <vector>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "triangular_playfield.hpp"

// using Catch::Matchers::Message;
// using std::vector;

TEST_CASE("Construction of cubes to form a triangular playfield", "[UT, fast]")
{
        const auto hight{3U};
        const auto cubes{qbert::Triangular_Playfield::cubes(hight)};
        REQUIRE(cubes.size() == 6);

        REQUIRE(cubes[0].position() == qbert::game::Coordinate{0.0f, 0.0f, 0.0f});

        REQUIRE(cubes[1].position() == qbert::game::Coordinate{1.0f, -1.0f, 0.0f});
        REQUIRE(cubes[2].position() == qbert::game::Coordinate{0.0f, -1.0f, 1.0f});

        REQUIRE(cubes[3].position() == qbert::game::Coordinate{2.0f, -2.0f, 0.0f});
        REQUIRE(cubes[4].position() == qbert::game::Coordinate{1.0f, -2.0f, 1.0f});
        REQUIRE(cubes[5].position() == qbert::game::Coordinate{0.0f, -2.0f, 2.0f});
}

TEST_CASE("No cube has been touched on a new playfield", "[UT, fast]")
{
        qbert::Triangular_Playfield playfield;
        REQUIRE_FALSE(playfield.all_cubes_were_touched());

        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{0.0f, 0.0f, 0.0f}));

        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{1.0f, -1.0f, 0.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{0.0f, -1.0f, 1.0f}));

        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{2.0f, -2.0f, 0.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{1.0f, -2.0f, 1.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{0.0f, -2.0f, 2.0f}));

        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{3.0f, -3.0f, 0.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{2.0f, -3.0f, 1.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{1.0f, -3.0f, 2.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{0.0f, -3.0f, 3.0f}));

        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{4.0f, -4.0f, 0.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{3.0f, -4.0f, 1.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{2.0f, -4.0f, 2.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{1.0f, -4.0f, 3.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{0.0f, -4.0f, 4.0f}));

        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{5.0f, -5.0f, 0.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{4.0f, -5.0f, 1.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{3.0f, -5.0f, 2.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{2.0f, -5.0f, 3.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{1.0f, -5.0f, 4.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{0.0f, -5.0f, 5.0f}));

        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{6.0f, -6.0f, 0.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{5.0f, -6.0f, 1.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{4.0f, -6.0f, 2.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{3.0f, -6.0f, 3.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{2.0f, -6.0f, 4.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{1.0f, -6.0f, 5.0f}));
        REQUIRE_FALSE(playfield.has_been_touched(qbert::game::Coordinate{0.0f, -6.0f, 6.0f}));
}

TEST_CASE("Playfield is made up of 28 cubes", "[UT, fast]")
{
        qbert::Triangular_Playfield playfield;
        REQUIRE(playfield.cubes().size() == 28);
}

TEST_CASE("Assert cube count on every row", "[UT, fast]")
{
        qbert::Triangular_Playfield playfield;
        REQUIRE(playfield.cube_count_on_row(0) == 1);
        REQUIRE(playfield.cube_count_on_row(1) == 2);
        REQUIRE(playfield.cube_count_on_row(2) == 3);
        REQUIRE(playfield.cube_count_on_row(3) == 4);
        REQUIRE(playfield.cube_count_on_row(4) == 5);
        REQUIRE(playfield.cube_count_on_row(5) == 6);
        REQUIRE(playfield.cube_count_on_row(6) == 7);
}

TEST_CASE("Assert cube count for playfields of different hights", "[UT, fast]")
{
        REQUIRE(qbert::Triangular_Playfield::cube_count(1) == 1);
        REQUIRE(qbert::Triangular_Playfield::cube_count(2) == 3);
        REQUIRE(qbert::Triangular_Playfield::cube_count(3) == 6);
        REQUIRE(qbert::Triangular_Playfield::cube_count(4) == 10);
        REQUIRE(qbert::Triangular_Playfield::cube_count(5) == 15);
        REQUIRE(qbert::Triangular_Playfield::cube_count(6) == 21);
        REQUIRE(qbert::Triangular_Playfield::cube_count(7) == 28);
}

TEST_CASE("Positions within cube (0,0,0) recognized as 'character is colliding with cube'", "[UT, fast]")
{
        qbert::Triangular_Playfield playfield;
        for (auto x = 0.0f; x < 1.0f; x += 0.1f)
        {
                for (auto y = 0.0f; y > -1.0f; y -= 0.1f)
                {
                        for (auto z = 0.0f; z < 1.0f; z += 0.1f)
                        {
                                std::cout << "X=" << x << ", Y=" << y << ", Z=" << z << '\n';
                                const qbert::game::Coordinate position_within_cube_0_0_0{x, y, z};
                                if (x >= 0.1f)
                                {
                                        std::cout << "CR";
                                }
                                const auto cube{playfield.cube_at(position_within_cube_0_0_0)};
                                REQUIRE(cube != nullptr);
                                REQUIRE(cube->position().x == 0.0f);
                                REQUIRE(cube->position().y == 0.0f);
                                REQUIRE(cube->position().z == 0.0f);
                        }
                }
        }
}

TEST_CASE("Positions within cube (3,3,1) recognized as 'character is colliding with cube'", "[UT, fast]")
{
        qbert::Triangular_Playfield playfield;
        for (auto x = 3.0f; x < 3.0f; x += 0.1f)
        {
                for (auto y = 3.0f; y < 4.0f; y += 0.1f)
                {
                        for (auto z = 1.0f; z < 2.0f; z += 0.1f)
                        {
                                const qbert::game::Coordinate position_within_cube_3_3_1{x, y, z};
                                const auto cube{playfield.cube_at(position_within_cube_3_3_1)};
                                REQUIRE(cube != nullptr);
                                REQUIRE(cube->position().x == 3.0f);
                                REQUIRE(cube->position().y == 1.0f);
                                REQUIRE(cube->position().z == 0.0f);
                        }
                }
        }
}
