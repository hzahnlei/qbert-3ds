/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "attract_mode.hpp"

TEST_CASE("'attract mode' touching cube only succeeds iif Q*bert touches a cube (standing on a cube)", "[CT, fast]")
{
        qbert::Attract_Mode mode;
        const qbert::Cube cube_qbert_is_standing_on{qbert::game::Coordinate{0.0f, 0.0f, 0.0f}};
        REQUIRE(mode.touch(cube_qbert_is_standing_on));
}

TEST_CASE("'attract mode'touching cube will not succeeds if Q*bert outlide playfield (not standing on a cube)",
          "[CT, fast]")
{
        qbert::Attract_Mode mode;
        const qbert::Cube hypothetical_cube_outside_playfield{qbert::game::Coordinate{0.0f, -10.0f, 0.0f}};
        REQUIRE_FALSE(mode.touch(hypothetical_cube_outside_playfield));
}
