/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "sprite_to_scene_mapper.hpp"

//---- Translate small sprites, e.g. Q*bert ----

TEST_CASE("Translation matrix to place vertices for small sprites on screen", "[UT, fast]")
{
        const auto transform{qbert::sprite_to_scene::translate_small_sprite(qbert::game::Coordinate{1.0f, 2.0f, 3.0f})};
        REQUIRE(transform.row[0].x == 0.9f);
        REQUIRE(transform.row[0].y == 0.0f);
        REQUIRE(transform.row[0].z == 0.0f);
        REQUIRE(transform.row[0].w == 0.2f);
        REQUIRE(transform.row[1].x == 0.0f);
        REQUIRE(transform.row[1].y == 0.9f);
        REQUIRE(transform.row[1].z == 0.0f);
        REQUIRE(transform.row[1].w == 1.6f);
        REQUIRE(transform.row[2].x == 0.0f);
        REQUIRE(transform.row[2].y == 0.0f);
        REQUIRE(transform.row[2].z == 0.9f);
        REQUIRE(transform.row[2].w == 0.6f);
}

//---- Translate balloon ----

TEST_CASE("Balloon is positioned besides Q*bert if it dies close to the top of the screen", "[UT, fast]")
{
        const auto transform{
            qbert::sprite_to_scene::translate_balloon_to_player_pos(qbert::game::Coordinate{0.0f, 0.0f, -1.0f})};
        REQUIRE(transform.row[0].x == 0.9f);
        REQUIRE(transform.row[0].y == 0.0f);
        REQUIRE(transform.row[0].z == 0.0f);
        REQUIRE(transform.row[0].w == Approx(-0.08f));
        REQUIRE(transform.row[1].x == 0.0f);
        REQUIRE(transform.row[1].y == 0.9f);
        REQUIRE(transform.row[1].z == 0.0f);
        REQUIRE(transform.row[1].w == 1.24f);
        REQUIRE(transform.row[2].x == 0.0f);
        REQUIRE(transform.row[2].y == 0.0f);
        REQUIRE(transform.row[2].z == 0.9f);
        REQUIRE(transform.row[2].w == Approx(-0.48f));
}

TEST_CASE("Balloon is positioned above Q*bert if it dies far away from the top of the screen", "[UT, fast]")
{
        const auto transform{
            qbert::sprite_to_scene::translate_balloon_to_player_pos(qbert::game::Coordinate{0.0f, -1.0f, 0.0f})};
        REQUIRE(transform.row[0].x == 0.9f);
        REQUIRE(transform.row[0].y == 0.0f);
        REQUIRE(transform.row[0].z == 0.0f);
        REQUIRE(transform.row[0].w == Approx(-0.08f));
        REQUIRE(transform.row[1].x == 0.0f);
        REQUIRE(transform.row[1].y == 0.9f);
        REQUIRE(transform.row[1].z == 0.0f);
        REQUIRE(transform.row[1].w == 1.24f);
        REQUIRE(transform.row[2].x == 0.0f);
        REQUIRE(transform.row[2].y == 0.0f);
        REQUIRE(transform.row[2].z == 0.9f);
        REQUIRE(transform.row[2].w == Approx(-0.08f));
}

//---- Translate tall sprites, e.g. Coily ----

TEST_CASE("Translation matrix to place vertices for tall sprites on screen", "[UT, fast]")
{
        const auto transform{qbert::sprite_to_scene::translate_tall_sprite(qbert::game::Coordinate{1.0f, 2.0f, 3.0f})};
        REQUIRE(transform.row[0].x == 0.85f);
        REQUIRE(transform.row[0].y == 0.0f);
        REQUIRE(transform.row[0].z == 0.0f);
        REQUIRE(transform.row[0].w == 0.2f);
        REQUIRE(transform.row[1].x == 0.0f);
        REQUIRE(transform.row[1].y == 0.85f);
        REQUIRE(transform.row[1].z == 0.0f);
        REQUIRE(transform.row[1].w == 1.5f);
        REQUIRE(transform.row[2].x == 0.0f);
        REQUIRE(transform.row[2].y == 0.0f);
        REQUIRE(transform.row[2].z == 0.85f);
        REQUIRE(transform.row[2].w == 0.4f);
}
