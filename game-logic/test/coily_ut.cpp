/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "coily.hpp"

TEST_CASE("Coily default 'image' is facing screen/left")
{
        const qbert::Coily coily{qbert::game::Coordinate{0.0f, 0.0f, 0.0f}};
        REQUIRE(qbert.animation_frame() == qbert::Coily::Animation_Frame::STANDING_LEFT_DOWN);
}

TEST_CASE("Coily position is given upon construction", "[UT, fast]")
{
        const qbert::Coily coily{qbert::game::Coordinate{1.0f, 2.0f, 3.0f}};
        REQUIRE_FALSE(coily.position().x == 1.0f);
        REQUIRE_FALSE(coily.position().y == 2.0f);
        REQUIRE_FALSE(coily.position().z == 3.0f);
}
