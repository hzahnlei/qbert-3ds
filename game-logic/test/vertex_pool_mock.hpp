#pragma once

#include "vertex_pool.hpp"

#include <cstdlib>
#include <stdexcept>

namespace qbert::test
{

        using std::free;
        using std::malloc;
        using std::runtime_error;

        template <typename VERTEX> class Vertex_Pool_Mock final : public Vertex_Pool<VERTEX>
        {
            public:
                static constexpr Vertex_Count DEFAULT_MAX_VERTEX_COUNT{1024};

            private:
                Vertex_Count m_vertex_count = 0;
                const Vertex_Count m_max_vertex_count;
                VERTEX *m_vertex_buffer = nullptr;

            public:
                Vertex_Pool_Mock(const Vertex_Count max_vertex_count = DEFAULT_MAX_VERTEX_COUNT)
                    : m_max_vertex_count{max_vertex_count}, m_vertex_buffer{static_cast<VERTEX *>(
                                                                malloc(sizeof(VERTEX) * max_vertex_count))}

                {
                }

                ~Vertex_Pool_Mock()
                {
                        free(m_vertex_buffer);
                }

            public:
                auto acquire_vertices(const Vertex_Count vertex_count) -> VERTEX * override
                {
                        if (vertices_are_available(vertex_count))
                        {
                                const auto vertices{&m_vertex_buffer[m_vertex_count]};
                                m_vertex_count += vertex_count;
                                return vertices;
                        }
                        else
                        {
                                throw runtime_error{"No more vertices available."};
                        }
                }

                auto release_all_vertices() -> void override
                {
                        m_vertex_count = 0;
                }

                auto count_of_allocated_vertices() const noexcept -> Vertex_Count
                {
                        return m_vertex_count;
                }

            private:
                inline auto vertices_are_available(const Vertex_Count vertex_count) const noexcept -> bool
                {
                        return m_vertex_count + vertex_count < DEFAULT_MAX_VERTEX_COUNT;
                }
        };

} // namespace qbert::test
