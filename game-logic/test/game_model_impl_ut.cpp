/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "assertion.hpp"
#include "game_model_delegating_impl.hpp"
#include "playfield.hpp"

using Catch::Matchers::Message;

// class Dummy_Playfield final : public qbert::Playfield
// {
//     public:
//         auto touch(const Row_Index, const Column_Index) -> bool
//         {
//                 throw runtime_error{"not supported"};
//         }
//         auto untouch(const Row_Index, const Column_Index) -> bool
//         {
//                 throw runtime_error{"not supported"};
//         }
//         auto has_been_touched(const Row_Index, const Column_Index) const -> bool
//         {
//                 throw runtime_error{"not supported"};
//         }
//         auto all_cubes_were_touched() const -> bool
//         {
//                 throw runtime_error{"not supported"};
//         }
//         auto has_been_changed() -> bool
//         {
//                 throw runtime_error{"not supported"};
//         }
//         auto cube_count() const noexcept -> size_t
//         {
//                 return 0;
//         }
//         auto cube_count_on_row(const Row_Index) const -> size_t
//         {
//                 throw runtime_error{"not supported"};
//         }
//         auto row_count() const noexcept -> size_t
//         {
//                 return 0;
//         }
// };

// Dummy_Playfield DUMMY_PLAYFIELD;

TEST_CASE("Default mode is 'attract mode'", "[UT, fast]")
{
        const qbert::Game_Model_Delegating_Impl game;
        REQUIRE(game.state() == qbert::Game_Model::State::ATTRACT_MODE);
}

TEST_CASE("By default scene is viewed from top, right", "[UT, fast]")
{
        const qbert::Game_Model_Delegating_Impl game;
        REQUIRE(game.scene_angle_x() == 45);
        REQUIRE(game.scene_angle_y() == -45);
        REQUIRE(game.scene_angle_z() == 0);
}

TEST_CASE("In attract mode scene cannot be rotated", "[UT, fast]")
{
        qbert::Game_Model_Delegating_Impl game;
        game.rotate_scene_clockwise();
        REQUIRE(game.scene_angle_x() == 45);
        REQUIRE(game.scene_angle_y() == -45);
        REQUIRE(game.scene_angle_z() == 0);
}

static constexpr qbert::Player_Input START_PRESSED{.pressed = static_cast<qbert::Buttons>(qbert::Button::START),
                                                   .held = 0,
                                                   .released = 0,
                                                   .c_pad = {.dx = 0, .dy = 0},
                                                   .stereoscopic_3d_slider = 1.0f};

auto press_start_button(qbert::Game_Model &game)
{
        game.update(START_PRESSED);
}

TEST_CASE("In game mode scene can be rotated clockwise around Y axis", "[UT, fast]")
{
        GIVEN("Game in game mode")
        {
                qbert::Game_Model_Delegating_Impl game;
                press_start_button(game);
                WHEN("rotate scene clockwise")
                {
                        game.rotate_scene_clockwise();
                        THEN("scene has been rotated clockwise around Y axis")
                        {
                                REQUIRE(game.scene_angle_x() == 45);
                                REQUIRE(game.scene_angle_y() == -46);
                                REQUIRE(game.scene_angle_z() == 0);
                        }
                }
        }
}

TEST_CASE("In game mode scene can be rotated counter-clockwise around Y axis", "[UT, fast]")
{
        GIVEN("Game in game mode")
        {
                qbert::Game_Model_Delegating_Impl game;
                press_start_button(game);
                WHEN("rotate scene clockwise")
                {
                        game.rotate_scene_counter_clockwise();
                        THEN("scene has been rotated clockwise around Y axis")
                        {
                                REQUIRE(game.scene_angle_x() == 45);
                                REQUIRE(game.scene_angle_y() == -44);
                                REQUIRE(game.scene_angle_z() == 0);
                        }
                }
        }
}

SCENARIO("Scene can be roteted clockwise to a certain degree only", "[UT, fast]")
{
        GIVEN("Scene in default rotation/perspective")
        {
                qbert::Game_Model_Delegating_Impl game;
                press_start_button(game);
                WHEN("scene is rotated clockwise by too many degrees")
                {
                        for (auto i = 0; i < 360; i++)
                        {
                                game.rotate_scene_clockwise();
                        }
                        THEN("the scene rotates only to a certain degree")
                        {
                                REQUIRE(game.scene_angle_y() == qbert::Game_Model::MIN_SCENE_ROTATION_Y);
                        }
                }
        }
}

SCENARIO("Scene can be roteted counter-clockwise to a certain degree only", "[UT, fast]")
{
        GIVEN("Scene in default rotation/perspective")
        {
                qbert::Game_Model_Delegating_Impl game;
                press_start_button(game);
                WHEN("scene is rotated counter-clockwise by too many degrees")
                {
                        for (auto i = 0; i < 360; i++)
                        {
                                game.rotate_scene_counter_clockwise();
                        }
                        THEN("the scene rotates only to a certain degree")
                        {
                                REQUIRE(game.scene_angle_y() == qbert::Game_Model::MAX_SCENE_ROTATION_Y);
                        }
                }
        }
}

SCENARIO("Rotation of scene can be reset", "[UT, fast]")
{
        GIVEN("A game world that has been rotated")
        {
                qbert::Game_Model_Delegating_Impl game;
                game.rotate_scene_counter_clockwise();
                WHEN("rotation is reset")
                {
                        game.reset_scene_rotation();
                        THEN("the scene is back to their original orientation")
                        {
                                REQUIRE(game.scene_angle_x() == 45);
                                REQUIRE(game.scene_angle_y() == -45);
                                REQUIRE(game.scene_angle_z() == 0);
                        }
                }
        }
}
