/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "assertion.hpp"
#include "disc.hpp"
#include "game_test_tools.hpp"

using namespace qbert;
using Catch::Matchers::EndsWith;

TEST_CASE("A newly created disc has not rotated yet.", "[UT, assertion]")
{
        const qbert::Disc disc{test::ANY_POSITION};
        REQUIRE(disc.rotation() == 0.0_rad);
}

TEST_CASE("Disc position is given upon creation.", "[UT, assertion]")
{
        const qbert::Disc disc{{1.0f, 2.0f, 3.0f}};
        REQUIRE(disc.position().x == 1.0f);
        REQUIRE(disc.position().y == 2.0f);
        REQUIRE(disc.position().z == 3.0f);
}

TEST_CASE("Disc can be surfed by Q*bert (or any other game object in theory)", "[CT, fast]")
{
        qbert::Disc disc{test::ANY_POSITION};
        disc.carry_to(test::ANY_POSITION);
        SUCCEED("No exception thrown");
}

TEST_CASE("Disc cannot be surfed by more than one game object", "[CT, fast]")
{
        qbert::Disc disc{test::ANY_POSITION};
        disc.carry_to(test::ANY_POSITION);
        try
        {
                disc.carry_to(test::ANY_POSITION);
                FAIL("Expected exception");
        }
        catch (const qbert::Assertion_Violation &cause)
        {
                REQUIRE_THAT(cause.what(), EndsWith("Disc is already carrying a game object"));
        }
}
