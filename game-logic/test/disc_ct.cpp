/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "disc.hpp"
#include "game_model_mock.hpp"

using namespace qbert;

SCENARIO("Discs spins when interacting with game world", "[CT, fast]")
{
        GIVEN("A new disc and a game world")
        {
                qbert::Disc disc{{0.0f, 0.0f, 0.0f}};
                qbert::test::Game_Model_Mock game;
                WHEN("disc interacts with game world")
                {
                        disc.interact(game);
                        THEN("it rotates a tiny little bit")
                        {
                                REQUIRE(disc.rotation() == Approx(0.05_rad));
                        }
                }
        }
}
