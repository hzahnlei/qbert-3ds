/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "game_model_mock.hpp"
#include "overlay_to_scene_mapper.hpp"
#include "textured_colored_vertex.hpp"
#include "vertex_pool_mock.hpp"

//---- Initialization of overlay vertices ----

constexpr auto EXPECTED_MIN_VERTEX_COUNT{276U};

SCENARIO("Minimum amount of vertices is acquired to render text overlay for new game", "[CT, fast]")
{
        GIVEN("fresh vertex pool")
        {
                qbert::test::Vertex_Pool_Mock<qbert::Textured_Colored_Vertex> vertex_pool_mock;
                GIVEN("a game with initial score, player count etc.")
                {
                        qbert::test::Game_Model_Mock game_mock;
                        game_mock.with_hi_score(0)
                            .with_level_number(1)
                            .with_player_count(3)
                            .with_round_number(1)
                            .with_score(0);
                        WHEN("we initialize vertices to represent our overlay")
                        {
                                qbert::overlay_to_scene::init_vertices(game_mock, vertex_pool_mock);
                                THEN("minimum amount of vertices is used from the vertex pool")
                                {
                                        REQUIRE(vertex_pool_mock.count_of_allocated_vertices() ==
                                                EXPECTED_MIN_VERTEX_COUNT);
                                }
                        }
                }
        }
}

SCENARIO("More vertices are acquired to render text overlay if score has increased", "[CT, fast]")
{
        GIVEN("fresh vertex pool")
        {
                qbert::test::Vertex_Pool_Mock<qbert::Textured_Colored_Vertex> vertex_pool_mock;
                GIVEN("a game with increased score 1000")
                {
                        qbert::test::Game_Model_Mock game_mock;
                        game_mock.with_hi_score(0)
                            .with_level_number(1)
                            .with_player_count(3)
                            .with_round_number(1)
                            .with_score(1000);
                        WHEN("we initialize vertices to represent our overlay")
                        {
                                qbert::overlay_to_scene::init_vertices(game_mock, vertex_pool_mock);
                                THEN("higher number of vertices is used from the vertex pool to represent the "
                                     "increased score (more digits)")
                                {
                                        REQUIRE(vertex_pool_mock.count_of_allocated_vertices() ==
                                                EXPECTED_MIN_VERTEX_COUNT + (3 * 6));
                                }
                        }
                }
        }
}
