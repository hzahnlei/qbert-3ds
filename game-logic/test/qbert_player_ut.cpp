/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "qbert_player.hpp"

TEST_CASE("Q*bert default 'image' is facing screen/left", "[UT, fast]")
{
        const qbert::Qbert_Player qbert{qbert::game::Coordinate{0.0f, 0.0f, 0.0f}};
        REQUIRE(qbert.animation_frame() == qbert::Qbert_Player::Animation_Frame::STANDING_LEFT_DOWN);
}

TEST_CASE("Q*bert position is given upon construction", "[UT, fast]")
{
        const qbert::Qbert_Player qbert{qbert::game::Coordinate{1.0f, 2.0f, 3.0f}};
        REQUIRE(qbert.position().x == 1.0f);
        REQUIRE(qbert.position().y == 2.0f);
        REQUIRE(qbert.position().z == 3.0f);
}

TEST_CASE("Q*bert should not move if player does not touch controls", "[UT, fast]")
{
        REQUIRE(qbert::Qbert_Player::desired_move(qbert::NO_PLAYER_INPUT) == qbert::Qbert_Player::Desired_Move::NONE);
}

constexpr qbert::Player_Input SOUTH_WEST{.pressed = 0,
                                         .held = 0,
                                         .released = 0,
                                         .c_pad =
                                             {
                                                 .dx = -50,
                                                 .dy = -50,
                                             },
                                         .stereoscopic_3d_slider = 0.0f};

TEST_CASE("Q*bert should move LEFT/DOWN if player moves C-Pad S/W far enough", "[UT, fast]")
{
        REQUIRE(qbert::Qbert_Player::desired_move(SOUTH_WEST) == qbert::Qbert_Player::Desired_Move::LEFT_DOWN);
}

constexpr qbert::Player_Input NORTH_WEST{.pressed = 0,
                                         .held = 0,
                                         .released = 0,
                                         .c_pad =
                                             {
                                                 .dx = -50,
                                                 .dy = +50,
                                             },
                                         .stereoscopic_3d_slider = 0.0f};

TEST_CASE("Q*bert should move LEFT/UP if player moves C-Pad N/W far enough", "[UT, fast]")
{
        REQUIRE(qbert::Qbert_Player::desired_move(NORTH_WEST) == qbert::Qbert_Player::Desired_Move::LEFT_UP);
}

constexpr qbert::Player_Input SOUTH_EAST{.pressed = 0,
                                         .held = 0,
                                         .released = 0,
                                         .c_pad =
                                             {
                                                 .dx = +50,
                                                 .dy = -50,
                                             },
                                         .stereoscopic_3d_slider = 0.0f};

TEST_CASE("Q*bert should move RIGHT/DOWN if player moves C-Pad S/E far enough", "[UT, fast]")
{
        REQUIRE(qbert::Qbert_Player::desired_move(SOUTH_EAST) == qbert::Qbert_Player::Desired_Move::RIGHT_DOWN);
}

constexpr qbert::Player_Input NORTH_EAST{.pressed = 0,
                                         .held = 0,
                                         .released = 0,
                                         .c_pad =
                                             {
                                                 .dx = +50,
                                                 .dy = +50,
                                             },
                                         .stereoscopic_3d_slider = 0.0f};

TEST_CASE("Q*bert should move RIGHT/UP if player moves C-Pad N/E far enough", "[UT, fast]")
{
        REQUIRE(qbert::Qbert_Player::desired_move(NORTH_EAST) == qbert::Qbert_Player::Desired_Move::RIGHT_UP);
}
