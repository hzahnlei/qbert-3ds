/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "disc.hpp"
#include "game_coordinate.hpp"
#include "triangular_playfield.hpp"

// Forward declaration
auto touch_all_fields(qbert::Triangular_Playfield &playfield) -> void;

SCENARIO("Have to touch all cube to complete a playfield", "[CT, fast]")
{
        GIVEN("a new playfield")
        {
                qbert::Triangular_Playfield playfield;
                WHEN("we touch all cubes")
                {
                        touch_all_fields(playfield);
                        THEN("the playfield is recognized as complete")
                        {
                                REQUIRE(playfield.all_cubes_were_touched());
                        }
                }
        }
}

SCENARIO("Cubes can be untouched", "[CT, fast]")
{
        GIVEN("all cubes touched")
        {
                qbert::Triangular_Playfield playfield;
                touch_all_fields(playfield);
                WHEN("we untouch a single cube")
                {
                        playfield.untouch(qbert::game::Coordinate{0.0f, 0.0f, 0.0f});
                        THEN("the playfield is recognized as incomplete")
                        {
                                REQUIRE_FALSE(playfield.all_cubes_were_touched());
                        }
                }
        }
}

SCENARIO("Status of new playfield is UNCHANGED", "[CT, fast]")
{
        GIVEN("a new playfield")
        {
                qbert::Triangular_Playfield playfield;
                THEN("playfield has not been changed")
                {
                        REQUIRE_FALSE(playfield.has_been_changed());
                }
        }
}

SCENARIO("Status of touched playfield is CHANGED", "[CT, fast]")
{
        GIVEN("a new playfield")
        {
                qbert::Triangular_Playfield playfield;
                WHEN("we touch a cube")
                {
                        playfield.touch(qbert::game::Coordinate{0.0f, 0.0f, 0.0f});
                        THEN("playfield has been changed")
                        {
                                REQUIRE(playfield.has_been_changed());
                        }
                }
        }
}

SCENARIO("Status of untouched playfield is CHANGED", "[CT, fast]")
{
        GIVEN("a new playfield")
        {
                qbert::Triangular_Playfield playfield;
                GIVEN("a touched cube")
                {
                        playfield.touch(qbert::game::Coordinate{0.0f, 0.0f, 0.0f});
                        WHEN("we untouch that cube")
                        {
                                playfield.untouch(qbert::game::Coordinate{0.0f, 0.0f, 0.0f});
                                THEN("playfield has been changed")
                                {
                                        REQUIRE(playfield.has_been_changed());
                                }
                        }
                }
        }
}

SCENARIO("Querying change status of playfield reset that status", "[CT, fast]")
{
        GIVEN("a playfield in state CHANGED")
        {
                qbert::Triangular_Playfield playfield;
                playfield.touch(qbert::game::Coordinate{0.0f, 0.0f, 0.0f});
                WHEN("we query the change status")
                {
                        REQUIRE(playfield.has_been_changed());
                        THEN("status is unchanged")
                        {
                                REQUIRE_FALSE(playfield.has_been_changed());
                        }
                }
        }
}

SCENARIO("Disc on right side of playfield fly to top right", "[CT, fast]")
{
        GIVEN("a playfiled with disc right of playfield")
        {
                const qbert::Triangular_Playfield playfield;
                const qbert::Disc disc_right_of_playfield{
                    qbert::game::Coordinate{+3.0f, -3.0f, -1.0f - qbert::Playfield::GAP}};
                WHEN("disc flies to the top")
                {
                        const auto landing_pos{playfield.disc_landing_position(disc_right_of_playfield.position())};
                        THEN("it will land right of top cube")
                        {
                                REQUIRE(landing_pos == qbert::Triangular_Playfield::RIGHT_OF_TOP_CUBE);
                        }
                }
        }
}

SCENARIO("Disc on left side of playfield fly to top left", "[CT, fast]")
{
        GIVEN("a playfiled with disc left of playfield")
        {
                const qbert::Triangular_Playfield playfield;
                const qbert::Disc disc_left_of_playfield{
                    qbert::game::Coordinate{-1.0f - qbert::Playfield::GAP, -3.0f, +3.0f}};
                WHEN("disc flies to the top")
                {
                        const auto landing_pos{playfield.disc_landing_position(disc_left_of_playfield.position())};
                        THEN("it will land left of top cube")
                        {
                                REQUIRE(landing_pos == qbert::Triangular_Playfield::LEFT_OF_TOP_CUBE);
                        }
                }
        }
}

SCENARIO("Disc that are not on right side are considered left of playfield thus fly to top left", "[CT, fast]")
{
        GIVEN("a playfiled with disc not on right side of playfield")
        {
                const qbert::Triangular_Playfield playfield;
                const qbert::Disc disc_not_right_of_playfield{qbert::game::Coordinate{1.0f, -1.0f, 1.0f}};
                WHEN("disc flies to the top")
                {
                        const auto landing_pos{playfield.disc_landing_position(disc_not_right_of_playfield.position())};
                        THEN("it will land left of top cube")
                        {
                                REQUIRE(landing_pos == qbert::Triangular_Playfield::LEFT_OF_TOP_CUBE);
                        }
                }
        }
}

//---- Implementation details -------------------------------------------------

auto touch_all_fields(qbert::Triangular_Playfield &playfield) -> void
{
        REQUIRE(playfield.touch(qbert::game::Coordinate{0.0f, 0.0f, 0.0f}));

        REQUIRE(playfield.touch(qbert::game::Coordinate{1.0f, -1.0f, 0.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{0.0f, -1.0f, 1.0f}));

        REQUIRE(playfield.touch(qbert::game::Coordinate{2.0f, -2.0f, 0.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{1.0f, -2.0f, 1.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{0.0f, -2.0f, 2.0f}));

        REQUIRE(playfield.touch(qbert::game::Coordinate{3.0f, -3.0f, 0.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{2.0f, -3.0f, 1.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{1.0f, -3.0f, 2.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{0.0f, -3.0f, 3.0f}));

        REQUIRE(playfield.touch(qbert::game::Coordinate{4.0f, -4.0f, 0.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{3.0f, -4.0f, 1.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{2.0f, -4.0f, 2.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{1.0f, -4.0f, 3.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{0.0f, -4.0f, 4.0f}));

        REQUIRE(playfield.touch(qbert::game::Coordinate{5.0f, -5.0f, 0.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{4.0f, -5.0f, 1.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{3.0f, -5.0f, 2.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{2.0f, -5.0f, 3.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{1.0f, -5.0f, 4.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{0.0f, -5.0f, 5.0f}));

        REQUIRE(playfield.touch(qbert::game::Coordinate{6.0f, -6.0f, 0.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{5.0f, -6.0f, 1.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{4.0f, -6.0f, 2.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{3.0f, -6.0f, 3.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{2.0f, -6.0f, 4.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{1.0f, -6.0f, 5.0f}));
        REQUIRE(playfield.touch(qbert::game::Coordinate{0.0f, -6.0f, 6.0f}));
}
