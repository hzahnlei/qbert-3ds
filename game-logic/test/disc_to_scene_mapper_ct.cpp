/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <vector>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "colored_vertex.hpp"
#include "disc_to_scene_mapper.hpp"
#include "unit_disc_vertices.hpp"
#include "vertex_pool_mock.hpp"

using std::vector;

//---- Initialization of disc vertices ----

SCENARIO("No vertices are acquired, if list of discs is empty", "[CT, fast]")
{
        GIVEN("fresh vertex pool")
        {
                qbert::test::Vertex_Pool_Mock<qbert::Colored_Vertex> vertex_pool_mock;
                GIVEN("no discs")
                {
                        const vector<qbert::Disc> no_discs;
                        WHEN("we initialize vertices to represent our 'no discs'")
                        {
                                qbert::disc_to_scene::init_vertices(no_discs, vertex_pool_mock);
                                THEN("no vertices are used from the vertex pool")
                                {
                                        REQUIRE(vertex_pool_mock.count_of_allocated_vertices() == 0);
                                }
                        }
                }
        }
}

SCENARIO("Vertices for one disc are acquired, if list of discs contains one disc", "[CT, fast]")
{
        GIVEN("fresh vertex pool")
        {
                qbert::test::Vertex_Pool_Mock<qbert::Colored_Vertex> vertex_pool_mock;
                GIVEN("one discs")
                {
                        const qbert::Disc disc{qbert::game::Coordinate{1.0f, 2.0f, 3.0f}};
                        const vector<qbert::Disc> discs{disc};
                        WHEN("we initialize vertices to represent our discs")
                        {
                                qbert::disc_to_scene::init_vertices(discs, vertex_pool_mock);
                                THEN("just enough vertices for one disc are used from the vertex pool")
                                {
                                        REQUIRE(vertex_pool_mock.count_of_allocated_vertices() ==
                                                qbert::unit::disc::VERTICES_COUNT);
                                }
                        }
                }
        }
}

SCENARIO("Vertices for N discs are acquired, if list of discs contains N discs", "[CT, fast]")
{
        GIVEN("fresh vertex pool")
        {
                qbert::test::Vertex_Pool_Mock<qbert::Colored_Vertex> vertex_pool_mock;
                GIVEN("N discs")
                {
                        const qbert::Disc disc1{qbert::game::Coordinate{1.0f, 2.0f, 3.0f}};
                        const qbert::Disc disc2{qbert::game::Coordinate{1.0f, 3.0f, 3.0f}};
                        const qbert::Disc disc3{qbert::game::Coordinate{1.0f, 4.0f, 3.0f}};
                        const vector<qbert::Disc> discs{disc1, disc2, disc3};
                        WHEN("we initialize vertices to represent our N discs")
                        {
                                qbert::disc_to_scene::init_vertices(discs, vertex_pool_mock);
                                THEN("just enough vertices for N discs are used from the vertex pool")
                                {
                                        REQUIRE(vertex_pool_mock.count_of_allocated_vertices() ==
                                                discs.size() * qbert::unit::disc::VERTICES_COUNT);
                                }
                        }
                }
        }
}
