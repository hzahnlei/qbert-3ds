/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "transformations.hpp"

TEST_CASE("Factory produces working X rotation matrix", "[UT, fast]")
{
        const qbert::Position original_vector{1.0f, 2.0f, 3.0f};
        const auto rotate_x{qbert::transform::rotation_x(180.0f)};
        const auto rotated_vector{rotate_x(original_vector)};
        REQUIRE(rotated_vector.x == Approx(1.0f));
        REQUIRE(rotated_vector.y == Approx(-2.0f));
        REQUIRE(rotated_vector.z == Approx(-3.0f));
}

TEST_CASE("Factory produces working Y rotation matrix", "[UT, fast]")
{
        const qbert::Position original_vector{1.0f, 2.0f, 3.0f};
        const auto rotate_y{qbert::transform::rotation_y(180.0f)};
        const auto rotated_vector{rotate_y(original_vector)};
        REQUIRE(rotated_vector.x == Approx(-1.0f));
        REQUIRE(rotated_vector.y == Approx(2.0f));
        REQUIRE(rotated_vector.z == Approx(-3.0f));
}

TEST_CASE("Factory produces working Z rotation matrix", "[UT, fast]")
{
        const qbert::Position original_vector{1.0f, 2.0f, 3.0f};
        const auto rotate_z{qbert::transform::rotation_z(180.0f)};
        const auto rotated_vector{rotate_z(original_vector)};
        REQUIRE(rotated_vector.x == Approx(-1.0f));
        REQUIRE(rotated_vector.y == Approx(-2.0f));
        REQUIRE(rotated_vector.z == Approx(3.0f));
}

TEST_CASE("Factory produces working XYZ translation matrix", "[UT, fast]")
{
        const qbert::Position original_vector{0.0f, 0.0f, 0.0f};
        const auto translate{qbert::transform::translation({1.0f, 2.0f, 3.0f})};
        const auto translated_vector{translate(original_vector)};
        REQUIRE(translated_vector.x == Approx(1.0f));
        REQUIRE(translated_vector.y == Approx(2.0f));
        REQUIRE(translated_vector.z == Approx(3.0f));
}
