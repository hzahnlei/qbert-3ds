#pragma once

#include "game_model.hpp"
#include "player_input.hpp"
#include "qbert_player.hpp"
#include <cstdlib>

namespace qbert::test
{

        using std::size_t;

        static constexpr auto FRAMES_REQUIRED_FOR_FALLING_ONTO_TOP_CUBE{25U};
        static constexpr auto FRAMES_REQUIRED_TO_FALL_FROM_PLAYFIELD{FRAMES_REQUIRED_FOR_FALLING_ONTO_TOP_CUBE};
        static constexpr auto FRAMES_REQUIRED_FOR_JUMPING_OFF_PLAYFIELD{51U};

        static constexpr game::Coordinate POSITION_ON_TOP_CUBE{0.0f, 0.0f, 0.0f};
        static constexpr game::Coordinate TOP_CUBE_POSITION{0.0f, 0.0f, 0.0f};

        static constexpr Player_Input START_BUTTON_PRESSED{.pressed = static_cast<Buttons>(Button::START),
                                                           .held = static_cast<Buttons>(Button::NONE),
                                                           .released = static_cast<Buttons>(Button::NONE),
                                                           .c_pad =
                                                               {
                                                                   .dx = 0,
                                                                   .dy = 0,
                                                               },
                                                           .stereoscopic_3d_slider = 1.0f};

        constexpr Player_Input C_PAD_UP_RIGHT{.pressed = static_cast<Buttons>(Button::NONE),
                                              .held = static_cast<Buttons>(Button::NONE),
                                              .released = static_cast<Buttons>(Button::NONE),
                                              .c_pad =
                                                  {
                                                      .dx = 50,
                                                      .dy = 50,
                                                  },
                                              .stereoscopic_3d_slider = 1.0f};

        auto have_game_continue_for_n_frames(Game_Model &game, size_t frame_count) -> void;

        auto fall_for_n_frames(Game_Model &game, size_t frame_count) -> void;

        auto dead_player() -> Qbert_Player;

        static constexpr game::Coordinate ANY_POSITION{0.0f, 0.0f, 0.0f};

} // namespace qbert::test
