#pragma once

#include "coily.hpp"
#include "disc.hpp"
#include "game_model.hpp"
#include "qbert_player.hpp"
#include "triangular_playfield.hpp"
#include <stdexcept>
#include <vector>

namespace qbert::test
{

        using std::runtime_error;
        using std::vector;

        class Game_Model_Mock : public Game_Model
        {
            private:
                Coily m_coily{{-1.0f, -1.0f, -1.0f}};
                Qbert_Player m_cubert{{-1.0f, -1.0f, -1.0f}};
                vector<Disc> m_discs;
                Triangular_Playfield m_playfield;

                Player_Count m_player_count{3};
                Score m_score{0};
                Score m_hi_score{0};
                Level_Number m_level{1};
                Round_Number m_round{1};

            public:
                Game_Model_Mock()
                {
                }

            public:
                auto rotate_scene_clockwise() -> void override
                {
                        throw runtime_error{"not supported"};
                };
                auto rotate_scene_counter_clockwise() -> void override
                {
                        throw runtime_error{"not supported"};
                };
                auto reset_scene_rotation() -> void override
                {
                        throw runtime_error{"not supported"};
                };
                [[nodiscard]] auto scene_angle_x() const noexcept -> Scene_Angle override
                {
                        return -1;
                };
                [[nodiscard]] auto scene_angle_y() const noexcept -> Scene_Angle override
                {
                        return -1;
                };
                [[nodiscard]] auto scene_angle_z() const noexcept -> Scene_Angle override
                {
                        return -1;
                };

                [[nodiscard]] auto playfield() const noexcept -> const Playfield & override
                {
                        return m_playfield;
                };

                [[nodiscard]] auto discs() const noexcept -> const vector<Disc> & override
                {
                        return m_discs;
                };

                [[nodiscard]] auto disc_at(const game::Coordinate &) const -> const Disc *
                {
                        return nullptr;
                }

                auto attempt_to_attach_player_to(const Disc &) -> bool override
                {
                        throw runtime_error{"not supported"};
                }

                auto have_qbert_jump_off_disc_to_playfield() -> void override
                {
                        throw runtime_error{"not supported"};
                }

                [[nodiscard]] auto player() const noexcept -> const Qbert_Player & override
                {
                        return m_cubert;
                };

                [[nodiscard]] auto player_count() const noexcept -> Player_Count override
                {
                        return 0;
                };

                auto with_player_count(const Player_Count count) noexcept -> Game_Model_Mock &
                {
                        m_player_count = count;
                        return *this;
                }

                auto player_died() noexcept -> void override
                {
                        // Intentionally left blank
                }

                [[nodiscard]] auto coily() const noexcept -> const Coily & override
                {
                        return m_coily;
                };

                [[nodiscard]] auto score() const noexcept -> Score override
                {
                        return m_score;
                };

                auto with_score(const Score score) noexcept -> Game_Model_Mock &
                {
                        m_score = score;
                        return *this;
                }

                [[nodiscard]] auto hi_score() const noexcept -> Score override
                {
                        return m_hi_score;
                };

                auto with_hi_score(const Score score) noexcept -> Game_Model_Mock &
                {
                        m_hi_score = score;
                        return *this;
                }

                [[nodiscard]] auto level_number() const noexcept -> Level_Number override
                {
                        return m_level;
                }

                auto with_level_number(const Level_Number level) noexcept -> Game_Model_Mock &
                {
                        m_level = level;
                        return *this;
                }

                [[nodiscard]] auto round_number() const noexcept -> Round_Number override
                {
                        return m_round;
                }

                auto with_round_number(const Round_Number round) noexcept -> Game_Model_Mock &
                {
                        m_round = round;
                        return *this;
                }

                [[nodiscard]] auto state() const noexcept -> State override
                {
                        return State::ATTRACT_MODE;
                }

                [[nodiscard]] auto is_within_playfield(const game::Coordinate &) const noexcept -> bool override
                {
                        return false;
                }

                auto update(const Player_Input &) -> void override
                {
                        throw runtime_error{"not supported"};
                };

                auto touch(const Cube &) -> bool override
                {
                        return false;
                }
        };

} // namespace qbert::test
