/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "attract_mode.hpp"

TEST_CASE("State is always 'attract mode'", "[UT, fast]")
{
        qbert::Attract_Mode mode;
        REQUIRE(mode.state() == qbert::Game_Model::State::ATTRACT_MODE);
}

TEST_CASE("'attract mode' original orientation", "[UT, fast]")
{
        qbert::Attract_Mode mode;
        REQUIRE(mode.scene_angle_x() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_X);
        REQUIRE(mode.scene_angle_y() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Y);
        REQUIRE(mode.scene_angle_z() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Z);
}

TEST_CASE("'attract mode' initial score is zero", "[UT, fast]")
{
        qbert::Attract_Mode mode;
        REQUIRE(mode.score() == 0U);
        REQUIRE(mode.hi_score() == 0U);
}

TEST_CASE("'attract mode' hi-score can be set", "[UT, fast]")
{
        qbert::Attract_Mode mode;
        mode.set_hi_score(100U);
        REQUIRE(mode.hi_score() == 100U);
}

SCENARIO("In 'attract mode' rotating scene clockwise has no effect", "[UT, fast]")
{
        GIVEN("New attract mode")
        {
                qbert::Attract_Mode mode;
                WHEN("scene is rotated clockwise")
                {
                        mode.rotate_scene_clockwise();
                        THEN("scene remains in original orientation")
                        {
                                REQUIRE(mode.scene_angle_x() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_X);
                                REQUIRE(mode.scene_angle_y() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Y);
                                REQUIRE(mode.scene_angle_z() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Z);
                        }
                }
        }
}

SCENARIO("In 'attract mode' rotating scene counter-clockwise has no effect", "[UT, fast]")
{
        GIVEN("New attract mode")
        {
                qbert::Attract_Mode mode;
                WHEN("scene is rotated counter-clockwise")
                {
                        mode.rotate_scene_counter_clockwise();
                        THEN("scene remains in original orientation")
                        {
                                REQUIRE(mode.scene_angle_x() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_X);
                                REQUIRE(mode.scene_angle_y() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Y);
                                REQUIRE(mode.scene_angle_z() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Z);
                        }
                }
        }
}

SCENARIO("In 'attract mode' resetting scene rotation has no effect", "[UT, fast]")
{
        GIVEN("New attract mode")
        {
                qbert::Attract_Mode mode;
                WHEN("scene orientation is reset")
                {
                        mode.reset_scene_rotation();
                        THEN("scene remains in original orientation")
                        {
                                REQUIRE(mode.scene_angle_x() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_X);
                                REQUIRE(mode.scene_angle_y() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Y);
                                REQUIRE(mode.scene_angle_z() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Z);
                        }
                }
        }
}
