/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "game_model_delegating_impl.hpp"
#include "game_test_tools.hpp"
#include "transformations.hpp"

using namespace qbert;

SCENARIO("Updating the game world spins discs", "[CT, fast]")
{
        GIVEN("a new game world")
        {
                qbert::Game_Model_Delegating_Impl game;
                WHEN("the game is updated")
                {
                        game.update(qbert::NO_PLAYER_INPUT);
                        THEN("all discs have spun a tiny little bit")
                        {
                                REQUIRE(game.discs().size() == 2);
                                REQUIRE(game.discs()[0].rotation() == Approx(0.05_rad));
                                REQUIRE(game.discs()[1].rotation() == Approx(0.05_rad));
                        }
                }
        }
}

SCENARIO("The program starts in 'attract' mode", "[CT, fast]")
{
        GIVEN("a new game world")
        {
                qbert::Game_Model_Delegating_Impl game;
                THEN("game is in 'attract' mode")
                {
                        REQUIRE(game.state() == qbert::Game_Model::State::ATTRACT_MODE);
                }
        }
}

SCENARIO("Press START and program changes to 'game' mode", "[CT, fast]")
{
        GIVEN("a new game world")
        {
                qbert::Game_Model_Delegating_Impl game;
                WHEN("START is pressed")
                {
                        game.update(qbert::test::START_BUTTON_PRESSED);
                        THEN("game is in 'game' mode")
                        {
                                REQUIRE(game.state() == qbert::Game_Model::State::PLAY);
                        }
                }
        }
}

SCENARIO("In 'attract' mode, Q*bert is dropped and landing on top cube", "[CT, fast]")
{
        GIVEN("a new game world")
        {
                qbert::Game_Model_Delegating_Impl game;
                WHEN("when Q*bert is falling for some time")
                {
                        qbert::test::fall_for_n_frames(game, qbert::test::FRAMES_REQUIRED_FOR_FALLING_ONTO_TOP_CUBE);
                        THEN("he finally lands on the top cube")
                        {
                                REQUIRE(game.player().position() == qbert::test::POSITION_ON_TOP_CUBE);
                        }
                }
        }
}

SCENARIO("In 'attract' mode, top cube color changes when Q*bert dopped onto it", "[CT, fast]")
{
        GIVEN("a new game world")
        {
                qbert::Game_Model_Delegating_Impl game;
                WHEN("when Q*bert lands on top cube")
                {
                        qbert::test::fall_for_n_frames(game, qbert::test::FRAMES_REQUIRED_FOR_FALLING_ONTO_TOP_CUBE);
                        THEN("color of top cube has changed")
                        {
                                REQUIRE(game.playfield().cube_at(qbert::test::TOP_CUBE_POSITION)->has_been_touched());
                        }
                }
        }
}

SCENARIO("In 'attract' mode, Q*bert is rewarded 25pts for each cube changed to target color", "[CT, fast]")
{
        GIVEN("a new game world")
        {
                qbert::Game_Model_Delegating_Impl game;
                WHEN("Q*bert lands on top cube")
                {
                        qbert::test::fall_for_n_frames(game, qbert::test::FRAMES_REQUIRED_FOR_FALLING_ONTO_TOP_CUBE);
                        THEN("player gets rewarded 25pts")
                        {
                                REQUIRE(game.score() == 25U);
                        }
                }
        }
}

SCENARIO("Q*bert dies when jumping off playfield", "[CT, fast]")
{
        GIVEN("game has started")
        {
                qbert::Game_Model_Delegating_Impl game;
                game.update(qbert::test::START_BUTTON_PRESSED);
                GIVEN("player with initial number of lives")
                {
                        const auto initial_player_count{game.player_count()};
                        GIVEN("Q*bert stands on top cube")
                        {
                                qbert::test::fall_for_n_frames(game,
                                                               qbert::test::FRAMES_REQUIRED_FOR_FALLING_ONTO_TOP_CUBE);
                                WHEN("Q*bert jumps into direction top/right")
                                {
                                        game.update(qbert::test::C_PAD_UP_RIGHT);
                                        qbert::test::have_game_continue_for_n_frames(
                                            game, qbert::test::FRAMES_REQUIRED_FOR_JUMPING_OFF_PLAYFIELD);
                                        THEN("Q*Bert has jumped off playfield")
                                        {
                                                REQUIRE(game.player().position().x == 0.0f);
                                                REQUIRE(game.player().position().y == 0.0f);
                                                REQUIRE(game.player().position().z == -1.0f);
                                        }
                                        THEN("Q*Bert ultimately dies")
                                        {
                                                REQUIRE(game.player().animation_frame() ==
                                                        qbert::Qbert_Player::Animation_Frame::DEAD);
                                        }
                                        THEN("player looses one live")
                                        {
                                                REQUIRE(game.player_count() == initial_player_count - 1);
                                        }
                                }
                        }
                }
        }
}
