/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "coily.hpp"
#include "game_coordinate.hpp"
#include "game_test_tools.hpp"
#include "qbert_player.hpp"
#include "sprite_to_scene_mapper.hpp"
#include "textured_vertex.hpp"
#include "unit_sprite_vertices.hpp"
#include "vertex_pool_mock.hpp"

//---- Init Q*bert vertices ----

SCENARIO("Living Q*Bert requires a minimum of 6 vertices (to form one square)", "[CT, fast]")
{
        GIVEN("fresh vertex pool")
        {
                qbert::test::Vertex_Pool_Mock<qbert::Textured_Vertex> vertex_pool_mock;
                GIVEN("player is alive")
                {
                        const qbert::game::Coordinate any_position{0.0f, 0.0f, 0.0f};
                        const qbert::Qbert_Player player_alive{any_position};
                        WHEN("we map the player model to vertices")
                        {
                                qbert::sprite_to_scene::init_qbert_vertices(player_alive, vertex_pool_mock);
                                THEN("only vertices for the player are acquired from the vertex pool")
                                {
                                        REQUIRE(vertex_pool_mock.count_of_allocated_vertices() ==
                                                qbert::unit::sprite::VERTICES_COUNT);
                                }
                        }
                }
        }
}

TEST_CASE("Dead Q*Bert requires more vertices because of the balloon (to form two squares)", "[CT, fast]")
{
        GIVEN("fresh vertex pool")
        {
                qbert::test::Vertex_Pool_Mock<qbert::Textured_Vertex> vertex_pool_mock;
                GIVEN("player is dead (fallen from playfield)")
                {
                        const auto player_dead{qbert::test::dead_player()};
                        WHEN("we map the player model to vertices")
                        {
                                qbert::sprite_to_scene::init_qbert_vertices(player_dead, vertex_pool_mock);
                                THEN("vertices for the player AND the balloon are acquired from the vertex pool")
                                {
                                        REQUIRE(vertex_pool_mock.count_of_allocated_vertices() ==
                                                qbert::unit::sprite::VERTICES_COUNT +
                                                    qbert::unit::balloon_sprite::VERTICES_COUNT);
                                }
                        }
                }
        }
}

//---- Init Coily vertices ----

SCENARIO("Coily requires a minimum of 6 vertices (to form one square)", "[CT, fast]")
{
        GIVEN("fresh vertex pool")
        {
                qbert::test::Vertex_Pool_Mock<qbert::Textured_Vertex> vertex_pool_mock;
                GIVEN("any Coily")
                {
                        const qbert::game::Coordinate any_position{0.0f, 0.0f, 0.0f};
                        const qbert::Coily coily{any_position};
                        WHEN("we map the Coily model to vertices")
                        {
                                qbert::sprite_to_scene::init_coily_vertices(coily, vertex_pool_mock);
                                THEN("only vertices for Coily are acquired from the vertex pool")
                                {
                                        REQUIRE(vertex_pool_mock.count_of_allocated_vertices() ==
                                                qbert::unit::tall_sprite::VERTICES_COUNT);
                                }
                        }
                }
        }
}
