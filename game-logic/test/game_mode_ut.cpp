/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "game_mode.hpp"

TEST_CASE("State is always 'game mode'", "[UT, fast]")
{
        qbert::Game_Mode mode;
        REQUIRE(mode.state() == qbert::Game_Model::State::PLAY);
}

TEST_CASE("'game mode' original orientation", "[UT, fast]")
{
        qbert::Game_Mode mode;
        REQUIRE(mode.scene_angle_x() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_X);
        REQUIRE(mode.scene_angle_y() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Y);
        REQUIRE(mode.scene_angle_z() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Z);
}

TEST_CASE("'game mode' initial score is zero", "[UT, fast]")
{
        qbert::Game_Mode mode;
        REQUIRE(mode.score() == 0U);
        REQUIRE(mode.hi_score() == 0U);
}

TEST_CASE("'game mode' hi-score can be set", "[UT, fast]")
{
        qbert::Game_Mode mode;
        mode.set_hi_score(100U);
        REQUIRE(mode.hi_score() == 100U);
}

SCENARIO("In 'game mode' scene can be rotated clockwise", "[UT, fast]")
{
        GIVEN("New game mode")
        {
                qbert::Game_Mode mode;
                WHEN("scene is rotated clockwise")
                {
                        mode.rotate_scene_clockwise();
                        THEN("scene has been rotated around Y axis")
                        {
                                REQUIRE(mode.scene_angle_x() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_X);
                                REQUIRE(mode.scene_angle_y() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Y - 1);
                                REQUIRE(mode.scene_angle_z() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Z);
                        }
                }
        }
}

SCENARIO("In 'game mode' scene can be rotated counter-clockwise", "[UT, fast]")
{
        GIVEN("New game mode")
        {
                qbert::Game_Mode mode;
                WHEN("scene is rotated counter-clockwise")
                {
                        mode.rotate_scene_counter_clockwise();
                        THEN("scene has been rotated around Y axis")
                        {
                                REQUIRE(mode.scene_angle_x() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_X);
                                REQUIRE(mode.scene_angle_y() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Y + 1);
                                REQUIRE(mode.scene_angle_z() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Z);
                        }
                }
        }
}

SCENARIO("In 'game mode' scene rotation can be reset", "[UT, fast]")
{
        GIVEN("Game mode with scene rotated")
        {
                qbert::Game_Mode mode;
                mode.rotate_scene_clockwise();
                WHEN("scene orientation is reset")
                {
                        mode.reset_scene_rotation();
                        THEN("scene returns to original orientation")
                        {
                                REQUIRE(mode.scene_angle_x() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_X);
                                REQUIRE(mode.scene_angle_y() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Y);
                                REQUIRE(mode.scene_angle_z() == qbert::Game_Model::DEFAULT_SCENE_ROTATION_Z);
                        }
                }
        }
}

SCENARIO("In 'game mode' scene can be roteted clockwise to a certain degree only", "[UT, fast]")
{
        GIVEN("Scene in default rotation/perspective")
        {
                qbert::Game_Mode mode;
                WHEN("scene is rotated clockwise by too many degrees")
                {
                        for (auto i = 0; i < 360; i++)
                        {
                                mode.rotate_scene_clockwise();
                        }
                        THEN("the scene rotates only to a certain degree")
                        {
                                REQUIRE(mode.scene_angle_y() == qbert::Game_Model::MIN_SCENE_ROTATION_Y);
                        }
                }
        }
}

SCENARIO("In 'game mode' scene can be roteted counter-clockwise to a certain degree only", "[UT, fast]")
{
        GIVEN("Scene in default rotation/perspective")
        {
                qbert::Game_Mode mode;
                WHEN("scene is rotated counter-clockwise by too many degrees")
                {
                        for (auto i = 0; i < 360; i++)
                        {
                                mode.rotate_scene_counter_clockwise();
                        }
                        THEN("the scene rotates only to a certain degree")
                        {
                                REQUIRE(mode.scene_angle_y() == qbert::Game_Model::MAX_SCENE_ROTATION_Y);
                        }
                }
        }
}
