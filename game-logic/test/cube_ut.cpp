/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "cube.hpp"

TEST_CASE("New cube is untouched", "[UT, fast]")
{
        const qbert::Cube cube{qbert::game::Coordinate{0.0f, 0.0f, 0.0f}};
        REQUIRE_FALSE(cube.has_been_touched());
}

TEST_CASE("Cube position is given upon construction", "[UT, fast]")
{
        const qbert::Cube cube{qbert::game::Coordinate{1.0f, 2.0f, 3.0f}};
        REQUIRE(cube.position().x == 1.0f);
        REQUIRE(cube.position().y == 2.0f);
        REQUIRE(cube.position().z == 3.0f);
}

SCENARIO("Cube can be touched", "[UT, fast]")
{
        GIVEN("Untouched cube")
        {
                qbert::Cube cube{qbert::game::Coordinate{0.0f, 0.0f, 0.0f}};
                WHEN("cube is touched")
                {
                        REQUIRE(cube.touch());
                        THEN("it remembers having been touched")
                        {
                                REQUIRE(cube.has_been_touched());
                        }
                }
        }
}

SCENARIO("Cube can be touched multiple times (without effect)", "[UT, fast]")
{
        GIVEN("Touched cube")
        {
                qbert::Cube cube{qbert::game::Coordinate{0.0f, 0.0f, 0.0f}};
                cube.touch();
                WHEN("cube is touched again")
                {
                        REQUIRE_FALSE(cube.touch());
                        THEN("it is still touched")
                        {
                                REQUIRE(cube.has_been_touched());
                        }
                }
        }
}

SCENARIO("Cube can be untouched", "[UT, fast]")
{
        GIVEN("Touched cube")
        {
                qbert::Cube cube{qbert::game::Coordinate{0.0f, 0.0f, 0.0f}};
                cube.touch();
                WHEN("cube is untouched")
                {
                        REQUIRE(cube.untouch());
                        THEN("it remembers having been untouched")
                        {
                                REQUIRE_FALSE(cube.has_been_touched());
                        }
                }
        }
}

SCENARIO("Cube can be untouched multiple times (without effect)", "[UT, fast]")
{
        GIVEN("Untouched cube")
        {
                qbert::Cube cube{qbert::game::Coordinate{0.0f, 0.0f, 0.0f}};
                cube.touch();
                cube.untouch();
                WHEN("cube is untouched again")
                {
                        REQUIRE_FALSE(cube.untouch());
                        THEN("it is still untouched")
                        {
                                REQUIRE_FALSE(cube.has_been_touched());
                        }
                }
        }
}

TEST_CASE("Two cubes are equal iif positions and touch-states are equals", "[UT, fast]")
{
        const qbert::Cube cube_0_0_0_u{qbert::game::Coordinate{0.0f, 0.0f, 0.0f}, false};
        const qbert::Cube cube_0_0_0_t{qbert::game::Coordinate{0.0f, 0.0f, 0.0f}, true};
        const qbert::Cube cube_0_1_0_u{qbert::game::Coordinate{0.0f, 1.0f, 0.0f}, false};
        const qbert::Cube cube_0_1_0_t{qbert::game::Coordinate{0.0f, 1.0f, 0.0f}, true};

        REQUIRE(cube_0_0_0_u == cube_0_0_0_u);
        REQUIRE_FALSE(cube_0_0_0_u == cube_0_0_0_t);
        REQUIRE_FALSE(cube_0_0_0_u == cube_0_1_0_u);
        REQUIRE_FALSE(cube_0_0_0_u == cube_0_1_0_t);

        REQUIRE_FALSE(cube_0_0_0_t == cube_0_0_0_u);
        REQUIRE(cube_0_0_0_t == cube_0_0_0_t);
        REQUIRE_FALSE(cube_0_0_0_t == cube_0_1_0_u);
        REQUIRE_FALSE(cube_0_0_0_t == cube_0_1_0_t);

        REQUIRE_FALSE(cube_0_1_0_u == cube_0_0_0_u);
        REQUIRE_FALSE(cube_0_1_0_u == cube_0_0_0_t);
        REQUIRE(cube_0_1_0_u == cube_0_1_0_u);
        REQUIRE_FALSE(cube_0_1_0_u == cube_0_1_0_t);

        REQUIRE_FALSE(cube_0_1_0_t == cube_0_0_0_u);
        REQUIRE_FALSE(cube_0_1_0_t == cube_0_0_0_t);
        REQUIRE_FALSE(cube_0_1_0_t == cube_0_1_0_u);
        REQUIRE(cube_0_1_0_t == cube_0_1_0_t);
}
