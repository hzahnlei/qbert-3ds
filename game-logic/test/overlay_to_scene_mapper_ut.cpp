/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "overlay_to_scene_mapper.hpp"

//---- "Printing" ASCII characters ----

/*!
 * | Char | Mapped row | U coordinate in texture map |
 * |-----!|-----------!|----------------------------!|
 * |    0 |          0 |                      0.0000 |
 * |    1 |          1 |                      0.0625 |
 * |   15 |         15 |                      0.9375 |
 */
TEST_CASE("U coordinate for characters 0-15 (1st row)", "[UT, fast]")
{
        REQUIRE(qbert::overlay_to_scene::u_coordinate_of_ascii_char(00) == Approx(0.000f));
        REQUIRE(qbert::overlay_to_scene::u_coordinate_of_ascii_char(01) == Approx(0.0625f));
        REQUIRE(qbert::overlay_to_scene::u_coordinate_of_ascii_char(15) == Approx(0.9375f));
}

/*!
 * | Char | Mapped row | U coordinate in texture map |
 * |-----!|-----------!|----------------------------!|
 * |   16 |          0 |                      0.0000 |
 * |   17 |          1 |                      0.0625 |
 * |   31 |         15 |                      0.9375 |
 */
TEST_CASE("U coordinate for characters 16-31 (2nd row)", "[UT, fast]")
{
        REQUIRE(qbert::overlay_to_scene::u_coordinate_of_ascii_char(16) == Approx(0.000f));
        REQUIRE(qbert::overlay_to_scene::u_coordinate_of_ascii_char(17) == Approx(0.0625f));
        REQUIRE(qbert::overlay_to_scene::u_coordinate_of_ascii_char(31) == Approx(0.9375f));
}

/*!
 * | Char | Mapped column | U coordinate in texture map |
 * |-----!|--------------!|----------------------------!|
 * |   32 |             0 |                      0.0000 |
 * |   33 |             1 |                      0.0625 |
 * |   47 |            15 |                      0.9375 |
 */
TEST_CASE("U coordinate for characters 32-47 (3rd row)", "[UT, fast]")
{
        REQUIRE(qbert::overlay_to_scene::u_coordinate_of_ascii_char(' ') == Approx(0.000f));  // 32
        REQUIRE(qbert::overlay_to_scene::u_coordinate_of_ascii_char('!') == Approx(0.0625f)); // 33
        REQUIRE(qbert::overlay_to_scene::u_coordinate_of_ascii_char('/') == Approx(0.9375f)); // 47
}

/*!
 * | Char | Mapped row | V coordinate in texture map |
 * |-----!|-----------!|----------------------------!|
 * |    0 |          0 |                        0.75 |
 * |    1 |          0 |                        0.75 |
 * |   15 |          0 |                        0.75 |
 */
TEST_CASE("V coordinate for characters 0-15 (1st row)", "[UT, fast]")
{
        REQUIRE(qbert::overlay_to_scene::v_coordinate_of_ascii_char(00) == Approx(0.75f));
        REQUIRE(qbert::overlay_to_scene::v_coordinate_of_ascii_char(01) == Approx(0.75f));
        REQUIRE(qbert::overlay_to_scene::v_coordinate_of_ascii_char(15) == Approx(0.75f));
}

/*!
 * | Char | Mapped row | V coordinate in texture map |
 * |-----!|-----------!|----------------------------!|
 * |   16 |          1 |                     0.78125 |
 * |   17 |          1 |                     0.78125 |
 * |   31 |          1 |                     0.78125 |
 */
TEST_CASE("V coordinate for characters 16-31 (2nd row)", "[UT, fast]")
{
        REQUIRE(qbert::overlay_to_scene::v_coordinate_of_ascii_char(16) == Approx(0.78125f));
        REQUIRE(qbert::overlay_to_scene::v_coordinate_of_ascii_char(17) == Approx(0.78125f));
        REQUIRE(qbert::overlay_to_scene::v_coordinate_of_ascii_char(31) == Approx(0.78125f));
}

/*!
 * | Char | Mapped row | V coordinate in texture map |
 * |-----!|-----------!|----------------------------!|
 * |   32 |          2 |                      0.8125 |
 * |   33 |          2 |                      0.8125 |
 * |   47 |          2 |                      0.8125 |
 */
TEST_CASE("V coordinate for characters 32-47 (3nd row)", "[UT, fast]")
{
        REQUIRE(qbert::overlay_to_scene::v_coordinate_of_ascii_char(' ') == Approx(0.8125f)); // 32
        REQUIRE(qbert::overlay_to_scene::v_coordinate_of_ascii_char('!') == Approx(0.8125f)); // 33
        REQUIRE(qbert::overlay_to_scene::v_coordinate_of_ascii_char('/') == Approx(0.8125f)); // 47
}

TEST_CASE("Calculation of number of digits a printed number will require", "[UT, fast]")
{
        REQUIRE(qbert::overlay_to_scene::number_of_digits(0) == 1);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(1) == 1);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(2) == 1);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(3) == 1);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(4) == 1);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(5) == 1);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(6) == 1);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(7) == 1);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(8) == 1);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(9) == 1);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(10) == 2);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(11) == 2);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(12) == 2);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(13) == 2);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(14) == 2);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(15) == 2);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(16) == 2);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(17) == 2);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(18) == 2);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(19) == 2);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(20) == 2);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(1025) == 4);
        REQUIRE(qbert::overlay_to_scene::number_of_digits(102500) == 6);
}
