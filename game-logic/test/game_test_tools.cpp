#include "game_test_tools.hpp"

#include "game_model_mock.hpp"
#include <functional>

namespace qbert::test
{

        using std::function;

        auto have_game_continue_for_n_frames(Game_Model &game, size_t frame_count) -> void
        {
                while (frame_count > 0)
                {
                        game.update(NO_PLAYER_INPUT);
                        frame_count--;
                }
        }

        auto fall_for_n_frames(Game_Model &game, size_t frame_count) -> void
        {
                have_game_continue_for_n_frames(game, frame_count);
        }

        using Interaction = function<void()>;

        auto repeat_for_n_frames(size_t frame_count, const Interaction &interaction)
        {
                while (frame_count > 0)
                {
                        interaction();
                        frame_count--;
                }
        }

        auto dead_player() -> Qbert_Player
        {
                Game_Model_Mock game_mock;
                const game::Coordinate position_outside_playfield{0.0f, 0.0f, -1.0f};
                Qbert_Player player{position_outside_playfield};
                repeat_for_n_frames(FRAMES_REQUIRED_TO_FALL_FROM_PLAYFIELD + 1, [&]() { player.interact(game_mock); });
                return player;
        }

} // namespace qbert::test
