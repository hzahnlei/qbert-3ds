/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "disc_to_scene_mapper.hpp"

//---- Rotation and translation of discs ----

SCENARIO("Affine transformation matrix for discs respects disc position", "[UT, fast]")
{
        GIVEN("Disc at game coordinates (1,2,3), not rotated (0 rad)")
        {
                const qbert::Disc disc{qbert::game::Coordinate{1.0f, 2.0f, 3.0f}};
                WHEN("an affine transformation is computed for this disc")
                {
                        const auto matrix{qbert::disc_to_scene::rotate_and_translate(disc)};
                        THEN("this affine transform translates vertices to screen coordinates (without rotation)")
                        {
                                // 1.0 0.0 0.0 0.2
                                // 0.0 1.0 0.0 1.5
                                // 0.0 0.0 1.0 0.6
                                // -----+----- -+-
                                //      !       !
                                //      !       +-- Translation, including Y offset to have playfield
                                //      !           in center of screen
                                //      +---------- Simple identity, because disc has not been rotated
                                REQUIRE(matrix.row[0].x == 1.0f);
                                REQUIRE(matrix.row[0].y == 0.0f);
                                REQUIRE(matrix.row[0].z == 0.0f);
                                REQUIRE(matrix.row[0].w == 0.2f);
                                REQUIRE(matrix.row[1].x == 0.0f);
                                REQUIRE(matrix.row[1].y == 1.0f);
                                REQUIRE(matrix.row[1].z == 0.0f);
                                REQUIRE(matrix.row[1].w == 1.5f);
                                REQUIRE(matrix.row[2].x == 0.0f);
                                REQUIRE(matrix.row[2].y == 0.0f);
                                REQUIRE(matrix.row[2].z == 1.0f);
                                REQUIRE(matrix.row[2].w == 0.6f);
                        }
                }
        }
}
