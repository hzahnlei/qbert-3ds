/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <vector>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "colored_vertex.hpp"
#include "cube.hpp"
#include "playfield_to_scene_mapper.hpp"
#include "unit_cube_vertices.hpp"
#include "vertex_pool_mock.hpp"

using std::vector;

//---- Initialization of cube vertices ----

SCENARIO("No vertices are acquired, if list of cubes is empty", "[CT, fast]")
{
        GIVEN("fresh vertex pool")
        {
                qbert::test::Vertex_Pool_Mock<qbert::Colored_Vertex> vertex_pool_mock;
                GIVEN("no cubes")
                {
                        const vector<qbert::Cube> no_cubes;
                        WHEN("we initialize vertices to represent our 'no cubes'")
                        {
                                qbert::playfield_to_scene::init_vertices(no_cubes, vertex_pool_mock);
                                THEN("no vertices are used from the vertex pool")
                                {
                                        REQUIRE(vertex_pool_mock.count_of_allocated_vertices() == 0);
                                }
                        }
                }
        }
}

SCENARIO("Vertices for one cube are acquired, if list of cubes contains one cube", "[CT, fast]")
{
        GIVEN("fresh vertex pool")
        {
                qbert::test::Vertex_Pool_Mock<qbert::Colored_Vertex> vertex_pool_mock;
                GIVEN("one cube")
                {
                        const qbert::Cube cube{qbert::game::Coordinate{1.0f, 2.0f, 3.0f}};
                        const vector<qbert::Cube> cubes{cube};
                        WHEN("we initialize vertices to represent our cubes")
                        {
                                qbert::playfield_to_scene::init_vertices(cubes, vertex_pool_mock);
                                THEN("just enough vertices for one cube are used from the vertex pool")
                                {
                                        REQUIRE(vertex_pool_mock.count_of_allocated_vertices() ==
                                                qbert::unit::cube::VERTICES_COUNT);
                                }
                        }
                }
        }
}

SCENARIO("Vertices for N cubes are acquired, if list of cubes contains N cubes", "[CT, fast]")
{
        GIVEN("fresh vertex pool")
        {
                qbert::test::Vertex_Pool_Mock<qbert::Colored_Vertex> vertex_pool_mock;
                GIVEN("N cubes")
                {
                        const qbert::Cube cube1{qbert::game::Coordinate{1.0f, 2.0f, 3.0f}};
                        const qbert::Cube cube2{qbert::game::Coordinate{1.0f, 3.0f, 3.0f}};
                        const qbert::Cube cube3{qbert::game::Coordinate{1.0f, 4.0f, 3.0f}};
                        const vector<qbert::Cube> cubes{cube1, cube2, cube3};
                        WHEN("we initialize vertices to represent our N cubes")
                        {
                                qbert::playfield_to_scene::init_vertices(cubes, vertex_pool_mock);
                                THEN("just enough vertices for N cubes are used from the vertex pool")
                                {
                                        REQUIRE(vertex_pool_mock.count_of_allocated_vertices() ==
                                                cubes.size() * qbert::unit::cube::VERTICES_COUNT);
                                }
                        }
                }
        }
}
