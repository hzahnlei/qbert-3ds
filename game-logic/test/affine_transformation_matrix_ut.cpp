/*!
 * Q*bert for Nintendo 3DS
 *
 *(c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- C headers
//---- C++ headers
#include <stdexcept>
//---- 3rd party headers
#include "catch2/catch.hpp"
//---- Project headers
#include "affine_transformation_matrix.hpp"

SCENARIO("Transforms can be used to mutate vecors in place", "[UT, fast]")
{
        GIVEN("any transformation matrix and vector")
        {
                qbert::Affine_Transformation_Matrix transform{.row = {
                                                                  {1.0f, 0.0f, 0.0f, 0.5f},
                                                                  {0.0f, 1.0f, 0.0f, 0.6f},
                                                                  {0.0f, 0.0f, 1.0f, 0.7f},
                                                              }};
                qbert::Position position{1.0f, 2.0f, 3.0f};
                WHEN("we apply matrix on vector")
                {
                        transform.apply_to(position);
                        THEN("original vector has been changed accordingly")
                        {
                                REQUIRE(position.x == Approx(1.5f));
                                REQUIRE(position.y == Approx(2.6f));
                                REQUIRE(position.z == Approx(3.7f));
                        }
                }
        }
}

SCENARIO("Transforms can be used to create new, transformed vecors", "[UT, fast]")
{
        GIVEN("any transformation matrix and vector")
        {
                qbert::Affine_Transformation_Matrix transform{.row = {
                                                                  {1.0f, 0.0f, 0.0f, 0.5f},
                                                                  {0.0f, 1.0f, 0.0f, 0.6f},
                                                                  {0.0f, 0.0f, 1.0f, 0.7f},
                                                              }};
                const qbert::Position position{1.0f, 2.0f, 3.0f};
                WHEN("we multiply matrix with vector")
                {
                        const auto transformed_vector{transform(position)};
                        THEN("original vector has NOT been changed")
                        {
                                REQUIRE(position.x == Approx(1.0f));
                                REQUIRE(position.y == Approx(2.0f));
                                REQUIRE(position.z == Approx(3.0f));
                        }
                        THEN("new vector reflects transformation")
                        {
                                REQUIRE(transformed_vector.x == Approx(1.5f));
                                REQUIRE(transformed_vector.y == Approx(2.6f));
                                REQUIRE(transformed_vector.z == Approx(3.7f));
                        }
                }
        }
}
