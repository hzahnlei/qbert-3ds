#pragma once

#include "position.hpp"

namespace qbert
{

        struct Affine_Transformation_Matrix
        {
                struct Row
                {
                        float x, y, z, w;
                };

                Row row[3];

            public:
                auto apply_to(Position &) const -> void;
                auto operator()(const Position &) const -> Position;

            private:
                inline static auto dot_product(const Row &, const Position &) -> float;
        };

        auto Affine_Transformation_Matrix::dot_product(const Row &transform, const Position &vertex) -> float
        {
                return (transform.x * vertex.x) + (transform.y * vertex.y) + (transform.z * vertex.z) + transform.w;
        }

} // namespace qbert
