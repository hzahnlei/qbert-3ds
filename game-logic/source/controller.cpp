#include "controller.hpp"

#include "affine_transformation_matrix.hpp"
#include "model_to_scene_mapper.hpp"
#include "triangular_playfield.hpp"

namespace qbert
{

        Controller::Controller(Game_Model &model, View &view)
            : m_model{model}, m_view{view}, m_scene_mapper{model, m_view.colored_vertices(), m_view.textured_vertices(),
                                                           m_view.overlay_vertices()}
        {
        }

        auto Controller::game_loop() -> void
        {
                while (m_view.main_loop_should_run())
                {
                        const auto player_input{m_view.player_input()};
                        if (player_input.is_pressed(Button::SELECT))
                        {
                                break; // Exit game loop
                        }
                        else
                        {
                                handle_interocular_distance_slider(player_input);
                                handle_perspective_keys(player_input);
                                update_model(player_input);
                                update_view();
                                render_view();
                        }
                }
        }

        //---- Implementation details ----

        auto Controller::handle_interocular_distance_slider(const Player_Input &player_input) -> void
        {
                const auto curr_slider_value{player_input.stereoscopic_3d_slider};
                if (curr_slider_value != m_former_slider_value)
                {
                        m_view.adjust_interocular_distance(curr_slider_value);
                }
                m_former_slider_value = curr_slider_value;
        }

        auto Controller::handle_perspective_keys(const Player_Input &player_input) -> void
        {
                if (player_input.is_held(Button::L) && player_input.is_held(Button::R))
                {
                        m_model.reset_scene_rotation();
                }
                else if (player_input.is_held(Button::L))
                {
                        m_model.rotate_scene_clockwise();
                }
                else if (player_input.is_held(Button::R))
                {
                        m_model.rotate_scene_counter_clockwise();
                }
        }

        auto Controller::update_model(const Player_Input &player_input) -> void
        {
                m_model.update(player_input);
        }

        auto Controller::update_view() -> void
        {
                m_scene_mapper.update_scene();
                m_view.set_perspective(m_model.scene_angle_x(), m_model.scene_angle_y(), m_model.scene_angle_z());
        }

        /*!
         * This causes an interesting flicker effect (maybe for simulating an CRT?):
         * gfxSwapBuffers();
         * m_view.render();
         * gfxFlushBuffers();
         * gspWaitForVBlank();
         */
        auto Controller::render_view() -> void
        {
                m_view.render();
        }

} // namespace qbert