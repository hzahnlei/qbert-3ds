#pragma once

namespace qbert
{

        struct Position final
        {
                float x, y, z;
        };

} // namespace qbert
