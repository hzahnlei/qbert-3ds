#pragma once

namespace qbert
{

        template <typename VERTEX> class Vertex_Pool
        {
            public:
                Vertex_Pool() = default;
                Vertex_Pool(const Vertex_Pool &) = delete;
                Vertex_Pool(Vertex_Pool &&) noexcept = delete;
                Vertex_Pool &operator=(const Vertex_Pool &) = delete;
                Vertex_Pool &operator=(Vertex_Pool &&) noexcept = delete;
                virtual ~Vertex_Pool() = default;

            public:
                virtual auto acquire_vertices(const Vertex_Count vertex_count) -> VERTEX * = 0;
                virtual auto release_all_vertices() -> void = 0;
        };

} // namespace qbert