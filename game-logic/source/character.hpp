/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

#pragma once

//---- C headers
//---- C++ headers
//---- 3rd party headers
//---- Project headers
#include "game_coordinate.hpp"
#include "game_model.hpp"
#include "player_input.hpp"

namespace qbert
{

        class Character
        {

            protected:
                Character() = default;
                Character(const Character &) = default;
                Character(Character &&) noexcept = delete;
                Character &operator=(const Character &) = delete;
                Character &operator=(Character &&) noexcept = delete;
                virtual ~Character() = default;

            public:
                virtual auto interact(Game_Model &, const Player_Input & = NO_PLAYER_INPUT) -> void = 0;

                [[nodiscard]] virtual auto position() const noexcept -> const game::Coordinate & = 0;

                [[nodiscard]] virtual auto is_falling_off_playfield(const Game_Model &game) const -> bool = 0;

                [[nodiscard]] virtual auto is_on_or_above(const game::Coordinate &) const noexcept -> bool = 0;

                [[nodiscard]] virtual auto covers(const game::Coordinate &) const noexcept -> bool = 0;
        };

} // namespace qbert