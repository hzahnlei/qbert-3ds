#include "disc.hpp"

#include "assertion.hpp"
#include <cmath>
#include <memory>

namespace qbert
{

        using std::move;

        Disc::Disc(const game::Coordinate &position) : Abstract_Character{position}
        {
        }

        auto Disc::interact(Game_Model &game, const Player_Input &) -> void
        {
                switch (m_state)
                {
                        case State::INACTIVE:
                                inactive_behavior();
                                break;
                        case State::FLY_TO_NIRVANA:
                                fly_to_nirvana_behavior();
                                break;
                        case State::GO_TO_TOP:
                                fly_to_top_cube_behavior(game);
                                break;
                        case State::GO_UP:
                                fly_staight_up_behavior();
                                break;
                        case State::WAIT:
                                wait_behavior();
                                break;
                };
                Abstract_Character::interact(game);
        }

        auto Disc::is_on_or_above(const game::Coordinate &position) const noexcept -> bool
        {
                return ((position.x >= m_position.x) && (position.x < m_position.x + 1.0f)) &&
                       (position.y >= m_position.y) &&
                       ((position.z >= m_position.z) && (position.z < m_position.z + 1.0f));
        }

        auto Disc::covers(const game::Coordinate &player_position) const noexcept -> bool
        {
                return (player_position.x >= m_position.x) && (player_position.x < m_position.x + 1.0f) &&
                       (player_position.y >= m_position.y - 0.01f) && (player_position.y <= m_position.y + 0.01f) &&
                       (player_position.z >= m_position.z) && (player_position.z < m_position.z + 1.0f);
        }

        auto Disc::carry_to(const game::Coordinate &target_pos) -> void
        {
                DEV_ASSERT(State::INACTIVE != m_state, "Disc is inactive")
                DEV_ASSERT(State::WAIT == m_state, "Disc is already carrying a game object")

                m_state = State::GO_UP;

                m_carry_to_position = target_pos;

                m_carry_to_position_delta.x = target_pos.x - m_position.x;
                m_carry_to_position_delta.y = target_pos.y - m_position.y;
                m_carry_to_position_delta.z = target_pos.z - m_position.z;
                const auto max_delta = fabs(fmax(m_carry_to_position_delta.x,
                                                 fmax(m_carry_to_position_delta.y, m_carry_to_position_delta.z))) *
                                       20.0f;

                m_carry_to_position_delta.x /= max_delta;
                m_carry_to_position_delta.y /= max_delta;
                m_carry_to_position_delta.z /= max_delta;

                m_launch_position_y = m_position.y + 1.0f;
        }

        auto Disc::wait_behavior() -> void
        {
                rotate();
        }

        auto Disc::fly_staight_up_behavior() -> void
        {
                if (launch_position_is_reached())
                {
                        start_flying_to_top_cube();
                }
                else
                {
                        continue_flying_staight_up();
                }
                rotate_fast();
        }

        auto Disc::fly_to_top_cube_behavior(Game_Model &game) -> void
        {
                if (target_position_is_reached())
                {
                        start_ascending_to_nirvana(game);
                }
                else
                {
                        continue_flying_to_top_cube();
                }
                rotate_fast();
        }

        auto Disc::inactive_behavior() -> void
        {
                // Intentionally left blank
        }

        auto Disc::fly_to_nirvana_behavior() -> void
        {
                if (has_reached_nirvana()) // outside screen
                {
                        take_out_of_game();
                }
                else
                {
                        continue_flying_to_nirvana();
                }
        }

} // namespace qbert