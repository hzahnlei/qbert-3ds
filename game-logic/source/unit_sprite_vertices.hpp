/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "textured_vertex.hpp"

namespace qbert::unit::sprite
{
        constexpr Textured_Vertex VERTICES[]{// First triangle
                                             {{-0.1f, -0.1f, 0.0f}, {0.0f, 0.0f}},
                                             {{+0.1f, -0.1f, 0.0f}, {0.0f, 0.0f}},
                                             {{+0.1f, +0.1f, 0.0f}, {0.0f, 0.0f}},
                                             // Second triangle
                                             {{+0.1f, +0.1f, 0.0f}, {0.0f, 0.0f}},
                                             {{-0.1f, +0.1f, 0.0f}, {0.0f, 0.0f}},
                                             {{-0.1f, -0.1f, 0.0f}, {0.0f, 0.0f}}};

        constexpr auto VERTICES_COUNT{sizeof(VERTICES) / sizeof(VERTICES[0])};

        constexpr auto MEM_SIZE{sizeof(VERTICES)};
} // namespace qbert::unit::sprite

namespace qbert::unit::tall_sprite
{
        constexpr Textured_Vertex VERTICES[]{// First triangle
                                             {{-0.1f, -0.2f, 0.0f}, {0.0f, 0.0f}},
                                             {{+0.1f, -0.2f, 0.0f}, {0.0f, 0.0f}},
                                             {{+0.1f, +0.2f, 0.0f}, {0.0f, 0.0f}},
                                             // Second triangle
                                             {{+0.1f, +0.2f, 0.0f}, {0.0f, 0.0f}},
                                             {{-0.1f, +0.2f, 0.0f}, {0.0f, 0.0f}},
                                             {{-0.1f, -0.2f, 0.0f}, {0.0f, 0.0f}}};

        constexpr auto VERTICES_COUNT{sizeof(VERTICES) / sizeof(VERTICES[0])};

        constexpr auto MEM_SIZE{sizeof(VERTICES)};
} // namespace qbert::unit::tall_sprite

namespace qbert::unit::balloon_sprite
{
        constexpr Textured_Vertex VERTICES[]{// First triangle
                                             {{-0.3f, -0.2f, 0.0f}, {0.0f, 0.0f}},
                                             {{+0.3f, -0.2f, 0.0f}, {0.0f, 0.0f}},
                                             {{+0.3f, +0.2f, 0.0f}, {0.0f, 0.0f}},
                                             // Second triangle
                                             {{+0.3f, +0.2f, 0.0f}, {0.0f, 0.0f}},
                                             {{-0.3f, +0.2f, 0.0f}, {0.0f, 0.0f}},
                                             {{-0.3f, -0.2f, 0.0f}, {0.0f, 0.0f}}};

        constexpr auto VERTICES_COUNT{sizeof(VERTICES) / sizeof(VERTICES[0])};

        constexpr auto MEM_SIZE{sizeof(VERTICES)};
} // namespace qbert::unit::balloon_sprite
