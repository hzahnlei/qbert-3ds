#include "game_coordinate.hpp"

#include <cmath>

namespace qbert::game
{

        using std::roundf;

        auto Coordinate::operator==(const Coordinate &other) const noexcept -> bool
        {
                return (this->x == other.x) && (this->y == other.y) && (this->z == other.z);
        }

        auto Coordinate::quantize() noexcept -> void
        {
                x = roundf(x);
                y = roundf(y);
                z = roundf(z);
        }

        auto operator<<(ostream &out, const Coordinate coordinate) -> ostream &
        {
                return out << "game::Coordinate{.x=" << coordinate.x << ", .y=" << coordinate.y
                           << ", .z=" << coordinate.z << "}";
        }

} // namespace qbert::game
