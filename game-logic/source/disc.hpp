#pragma once

#include "abstract_character.hpp"
#include "transformations.hpp"

namespace qbert
{

        class Disc final : public Abstract_Character
        {
            private:
                Radians m_rotation_rad{0.0_rad};
                game::Coordinate m_carry_to_position{0.0f, 0.0f, 0.0f};
                game::Coordinate m_carry_to_position_delta{0.0f, 0.0f, 0.0f};
                float m_launch_position_y{0.0f};

                enum class State
                {
                        INACTIVE,
                        FLY_TO_NIRVANA,
                        GO_TO_TOP,
                        GO_UP,
                        WAIT,
                };
                State m_state{State::WAIT};

            public:
                Disc(const game::Coordinate &);

            public:
                auto interact(Game_Model &, const Player_Input & = NO_PLAYER_INPUT) -> void override;

                [[nodiscard]] inline auto rotation() const noexcept -> Radians;

                [[nodiscard]] auto is_on_or_above(const game::Coordinate &) const noexcept -> bool override;

                [[nodiscard]] auto covers(const game::Coordinate &) const noexcept -> bool override;

                auto carry_to(const game::Coordinate &) -> void;

                [[nodiscard]] inline auto can_be_used_for_surfing() const noexcept -> bool;

            private:
                auto wait_behavior() -> void;
                auto fly_staight_up_behavior() -> void;
                inline auto continue_flying_staight_up() noexcept -> void;
                inline auto start_flying_to_top_cube() noexcept -> void;
                auto fly_to_top_cube_behavior(Game_Model &) -> void;
                inline auto start_ascending_to_nirvana(Game_Model &) -> void;
                inline auto continue_flying_to_top_cube() noexcept -> void;
                auto fly_to_nirvana_behavior() -> void;
                auto inactive_behavior() -> void;
                inline auto has_reached_nirvana() const noexcept -> bool;
                inline auto take_out_of_game() noexcept -> void;
                inline auto continue_flying_to_nirvana() noexcept -> void;

                inline auto rotate() -> void;

                inline auto rotate_fast() -> void;

                inline auto target_position_is_reached() const noexcept -> bool;

                inline auto launch_position_is_reached() const noexcept -> bool;
        };

        auto Disc::can_be_used_for_surfing() const noexcept -> bool
        {
                return State::WAIT == m_state;
        }

        auto Disc::continue_flying_staight_up() noexcept -> void
        {
                m_position.y += m_carry_to_position_delta.y;
        }

        auto Disc::start_flying_to_top_cube() noexcept -> void
        {
                m_state = State::GO_TO_TOP;
        }

        auto Disc::start_ascending_to_nirvana(Game_Model &game) -> void
        {
                m_state = State::FLY_TO_NIRVANA;
                m_carry_to_position = game::Coordinate{0.0f, 0.0f, 0.0f};
                m_carry_to_position_delta = game::Coordinate{0.0f, 0.0f, 0.0f};
                m_launch_position_y = 0.0f;
                game.have_qbert_jump_off_disc_to_playfield();
        }

        auto Disc::continue_flying_to_top_cube() noexcept -> void
        {
                m_position.x += m_carry_to_position_delta.x;
                if (m_position.y < m_carry_to_position.y)
                {
                        m_position.y += m_carry_to_position_delta.y;
                }
                m_position.z += m_carry_to_position_delta.z;
        }

        auto Disc::has_reached_nirvana() const noexcept -> bool
        {
                return m_position.y >= 3.0f;
        }

        auto Disc::take_out_of_game() noexcept -> void
        {
                m_state = State::INACTIVE;
        }

        auto Disc::continue_flying_to_nirvana() noexcept -> void
        {
                m_position.y += 0.1f;
                rotate_fast();
        }

        auto Disc::rotation() const noexcept -> Radians
        {
                return m_rotation_rad;
        }

        auto Disc::rotate() -> void
        {
                m_rotation_rad += 0.05_rad;
        }

        auto Disc::rotate_fast() -> void
        {
                m_rotation_rad += 0.20_rad;
        }

        constexpr auto EPSILON{0.01f};

        auto Disc::target_position_is_reached() const noexcept -> bool
        {
                return (fabs(m_position.x - m_carry_to_position.x) < EPSILON) &&
                       (fabs(m_position.y - m_carry_to_position.y) < EPSILON) &&
                       (fabs(m_position.z - m_carry_to_position.z) < EPSILON);
        }

        auto Disc::launch_position_is_reached() const noexcept -> bool
        {
                return fabs(m_position.y - m_launch_position_y) < EPSILON;
        }

} // namespace qbert