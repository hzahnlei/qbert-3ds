#include "model_to_scene_mapper.hpp"

#include "disc_to_scene_mapper.hpp"
#include "overlay_to_scene_mapper.hpp"
#include "playfield_to_scene_mapper.hpp"
#include "screen_dimensions.hpp"
#include "sprite_to_scene_mapper.hpp"
#include "transformations.hpp"
#include "triangular_playfield.hpp"
#include "unit_cube_vertices.hpp"
#include "unit_disc_vertices.hpp"
#include "unit_sprite_vertices.hpp"
#include <algorithm>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <vector>

namespace qbert
{

        using std::size_t;
        using std::vector;

        Model_to_Scene_Mapper::Model_to_Scene_Mapper(Game_Model &game, Vertex_Pool<Colored_Vertex> &colored_vertices,
                                                     Vertex_Pool<Textured_Vertex> &textured_vertices,
                                                     Vertex_Pool<Textured_Colored_Vertex> &overlay_vertices)
            : m_game{game}, m_colored_vertices{colored_vertices}, m_textured_vertices{textured_vertices},
              m_overlay_vertices{overlay_vertices}
        {
                init_scene();
        }

        auto Model_to_Scene_Mapper::update_scene() -> void
        {
                init_scene();
        }

        //--- init scene ---

        auto Model_to_Scene_Mapper::init_scene() -> void
        {
                m_colored_vertices.release_all_vertices();
                m_textured_vertices.release_all_vertices();
                m_overlay_vertices.release_all_vertices();
                playfield_to_scene::init_vertices(m_game.playfield().cubes(), m_colored_vertices);
                disc_to_scene::init_vertices(m_game.discs(), m_colored_vertices);
                sprite_to_scene::init_coily_vertices(m_game.coily(), m_textured_vertices);
                sprite_to_scene::init_qbert_vertices(m_game.player(), m_textured_vertices);
                overlay_to_scene::init_vertices(m_game, m_overlay_vertices);
        }

} // namespace qbert