#pragma once

#include "attract_mode.hpp"
#include "coily.hpp"
#include "disc.hpp"
#include "game_mode.hpp"
#include "game_model.hpp"
#include "qbert_player.hpp"

namespace qbert
{

        /*!
         * An implementation of the game that gets work done by delegation.
         */
        class Game_Model_Delegating_Impl final : public Game_Model
        {

            private:
                Level_Number m_level_number{1};
                Round_Number m_round_number{1};
                State m_state{State::ATTRACT_MODE};
                Attract_Mode m_attract_mode;
                Game_Mode m_game_mode;
                Game_Model *m_current_mode{nullptr};

            public:
                Game_Model_Delegating_Impl();

                auto rotate_scene_clockwise() -> void override;
                auto rotate_scene_counter_clockwise() -> void override;
                auto reset_scene_rotation() -> void override;
                [[nodiscard]] inline auto scene_angle_x() const noexcept -> Scene_Angle override;
                [[nodiscard]] inline auto scene_angle_y() const noexcept -> Scene_Angle override;
                [[nodiscard]] inline auto scene_angle_z() const noexcept -> Scene_Angle override;

                [[nodiscard]] inline auto playfield() const noexcept -> const Playfield & override;

                [[nodiscard]] inline auto discs() const noexcept -> const vector<Disc> & override;
                [[nodiscard]] inline auto disc_at(const game::Coordinate &) const -> const Disc * override;
                inline auto attempt_to_attach_player_to(const Disc &) -> bool override;
                inline auto have_qbert_jump_off_disc_to_playfield() -> void override;

                [[nodiscard]] inline auto player() const noexcept -> const Qbert_Player & override;
                [[nodiscard]] inline auto player_count() const noexcept -> Player_Count override;
                inline auto player_died() noexcept -> void override;

                [[nodiscard]] inline auto coily() const noexcept -> const Coily & override;

                [[nodiscard]] inline auto score() const noexcept -> Score override;
                [[nodiscard]] inline auto hi_score() const noexcept -> Score override;

                [[nodiscard]] inline auto level_number() const noexcept -> Level_Number override;
                [[nodiscard]] inline auto round_number() const noexcept -> Round_Number override;

                [[nodiscard]] inline auto state() const noexcept -> State override;

                inline auto touch(const Cube &) -> bool override;

                [[nodiscard]] inline auto is_within_playfield(const game::Coordinate &) const -> bool override;

                auto update(const Player_Input &) -> void override;

            private:
                auto start_new_game() -> void;
                inline auto playing_already() const noexcept -> bool;
        };

        auto Game_Model_Delegating_Impl::scene_angle_x() const noexcept -> Scene_Angle
        {
                return m_current_mode->scene_angle_x();
        }

        auto Game_Model_Delegating_Impl::scene_angle_y() const noexcept -> Scene_Angle
        {
                return m_current_mode->scene_angle_y();
        }

        auto Game_Model_Delegating_Impl::scene_angle_z() const noexcept -> Scene_Angle
        {
                return m_current_mode->scene_angle_z();
        }

        auto Game_Model_Delegating_Impl::playfield() const noexcept -> const Playfield &
        {
                return m_current_mode->playfield();
        }

        auto Game_Model_Delegating_Impl::discs() const noexcept -> const vector<Disc> &
        {
                return m_current_mode->discs();
        }

        auto Game_Model_Delegating_Impl::disc_at(const game::Coordinate &position) const -> const Disc *
        {
                return m_current_mode->disc_at(position);
        }

        auto Game_Model_Delegating_Impl::attempt_to_attach_player_to(const Disc &disc) -> bool
        {
                return m_current_mode->attempt_to_attach_player_to(disc);
        }

        auto Game_Model_Delegating_Impl::have_qbert_jump_off_disc_to_playfield() -> void
        {
                m_current_mode->have_qbert_jump_off_disc_to_playfield();
        }

        auto Game_Model_Delegating_Impl::player() const noexcept -> const Qbert_Player &
        {
                return m_current_mode->player();
        }

        auto Game_Model_Delegating_Impl::player_count() const noexcept -> Player_Count
        {
                return m_current_mode->player_count();
        }

        auto Game_Model_Delegating_Impl::player_died() noexcept -> void
        {
                m_current_mode->player_died();
        }

        auto Game_Model_Delegating_Impl::coily() const noexcept -> const Coily &
        {
                return m_current_mode->coily();
        }

        auto Game_Model_Delegating_Impl::score() const noexcept -> Score
        {
                return m_current_mode->score();
        }

        auto Game_Model_Delegating_Impl::hi_score() const noexcept -> Score
        {
                return m_current_mode->hi_score();
        }

        auto Game_Model_Delegating_Impl::level_number() const noexcept -> Level_Number
        {
                return m_current_mode->level_number();
        }

        auto Game_Model_Delegating_Impl::round_number() const noexcept -> Round_Number
        {
                return m_current_mode->round_number();
        }

        auto Game_Model_Delegating_Impl::state() const noexcept -> State
        {
                return m_state;
        }

        auto Game_Model_Delegating_Impl::touch(const Cube &cube) -> bool
        {
                return m_current_mode->touch(cube.position());
        }

        auto Game_Model_Delegating_Impl::is_within_playfield(const game::Coordinate &position) const -> bool
        {
                return m_current_mode->is_within_playfield(position);
        }

        auto Game_Model_Delegating_Impl::playing_already() const noexcept -> bool
        {
                return m_state == State::PLAY;
        }

} // namespace qbert
