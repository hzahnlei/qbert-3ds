#pragma once

#include "abstract_playable_mode.hpp"
#include "coily.hpp"
#include "disc.hpp"
#include "position.hpp"
#include "qbert_player.hpp"

namespace qbert
{

        class Attract_Mode final : public Abstract_Playable_Mode
        {
            private:
                size_t m_pass{0};

            public:
                [[nodiscard]] inline auto scene_angle_y() const noexcept -> Scene_Angle;

                auto rotate_scene_clockwise() -> void override;
                auto rotate_scene_counter_clockwise() -> void override;
                auto reset_scene_rotation() -> void override;

                [[nodiscard]] inline auto state() const noexcept -> State override;

                auto update(const Player_Input &) -> void override;
        };

        auto Attract_Mode::scene_angle_y() const noexcept -> Scene_Angle
        {
                return DEFAULT_SCENE_ROTATION_Y;
        }

        auto Attract_Mode::state() const noexcept -> State
        {
                return State::ATTRACT_MODE;
        }

} // namespace qbert