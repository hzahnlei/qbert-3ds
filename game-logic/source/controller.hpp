#pragma once

#include "game_model_delegating_impl.hpp"
#include "model_to_scene_mapper.hpp"
#include "view.hpp"

namespace qbert
{

        class Controller final
        {
            private:
                Game_Model &m_model;
                View &m_view;
                float m_former_slider_value{-1.0f};
                Model_to_Scene_Mapper m_scene_mapper;

            public:
                Controller(Game_Model &, View &);

            public:
                auto game_loop() -> void;

            private:
                auto handle_interocular_distance_slider(const Player_Input &) -> void;
                auto handle_perspective_keys(const Player_Input &) -> void;

                auto update_model(const Player_Input &) -> void;

                auto update_view() -> void;

                auto render_view() -> void;
        };

} // namespace qbert