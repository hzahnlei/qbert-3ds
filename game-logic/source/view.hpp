#pragma once

#include "colored_vertex.hpp"
#include "player_input.hpp"
#include "textured_colored_vertex.hpp"
#include "textured_vertex.hpp"
#include "transformations.hpp"
#include "vertex_pool.hpp"

namespace qbert
{

        class View
        {

            public:
                View() = default;
                View(const View &) = delete;
                View(View &&) noexcept = delete;
                View &operator=(const View &) = delete;
                View &operator=(View &&) noexcept = delete;
                virtual ~View() = default;

            public:
                virtual auto set_perspective(const Degrees angle_x, const Degrees angle_y, const Degrees angle_z)
                    -> void = 0;
                virtual auto adjust_interocular_distance(const float) -> void = 0;

                virtual auto colored_vertices() const noexcept -> Vertex_Pool<Colored_Vertex> & = 0;
                virtual auto textured_vertices() const noexcept -> Vertex_Pool<Textured_Vertex> & = 0;
                virtual auto overlay_vertices() const noexcept -> Vertex_Pool<Textured_Colored_Vertex> & = 0;

                virtual auto render() -> void = 0;

                virtual auto main_loop_should_run() const noexcept -> bool = 0;
                virtual auto player_input() const noexcept -> Player_Input = 0;
        };

} // namespace qbert