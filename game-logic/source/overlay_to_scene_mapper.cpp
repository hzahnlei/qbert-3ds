#include "overlay_to_scene_mapper.hpp"

#include "color.hpp"
#include <cmath>
#include <string>

namespace qbert::overlay_to_scene
{

        using std::max;
        using std::string;

        auto number_of_digits(unsigned int number) -> unsigned int
        {
                auto count{0U};
                do
                {
                        number /= 10;
                        count++;
                } while (number > 0);
                return count;
        }

        using Overlay_Position_X = unsigned int;
        using Overlay_Position_Y = unsigned int;

        auto put_char(const Overlay_Position_X x, const Overlay_Position_Y y, const Color &color, const char glyph,
                      Vertex_Pool<Textured_Colored_Vertex> &overlay_vertices) -> void
        {
                auto vertices{overlay_vertices.acquire_vertices(6)};
                const auto pixel_pos_x{static_cast<float>(x * font::PIXEL_COUNT_U)};
                const auto pixel_pos_y{static_cast<float>(y * font::PIXEL_COUNT_V)};
                const auto tex_coord_u{u_coordinate_of_ascii_char(glyph)};
                const auto tex_coord_v{v_coordinate_of_ascii_char(glyph)};
                // 1st triangle, upper left
                vertices[0].position = {pixel_pos_x, pixel_pos_y, 1.0f};
                vertices[0].texture_coordinate.u = tex_coord_u;
                vertices[0].texture_coordinate.v = tex_coord_v;
                vertices[0].color = color;
                vertices[1].position = {pixel_pos_x + font::PIXEL_COUNT_U, pixel_pos_y + font::PIXEL_COUNT_V, 1.0f};
                vertices[1].texture_coordinate.u = tex_coord_u + font::UNITS_U;
                vertices[1].texture_coordinate.v = tex_coord_v + font::UNITS_V;
                vertices[1].color = color;
                vertices[2].position = {pixel_pos_x, pixel_pos_y + font::PIXEL_COUNT_V, 1.0f};
                vertices[2].texture_coordinate.u = tex_coord_u;
                vertices[2].texture_coordinate.v = tex_coord_v + font::UNITS_V;
                vertices[2].color = color;
                // 2nd triangle, lower right
                vertices[3].position = {pixel_pos_x, pixel_pos_y, 1.0f};
                vertices[3].texture_coordinate.u = tex_coord_u;
                vertices[3].texture_coordinate.v = tex_coord_v;
                vertices[3].color = color;
                vertices[4].position = {pixel_pos_x + font::PIXEL_COUNT_U, pixel_pos_y, 1.0f};
                vertices[4].texture_coordinate.u = tex_coord_u + font::UNITS_U;
                vertices[4].texture_coordinate.v = tex_coord_v;
                vertices[4].color = color;
                vertices[5].position = {pixel_pos_x + font::PIXEL_COUNT_U, pixel_pos_y + font::PIXEL_COUNT_V, 1.0f};
                vertices[5].texture_coordinate.u = tex_coord_u + font::UNITS_U;
                vertices[5].texture_coordinate.v = tex_coord_v + font::UNITS_V;
                vertices[5].color = color;
        }

        auto put_text(const Overlay_Position_X x, const Overlay_Position_Y y, const Color &color, const string &text,
                      Vertex_Pool<Textured_Colored_Vertex> &overlay_vertices) -> void
        {
                auto x_offset{0U};
                for (const auto ascii_char : text)
                {
                        put_char(x + x_offset, y, color, ascii_char, overlay_vertices);
                        x_offset++;
                }
        }

        auto put_number_right_alligned(const Overlay_Position_X x, const Overlay_Position_Y y, const Color &color,
                                       unsigned int number, Vertex_Pool<Textured_Colored_Vertex> &overlay_vertices)
            -> Overlay_Position_X
        {
                auto x_offset{0U};
                do
                {
                        const auto rest{number / 10};
                        const auto digit{number - (rest * 10)};
                        number /= 10;
                        put_char(x + x_offset, y, color, digit + '0', overlay_vertices);
                        x_offset--;
                } while (number > 0);
                return x + x_offset;
        }

        enum class Flip
        {
                NONE,
                X,
                Y,
                XY
        };

        auto put_shape_unflipped(const Position &pos, const Color &color, const unsigned int sprite_id,
                                 Vertex_Pool<Textured_Colored_Vertex> &overlay_vertices) -> void
        {
                auto vertices{overlay_vertices.acquire_vertices(6)};
                const auto tex_coord_u{u_coordinate_of_shape(sprite_id)};
                const auto tex_coord_v{v_coordinate_of_shape(sprite_id)};
                // 1st triangle, upper left
                vertices[0].position = pos;
                vertices[0].texture_coordinate.u = tex_coord_u;
                vertices[0].texture_coordinate.v = tex_coord_v;
                vertices[0].color = color;
                vertices[1].position = {pos.x + sprite::PIXEL_COUNT_U, pos.y + sprite::PIXEL_COUNT_V, pos.z};
                vertices[1].texture_coordinate.u = tex_coord_u + sprite::UNITS_U;
                vertices[1].texture_coordinate.v = tex_coord_v + sprite::UNITS_V;
                vertices[1].color = color;
                vertices[2].position = {pos.x, pos.y + sprite::PIXEL_COUNT_U, pos.z};
                vertices[2].texture_coordinate.u = tex_coord_u;
                vertices[2].texture_coordinate.v = tex_coord_v + sprite::UNITS_V;
                vertices[2].color = color;
                // 2nd triangle, lower right
                vertices[3].position = pos;
                vertices[3].texture_coordinate.u = tex_coord_u;
                vertices[3].texture_coordinate.v = tex_coord_v;
                vertices[3].color = color;
                vertices[4].position = {pos.x + sprite::PIXEL_COUNT_U, pos.y, pos.z};
                vertices[4].texture_coordinate.u = tex_coord_u + sprite::UNITS_U;
                vertices[4].texture_coordinate.v = tex_coord_v;
                vertices[4].color = color;
                vertices[5].position = {pos.x + sprite::PIXEL_COUNT_U, pos.y + sprite::PIXEL_COUNT_V, pos.z};
                vertices[5].texture_coordinate.u = tex_coord_u + sprite::UNITS_U;
                vertices[5].texture_coordinate.v = tex_coord_v + sprite::UNITS_V;
                vertices[5].color = color;
        }

        auto put_shape_flipped_x(const Position &, const Color &, const unsigned int,
                                 Vertex_Pool<Textured_Colored_Vertex> &) -> void
        {
                throw std::runtime_error{"put_shape_flipped_x not supported"};
        }

        auto put_shape_flipped_y(const Position &pos, const Color &color, const unsigned int sprite_id,
                                 Vertex_Pool<Textured_Colored_Vertex> &overlay_vertices) -> void
        {
                auto vertices{overlay_vertices.acquire_vertices(6)};
                const auto tex_coord_u{u_coordinate_of_shape(sprite_id)};
                const auto tex_coord_v{v_coordinate_of_shape(sprite_id)};
                // 1st triangle, upper left
                vertices[0].position = pos;
                vertices[0].texture_coordinate.u = tex_coord_u + sprite::UNITS_U;
                vertices[0].texture_coordinate.v = tex_coord_v;
                vertices[0].color = color;
                vertices[1].position = {pos.x + sprite::PIXEL_COUNT_U, pos.y + sprite::PIXEL_COUNT_V, pos.z};
                vertices[1].texture_coordinate.u = tex_coord_u;
                vertices[1].texture_coordinate.v = tex_coord_v + sprite::UNITS_V;
                vertices[1].color = color;
                vertices[2].position = {pos.x, pos.y + sprite::PIXEL_COUNT_U, pos.z};
                vertices[2].texture_coordinate.u = tex_coord_u + sprite::UNITS_U;
                vertices[2].texture_coordinate.v = tex_coord_v + sprite::UNITS_V;
                vertices[2].color = color;
                // 2nd triangle, lower right
                vertices[3].position = pos;
                vertices[3].texture_coordinate.u = tex_coord_u + sprite::UNITS_U;
                vertices[3].texture_coordinate.v = tex_coord_v;
                vertices[3].color = color;
                vertices[4].position = {pos.x + sprite::PIXEL_COUNT_U, pos.y, pos.z};
                vertices[4].texture_coordinate.u = tex_coord_u;
                vertices[4].texture_coordinate.v = tex_coord_v;
                vertices[4].color = color;
                vertices[5].position = {pos.x + sprite::PIXEL_COUNT_U, pos.y + sprite::PIXEL_COUNT_V, pos.z};
                vertices[5].texture_coordinate.u = tex_coord_u;
                vertices[5].texture_coordinate.v = tex_coord_v + sprite::UNITS_V;
                vertices[5].color = color;
        }

        auto put_shape_flipped_x_y(const Position &, const Color &, const unsigned int,
                                   Vertex_Pool<Textured_Colored_Vertex> &) -> void
        {
                throw std::runtime_error{"put_shape_flipped_x_y not supported"};
        }

        auto put_shape(const Position &pos, const Color &color, const unsigned int sprite_id, const Flip flip,
                       Vertex_Pool<Textured_Colored_Vertex> &overlay_vertices) -> void
        {
                switch (flip)
                {
                        case Flip::NONE:
                                put_shape_unflipped(pos, color, sprite_id, overlay_vertices);
                                return;
                        case Flip::X:
                                put_shape_flipped_x(pos, color, sprite_id, overlay_vertices);
                                return;
                        case Flip::XY:
                                put_shape_flipped_x_y(pos, color, sprite_id, overlay_vertices);
                                return;
                        case Flip::Y:
                                put_shape_flipped_y(pos, color, sprite_id, overlay_vertices);
                                return;
                }
        }

        static auto frame_count{0U};
        static auto anim{0U};

        auto init_vertices(const Game_Model &game, Vertex_Pool<Textured_Colored_Vertex> &vertices) -> void
        {
                const auto max_digit_count{number_of_digits(max(game.score(), game.hi_score()))};
                put_text(0, 29, Palette::PURPLE, "HI-SCORE:", vertices);
                put_number_right_alligned(10 + max_digit_count - 1, 29, Palette::ORANGE, game.hi_score(), vertices);

                put_text(3, 28, Palette::MAGENTA, "SCORE:", vertices);
                put_number_right_alligned(10 + max_digit_count - 1, 28, Palette::YELLOW, game.score(), vertices);

                put_text(0, 26, Palette::RED, "CHANGE TO:", vertices);

                auto new_x{put_number_right_alligned(49, 26, Palette::ORANGE, game.level_number(), vertices)};
                put_text(new_x - 6, 26, Palette::GREEN, "LEVEL:", vertices);

                new_x = put_number_right_alligned(49, 25, Palette::ORANGE, game.round_number(), vertices);
                put_text(new_x - 6, 25, Palette::PINK, "ROUND:", vertices);

                static constexpr int ANIM_PHASES[4]{0, -1, 0, +1};
                const auto curr_anim{ANIM_PHASES[anim % 4]};

                put_shape({0.0f + curr_anim, 192.0f, 0.0f}, Palette::PINK, 66, Flip::NONE, vertices);
                put_shape({16.0f + curr_anim, 192.0f, 0.0f}, Palette::PINK, 66, Flip::NONE, vertices);
                put_shape({32.0f, 192.0f, 2.0f}, Palette::YELLOW, 82, Flip::NONE, vertices);
                put_shape({48.0f + (-curr_anim), 192.0f, 0.0f}, Palette::PINK, 66, Flip::Y, vertices);
                put_shape({64.0f + (-curr_anim), 192.0f, 0.0f}, Palette::PINK, 66, Flip::Y, vertices);

                if (0 == frame_count % 10)
                {
                        anim++;
                }
                frame_count++;

                for (auto count{0U}; count < game.player_count(); count++)
                {
                        auto x{count % 4};
                        auto y{count / 4};
                        put_shape({0.0f + (10 * x), 160.0f - (10 * y), 0.0f}, Palette::WHITE, 48, Flip::NONE, vertices);
                }
        }

} // namespace qbert::overlay_to_scene
