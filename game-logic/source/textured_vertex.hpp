#pragma once

#include "position.hpp"
#include "vertex.hpp"

namespace qbert
{

        struct Texture_Coordinate final
        {
                float u, v;
        };

        struct Textured_Vertex final
        {
                Position position;
                Texture_Coordinate texture_coordinate;

                inline static constexpr auto byte_count() noexcept -> size_t;
                inline static constexpr auto byte_count(const Vertex_Count) noexcept -> size_t;
                inline static constexpr auto component_count() noexcept -> size_t;
        };

        constexpr auto Textured_Vertex::byte_count() noexcept -> size_t
        {
                return sizeof(Textured_Vertex);
        }

        constexpr auto Textured_Vertex::byte_count(const Vertex_Count vertex_count) noexcept -> size_t
        {
                return vertex_count * Textured_Vertex::byte_count();
        }

        constexpr auto Textured_Vertex::component_count() noexcept -> size_t
        {
                return 2;
        }

} // namespace qbert
