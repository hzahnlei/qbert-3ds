#pragma once

#include <ostream>

namespace qbert::game
{

        using std::ostream;

        struct Coordinate final
        {
                float x, y, z;

                [[nodiscard]] auto operator==(const Coordinate &) const noexcept -> bool;
                auto quantize() noexcept -> void;
        };

        /*!
         * Stream operators can help debugging. For example: Catch2 uses them to print expected vs. actual in case of
         * violation of test condition.
         */
        auto operator<<(ostream &, const Coordinate) -> ostream &;

} // namespace qbert::game