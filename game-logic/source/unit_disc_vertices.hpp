/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "colored_vertex.hpp"

namespace qbert::unit::disc
{

        constexpr auto RADIUS{0.1f};
        constexpr auto HIGHT{RADIUS * 0.3f};

        constexpr Colored_Vertex VERTICES[]{
            //---- Segment #1 ----
            //---- DISC ----
            {{0.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::GREEN},
            {{0.0000f * RADIUS, 0.0000f, 1.0000f * RADIUS}, Palette::GREEN},
            {{0.5000f * RADIUS, 0.0000f, 0.8660f * RADIUS}, Palette::GREEN},
            //---- RIM ----
            {{0.0000f * RADIUS, 0.0000f, 1.0000f * RADIUS}, Palette::DARK_GREEN},
            {{0.0000f * RADIUS, 0.0000f - HIGHT, 1.0000f * RADIUS}, Palette::DARK_GREEN},
            {{0.5000f * RADIUS, 0.0000f, 0.8660f * RADIUS}, Palette::DARK_GREEN},
            //----
            {{0.5000f * RADIUS, 0.0000f - HIGHT, 0.8660f * RADIUS}, Palette::DARK_GREEN},
            {{0.5000f * RADIUS, 0.0000f, 0.8660f * RADIUS}, Palette::DARK_GREEN},
            {{0.0000f * RADIUS, 0.0000f - HIGHT, 1.0000f * RADIUS}, Palette::DARK_GREEN},
            //---- Segment #2 ----
            //---- DISC ----
            {{0.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::GREEN},
            {{0.5000f * RADIUS, 0.0000f, 0.8660f * RADIUS}, Palette::GREEN},
            {{0.8660f * RADIUS, 0.0000f, 0.5000f * RADIUS}, Palette::GREEN},
            //---- RIM ----
            {{0.5000f * RADIUS, 0.0000f, 0.8660f * RADIUS}, Palette::DARK_GREEN},
            {{0.5000f * RADIUS, 0.0000f - HIGHT, 0.8660f * RADIUS}, Palette::DARK_GREEN},
            {{0.8660f * RADIUS, 0.0000f, 0.5000f * RADIUS}, Palette::DARK_GREEN},
            //----
            {{0.8660f * RADIUS, 0.0000f - HIGHT, 0.5000f * RADIUS}, Palette::DARK_GREEN},
            {{0.8660f * RADIUS, 0.0000f, 0.5000f * RADIUS}, Palette::DARK_GREEN},
            {{0.5000f * RADIUS, 0.0000f - HIGHT, 0.8660f * RADIUS}, Palette::DARK_GREEN},
            //---- Segment #3 ----
            //---- DISC ----
            {{0.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::GREEN},
            {{0.8660f * RADIUS, 0.0000f, 0.5000f * RADIUS}, Palette::GREEN},
            {{1.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::GREEN},
            //---- RIM ----
            {{0.8660f * RADIUS, 0.0000f, 0.5000f * RADIUS}, Palette::DARK_GREEN},
            {{0.8660f * RADIUS, 0.0000f - HIGHT, 0.5000f * RADIUS}, Palette::DARK_GREEN},
            {{1.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::DARK_GREEN},
            //----
            {{1.0000f * RADIUS, 0.0000f - HIGHT, 0.0000f * RADIUS}, Palette::DARK_GREEN},
            {{1.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::DARK_GREEN},
            {{0.8660f * RADIUS, 0.0000f - HIGHT, 0.5000f * RADIUS}, Palette::DARK_GREEN},
            //---- Segment #4 ----
            //---- DISC ----
            {{0.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::RED},
            {{1.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::RED},
            {{0.8660f * RADIUS, 0.0000f, -0.5000f * RADIUS}, Palette::RED},
            //---- RIM ----
            {{1.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::DARK_RED},
            {{1.0000f * RADIUS, 0.0000f - HIGHT, 0.0000f * RADIUS}, Palette::DARK_RED},
            {{0.8660f * RADIUS, 0.0000f, -0.5000f * RADIUS}, Palette::DARK_RED},
            //----
            {{0.8660f * RADIUS, 0.0000f - HIGHT, -0.5000f * RADIUS}, Palette::DARK_RED},
            {{0.8660f * RADIUS, 0.0000f, -0.5000f * RADIUS}, Palette::DARK_RED},
            {{1.0000f * RADIUS, 0.0000f - HIGHT, 0.0000f * RADIUS}, Palette::DARK_RED},
            //---- Segment #5 ----
            //---- DISC ----
            {{0.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::RED},
            {{0.8660f * RADIUS, 0.0000f, -0.5000f * RADIUS}, Palette::RED},
            {{0.5000f * RADIUS, 0.0000f, -0.8660f * RADIUS}, Palette::RED},
            //---- RIM ----
            {{0.8660f * RADIUS, 0.0000f, -0.5000f * RADIUS}, Palette::DARK_RED},
            {{0.8660f * RADIUS, 0.0000f - HIGHT, -0.5000f * RADIUS}, Palette::DARK_RED},
            {{0.5000f * RADIUS, 0.0000f, -0.8660f * RADIUS}, Palette::DARK_RED},
            //----
            {{0.5000f * RADIUS, 0.0000f - HIGHT, -0.8660f * RADIUS}, Palette::DARK_RED},
            {{0.5000f * RADIUS, 0.0000f, -0.8660f * RADIUS}, Palette::DARK_RED},
            {{0.8660f * RADIUS, 0.0000f - HIGHT, -0.5000f * RADIUS}, Palette::DARK_RED},
            //---- Segment #6 ----
            //---- DISC ----
            {{0.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::RED},
            {{0.5000f * RADIUS, 0.0000f, -0.8660f * RADIUS}, Palette::RED},
            {{0.0000f * RADIUS, 0.0000f, -1.0000f * RADIUS}, Palette::RED},
            //---- RIM ----
            {{0.5000f * RADIUS, 0.0000f, -0.8660f * RADIUS}, Palette::DARK_RED},
            {{0.5000f * RADIUS, 0.0000f - HIGHT, -0.8660f * RADIUS}, Palette::DARK_RED},
            {{0.0000f * RADIUS, 0.0000f, -1.0000f * RADIUS}, Palette::DARK_RED},
            //----
            {{0.0000f * RADIUS, 0.0000f - HIGHT, -1.0000f * RADIUS}, Palette::DARK_RED},
            {{0.0000f * RADIUS, 0.0000f, -1.0000f * RADIUS}, Palette::DARK_RED},
            {{0.5000f * RADIUS, 0.0000f - HIGHT, -0.8660f * RADIUS}, Palette::DARK_RED},
            //---- Segment #7 ----
            //---- DISC ----
            {{0.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::BLUE},
            {{0.0000f * RADIUS, 0.0000f, -1.0000f * RADIUS}, Palette::BLUE},
            {{-0.5000f * RADIUS, 0.0000f, -0.8660f * RADIUS}, Palette::BLUE},
            //---- RIM ----
            {{0.0000f * RADIUS, 0.0000f, -1.0000f * RADIUS}, Palette::DARK_BLUE},
            {{0.0000f * RADIUS, 0.0000f - HIGHT, -1.0000f * RADIUS}, Palette::DARK_BLUE},
            {{-0.5000f * RADIUS, 0.0000f, -0.8660f * RADIUS}, Palette::DARK_BLUE},
            //----
            {{-0.5000f * RADIUS, 0.0000f - HIGHT, -0.8660f * RADIUS}, Palette::DARK_BLUE},
            {{-0.5000f * RADIUS, 0.0000f, -0.8660f * RADIUS}, Palette::DARK_BLUE},
            {{0.0000f * RADIUS, 0.0000f - HIGHT, -1.0000f * RADIUS}, Palette::DARK_BLUE},
            //---- Segment #8 ----
            //---- DISC ----
            {{0.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::BLUE},
            {{-0.5000f * RADIUS, 0.0000f, -0.8660f * RADIUS}, Palette::BLUE},
            {{-0.8660f * RADIUS, 0.0000f, -0.5000f * RADIUS}, Palette::BLUE},
            //---- RIM ----
            {{-0.5000f * RADIUS, 0.0000f, -0.8660f * RADIUS}, Palette::DARK_BLUE},
            {{-0.5000f * RADIUS, 0.0000f - HIGHT, -0.8660f * RADIUS}, Palette::DARK_BLUE},
            {{-0.8660f * RADIUS, 0.0000f, -0.5000f * RADIUS}, Palette::DARK_BLUE},
            //----
            {{-0.8660f * RADIUS, 0.0000f - HIGHT, -0.5000f * RADIUS}, Palette::DARK_BLUE},
            {{-0.8660f * RADIUS, 0.0000f, -0.5000f * RADIUS}, Palette::DARK_BLUE},
            {{-0.5000f * RADIUS, 0.0000f - HIGHT, -0.8660f * RADIUS}, Palette::DARK_BLUE},
            //---- Segment #9 ----
            //---- DISC ----
            {{0.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::BLUE},
            {{-0.8660f * RADIUS, 0.0000f, -0.5000f * RADIUS}, Palette::BLUE},
            {{-1.0000f * RADIUS, 0.0000f, -0.0000f * RADIUS}, Palette::BLUE},
            //---- RIM ----
            {{-0.8660f * RADIUS, 0.0000f, -0.5000f * RADIUS}, Palette::DARK_BLUE},
            {{-0.8660f * RADIUS, 0.0000f - HIGHT, -0.5000f * RADIUS}, Palette::DARK_BLUE},
            {{-1.0000f * RADIUS, 0.0000f, -0.0000f * RADIUS}, Palette::DARK_BLUE},
            //----
            {{-1.0000f * RADIUS, 0.0000f - HIGHT, -0.0000f * RADIUS}, Palette::DARK_BLUE},
            {{-1.0000f * RADIUS, 0.0000f, -0.0000f * RADIUS}, Palette::DARK_BLUE},
            {{-0.8660f * RADIUS, 0.0000f - HIGHT, -0.5000f * RADIUS}, Palette::DARK_BLUE},
            //---- Segment #10 ----
            //---- DISC ----
            {{0.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::YELLOW},
            {{-1.0000f * RADIUS, 0.0000f, -0.0000f * RADIUS}, Palette::YELLOW},
            {{-0.8660f * RADIUS, 0.0000f, 0.5000f * RADIUS}, Palette::YELLOW},
            //---- RIM ----
            {{-1.0000f * RADIUS, 0.0000f, -0.0000f * RADIUS}, Palette::BROWN},
            {{-1.0000f * RADIUS, 0.0000f - HIGHT, -0.0000f * RADIUS}, Palette::BROWN},
            {{-0.8660f * RADIUS, 0.0000f, 0.5000f * RADIUS}, Palette::BROWN},
            //----
            {{-0.8660f * RADIUS, 0.0000f - HIGHT, 0.5000f * RADIUS}, Palette::BROWN},
            {{-0.8660f * RADIUS, 0.0000f, 0.5000f * RADIUS}, Palette::BROWN},
            {{-1.0000f * RADIUS, 0.0000f - HIGHT, -0.0000f * RADIUS}, Palette::BROWN},
            //---- Segment #11 ----
            //---- DISC ----
            {{0.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::YELLOW},
            {{-0.8660f * RADIUS, 0.0000f, 0.5000f * RADIUS}, Palette::YELLOW},
            {{-0.5000f * RADIUS, 0.0000f, 0.8660f * RADIUS}, Palette::YELLOW},
            //---- RIM ----
            {{-0.8660f * RADIUS, 0.0000f, 0.5000f * RADIUS}, Palette::BROWN},
            {{-0.8660f * RADIUS, 0.0000f - HIGHT, 0.5000f * RADIUS}, Palette::BROWN},
            {{-0.5000f * RADIUS, 0.0000f, 0.8660f * RADIUS}, Palette::BROWN},
            //----
            {{-0.5000f * RADIUS, 0.0000f - HIGHT, 0.8660f * RADIUS}, Palette::BROWN},
            {{-0.5000f * RADIUS, 0.0000f, 0.8660f * RADIUS}, Palette::BROWN},
            {{-0.8660f * RADIUS, 0.0000f - HIGHT, 0.5000f * RADIUS}, Palette::BROWN},
            //---- Segment #12 ----
            //---- DISC ----
            {{0.0000f * RADIUS, 0.0000f, 0.0000f * RADIUS}, Palette::YELLOW},
            {{-0.5000f * RADIUS, 0.0000f, 0.8660f * RADIUS}, Palette::YELLOW},
            {{-0.0000f * RADIUS, 0.0000f, 1.0000f * RADIUS}, Palette::YELLOW},
            //---- RIM ----
            {{-0.5000f * RADIUS, 0.0000f, 0.8660f * RADIUS}, Palette::BROWN},
            {{-0.5000f * RADIUS, 0.0000f - HIGHT, 0.8660f * RADIUS}, Palette::BROWN},
            {{-0.0000f * RADIUS, 0.0000f, 1.0000f * RADIUS}, Palette::BROWN},
            //----
            {{-0.0000f * RADIUS, 0.0000f - HIGHT, 1.0000f * RADIUS}, Palette::BROWN},
            {{-0.0000f * RADIUS, 0.0000f, 1.0000f * RADIUS}, Palette::BROWN},
            {{-0.5000f * RADIUS, 0.0000f - HIGHT, 0.8660f * RADIUS}, Palette::BROWN},
        };

        constexpr auto VERTICES_COUNT{sizeof(VERTICES) / sizeof(VERTICES[0])};

        constexpr auto MEM_SIZE{sizeof(VERTICES)};

} // namespace qbert::unit::disc
