/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- Counterpart header
#include "base_exception.hpp"
//---- C headers
//---- C++ headers
//---- 3rd party headers
//---- Project headers

namespace qbert
{

        //-------------------------------------------------------------------------------------------------------------
        // General/static declarations
        //-------------------------------------------------------------------------------------------------------------

        auto operator<<(ostream &out, const Base_Exception &ex) -> ostream &
        {
                return out << ex.pretty_message();
        }

        auto operator<<(ostream &out, const Base_Exception *const ex) -> ostream &
        {
                return out << (nullptr == ex ? "nullptr" : ex->pretty_message());
        }

        //-------------------------------------------------------------------------------------------------------------
        // Construction/destruction
        //-------------------------------------------------------------------------------------------------------------

        Base_Exception::Base_Exception(const Message &message) : exception{}, m_message{message}
        {
        }

        //-------------------------------------------------------------------------------------------------------------
        // Public interface
        //-------------------------------------------------------------------------------------------------------------

        auto Base_Exception::what() const noexcept -> const char *
        {
                return m_message.c_str();
        }

        auto Base_Exception::message() const noexcept -> const Message &
        {
                return m_message;
        }

        //-------------------------------------------------------------------------------------------------------------
        // Internal helper methods
        //-------------------------------------------------------------------------------------------------------------

} // namespace qbert
