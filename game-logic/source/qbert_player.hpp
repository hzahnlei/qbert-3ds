/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

#pragma once

//---- C headers
//---- C++ headers
#include <functional>
#include <map>
//---- 3rd party headers
//---- Project headers
#include "abstract_character.hpp"
#include "disc.hpp"

namespace qbert
{

        using std::function;
        using std::map;

        class Qbert_Player final : public Abstract_Character
        {
            public:
                using Behavior = function<void(Game_Model &, const Player_Input &)>;

                enum class Animation_Frame
                {
                        CROUCHING_LEFT_DOWN,
                        CROUCHING_LEFT_UP,
                        CROUCHING_RIGHT_DOWN,
                        CROUCHING_RIGHT_UP,
                        DEAD,
                        STANDING_LEFT_DOWN,
                        STANDING_LEFT_UP,
                        STANDING_RIGHT_DOWN,
                        STANDING_RIGHT_UP,
                };

                enum class Desired_Move
                {
                        LEFT_DOWN,
                        LEFT_UP,
                        NONE,
                        RIGHT_DOWN,
                        RIGHT_UP
                };

                static auto desired_move(const Player_Input &) noexcept -> Desired_Move;

            private:
                enum class State
                {
                        WAITING,
                        FALLING,
                        JUMPING_LEFT_DOWN,
                        JUMPING_LEFT_UP,
                        JUMPING_RIGHT_DOWN,
                        JUMPING_RIGHT_UP,
                        START_JUMPING_LEFT_DOWN,
                        START_JUMPING_LEFT_UP,
                        START_JUMPING_RIGHT_DOWN,
                        START_JUMPING_RIGHT_UP,
                        DYING,
                        DEAD,
                        SURFING_ON_DISC,
                };
                State m_state{State::FALLING};
                const map<State, Behavior> m_behaviors;
                Disc const *m_surfing_on{nullptr};
                enum class Orientation
                {
                        LEFT_DOWN,
                        LEFT_UP,
                        RIGHT_DOWN,
                        RIGHT_UP
                };
                Orientation m_orientation{Orientation::LEFT_DOWN};

                static constexpr auto JUMP_DOWN_FRAME_COUNT{12U};
                static constexpr auto VERTICAL_SPEED{1.0f / JUMP_DOWN_FRAME_COUNT};
                static constexpr auto JUMP_UP_FRAME_COUNT{JUMP_DOWN_FRAME_COUNT * 2};
                static constexpr auto HORIZONTAL_SPEED{1.0f / JUMP_UP_FRAME_COUNT};

            public:
                Qbert_Player(const game::Coordinate &);

            public:
                auto interact(Game_Model &, const Player_Input & = NO_PLAYER_INPUT) -> void override;

                [[nodiscard]] auto animation_frame() const noexcept -> Animation_Frame;

                [[nodiscard]] auto is_on_or_above(const game::Coordinate &) const noexcept -> bool override;

                [[nodiscard]] auto covers(const game::Coordinate &) const noexcept -> bool override;

                auto jump_off_disc_to_playfield(const Playfield &) -> void;

            private:
                auto wait(const Player_Input &) -> void;

                auto fall(Game_Model &) -> void;
                inline auto has_landed_on_cube(const Game_Model &) const
                    -> const Cube *; // TODO add to game? and also has_landed_on_disc? + has_fallen_from_playfield?
                                     // FIXME should not have to know CUBE, just use position
                inline auto has_landed_on_disc(const Game_Model &) const
                    -> const Disc *; // FIXME should not have to know DISC, just use position
                inline auto continue_falling() noexcept -> void;
                inline auto stop_falling() noexcept -> void;
                inline auto change_cube_color(Game_Model &, const Cube &) const
                    -> void; // FIXME should not have to know CUBE, just use position

                auto jump_left_down(Game_Model &) -> void;
                inline auto start_falling() noexcept -> void;
                auto jump_left_up(Game_Model &) -> void;
                auto jump_right_down(Game_Model &) -> void;
                auto jump_right_up(Game_Model &) -> void;

                auto start_jumping_left_down() -> void;
                auto start_jumping_left_up() -> void;
                auto start_jumping_right_down() -> void;
                auto start_jumping_right_up() -> void;

                auto die() -> void;
                auto dying(Game_Model &) -> void;
                auto dead(Game_Model &) -> void;

                auto surf_on_disc(Game_Model &, const Disc &)
                    -> void; // FIXME should not have to know DISC, use CHARACTER?
                auto surfing_on_disc() -> void;
        };

        //---- Falling ----

        auto Qbert_Player::has_landed_on_cube(const Game_Model &game) const -> const Cube *
        {
                return game.playfield().cube_at(m_position);
        }

        auto Qbert_Player::has_landed_on_disc(const Game_Model &game) const -> const Disc *
        {
                return game.disc_at(m_position);
        }

        auto Qbert_Player::continue_falling() noexcept -> void
        {
                m_position.y -= VERTICAL_SPEED;
        }

        auto Qbert_Player::stop_falling() noexcept -> void
        {
                m_state = State::WAITING;
                level_out_limited_FP_precision(m_position);
        }

        auto Qbert_Player::change_cube_color(Game_Model &game, const Cube &cube) const -> void
        {
                game.touch(cube);
        }

        //---- Start falling ----

        auto Qbert_Player::start_falling() noexcept -> void
        {
                m_position.quantize();
                m_state = State::FALLING;
        }

} // namespace qbert