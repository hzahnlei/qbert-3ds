#pragma once

#include "color.hpp"
#include "position.hpp"
#include "vertex.hpp"
#include <cstdint>
#include <cstdlib>

namespace qbert
{
        using std::size_t;

        struct Colored_Vertex final
        {
                Position position;
                Color color;

                inline static constexpr auto byte_count() noexcept -> size_t;
                inline static constexpr auto byte_count(const Vertex_Count) noexcept -> size_t;
                inline static constexpr auto component_count() noexcept -> size_t;
        };

        constexpr auto Colored_Vertex::byte_count() noexcept -> size_t
        {
                return sizeof(Colored_Vertex);
        }

        constexpr auto Colored_Vertex::byte_count(const Vertex_Count vertex_count) noexcept -> size_t
        {
                return vertex_count * Colored_Vertex::byte_count();
        }

        constexpr auto Colored_Vertex::component_count() noexcept -> size_t
        {
                return 2;
        }

} // namespace qbert
