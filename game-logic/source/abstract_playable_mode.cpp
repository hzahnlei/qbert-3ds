#include "abstract_playable_mode.hpp"

#include "assertion.hpp"
#include <algorithm>

namespace qbert
{

        using std::any_of;
        using std::begin;
        using std::cbegin;
        using std::cend;
        using std::end;
        using std::for_each;

        // FIXME reward 50 points for each disc left at the end of round

        // FIXME no balloon if player falls off screen

        // FIXME ballon only if player hit by enemy

        auto Abstract_Playable_Mode::set_hi_score(const Score hi_score) -> void
        {
                m_hi_score = hi_score;
        }

        auto Abstract_Playable_Mode::disc_at(const game::Coordinate &player_position) const -> const Disc *
        {
                const auto is_at_same_position{
                    [&player_position](const auto &disc) { return disc.covers(player_position); }};
                const auto disc{find_if(cbegin(m_discs), cend(m_discs), is_at_same_position)};
                return (cend(m_discs) != disc) ? &(*disc) : nullptr;
        }

        auto Abstract_Playable_Mode::attempt_to_attach_player_to(const Disc &disc) -> bool
        {
                const auto disc_player_has_landed_on{disc_at(disc.position())};
                DEV_ASSERT(nullptr != disc_player_has_landed_on, "No disc to attach player to");
                if (disc_player_has_landed_on->can_be_used_for_surfing())
                {
                        const_cast<Disc *>(disc_player_has_landed_on)
                            ->carry_to(disc_landing_position(*disc_player_has_landed_on));
                        return true;
                }
                else
                {
                        return false;
                }
        }

        auto Abstract_Playable_Mode::have_qbert_jump_off_disc_to_playfield() -> void
        {
                m_player.jump_off_disc_to_playfield(playfield());
        }

        auto Abstract_Playable_Mode::player_died() noexcept -> void
        {
                m_player_count--;
                // TODO stop game if player count <= 0!!!!!!!!
        }

        auto Abstract_Playable_Mode::touch(const Cube &cube) -> bool
        {
                if (m_playfield.touch(cube.position()))
                {
                        m_score += 25;
                        return true;
                }
                else
                {
                        return false;
                }
        }

        auto Abstract_Playable_Mode::is_within_playfield(const game::Coordinate &position) const -> bool
        {
                return m_playfield.is_on_or_above(position) ||
                       any_of(cbegin(m_discs), cend(m_discs),
                              [&position](const auto &disc) { return disc.is_on_or_above(position); });
        }

        auto Abstract_Playable_Mode::update(const Player_Input &player_input) -> void
        {
                for_each(begin(m_discs), end(m_discs), [this](auto &disc) { disc.interact(*this); });
                m_player.interact(*this, player_input);
                m_coily.interact(*this);
        }

} // namespace qbert
