#pragma once

#include "abstract_playable_mode.hpp"
#include "coily.hpp"
#include "disc.hpp"
#include "position.hpp"
#include "qbert_player.hpp"

namespace qbert
{

        class Game_Mode final : public Abstract_Playable_Mode
        {
            private:
                Scene_Angle m_scene_angle_y{DEFAULT_SCENE_ROTATION_Y};

            public:
                [[nodiscard]] inline auto scene_angle_y() const noexcept -> Scene_Angle;

                auto rotate_scene_clockwise() -> void override;
                auto rotate_scene_counter_clockwise() -> void override;
                auto reset_scene_rotation() -> void override;

                [[nodiscard]] inline auto state() const noexcept -> State override;
        };

        auto Game_Mode::scene_angle_y() const noexcept -> Scene_Angle
        {
                return m_scene_angle_y;
        }

        auto Game_Mode::state() const noexcept -> State
        {
                return State::PLAY;
        }

} // namespace qbert
