/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- Counterpart header
#include "assertion.hpp"
//---- C headers
//---- C++ headers
//---- 3rd party headers
//---- Project headers

namespace qbert
{

        using std::to_string;

        //-------------------------------------------------------------------------------------------------------------
        // General/static declarations
        //-------------------------------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------------------------
        // Construction/destruction
        //-------------------------------------------------------------------------------------------------------------

        Assertion_Violation::Assertion_Violation(const File_Name &file, const Line_Number line, const Message &message)
            : Base_Exception{message}, m_file{file}, m_line{line}
        {
        }

        //-------------------------------------------------------------------------------------------------------------
        // Public interface
        //-------------------------------------------------------------------------------------------------------------

        auto Assertion_Violation::pretty_message() const -> Message
        {
                return "Assertion violated in " + m_file + " at " + to_string(m_line) + ": " + message();
        }

        auto Assertion_Violation::file() const noexcept -> const File_Name &
        {
                return m_file;
        }

        auto Assertion_Violation::line() const noexcept -> Line_Number
        {
                return m_line;
        }

        //-------------------------------------------------------------------------------------------------------------
        // Internal helper methods
        //-------------------------------------------------------------------------------------------------------------

} // namespace qbert
