#pragma once

#include "character.hpp"
#include <cstdint>
#include <cstdlib>

namespace qbert
{

        using std::size_t;

        class Abstract_Character : public Character
        {

            public:
                static inline auto level_out_limited_FP_precision(game::Coordinate &) noexcept -> void;

            protected:
                game::Coordinate m_position;
                size_t m_animation_counter{0U};

            public:
                Abstract_Character(const game::Coordinate &position);

            public:
                virtual auto interact(Game_Model &, const Player_Input & = NO_PLAYER_INPUT) -> void override;

                [[nodiscard]] inline auto position() const noexcept -> const game::Coordinate & final override;

                [[nodiscard]] virtual auto is_falling_off_playfield(const Game_Model &) const -> bool override;
        };

        auto Abstract_Character::level_out_limited_FP_precision(game::Coordinate &position) noexcept -> void
        {
                position.quantize();
        }

        auto Abstract_Character::position() const noexcept -> const game::Coordinate &
        {
                return m_position;
        }

} // namespace qbert
