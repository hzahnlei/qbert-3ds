#pragma once

#include "colored_vertex.hpp"
#include "textured_vertex.hpp"

namespace qbert
{

        struct Textured_Colored_Vertex final
        {
                Position position;
                Texture_Coordinate texture_coordinate;
                Color color;

                inline static constexpr auto byte_count() noexcept -> size_t;
                inline static constexpr auto byte_count(const Vertex_Count) noexcept -> size_t;
                inline static constexpr auto component_count() noexcept -> size_t;
        };

        constexpr auto Textured_Colored_Vertex::byte_count() noexcept -> size_t
        {
                return sizeof(Textured_Colored_Vertex);
        }

        constexpr auto Textured_Colored_Vertex::byte_count(const Vertex_Count vertex_count) noexcept -> size_t
        {
                return vertex_count * Textured_Colored_Vertex::byte_count();
        }

        constexpr auto Textured_Colored_Vertex::component_count() noexcept -> size_t
        {
                return 3;
        }

} // namespace qbert
