/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

#pragma once

//---- C headers
//---- C++ headers
//---- 3rd party headers
//---- Project headers
#include "abstract_character.hpp"

namespace qbert
{

        class Coily final : public Abstract_Character
        {
            public:
                enum class Animation_Frame
                {
                        STANDING_LEFT_DOWN
                };

            public:
                Coily(const game::Coordinate &);

            public:
                auto interact(Game_Model &, const Player_Input & = NO_PLAYER_INPUT) -> void override;

                [[nodiscard]] auto animation_frame() const noexcept -> Animation_Frame;

                [[nodiscard]] auto is_on_or_above(const game::Coordinate &) const noexcept -> bool override;

                [[nodiscard]] auto covers(const game::Coordinate &) const noexcept -> bool override;
        };

} // namespace qbert