/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "colored_vertex.hpp"

namespace qbert::unit::cube
{

        constexpr auto SIDE_LENGTH{0.2f};
        constexpr auto HALF_LENGTH{SIDE_LENGTH / 2.0f};

        constexpr Colored_Vertex VERTICES[]{
            // Top face
            // First triangle
            {.position{-HALF_LENGTH, +HALF_LENGTH, -HALF_LENGTH}, .color{Palette::BLUE}},
            {{-HALF_LENGTH, +HALF_LENGTH, +HALF_LENGTH}, Palette::BLUE},
            {{+HALF_LENGTH, +HALF_LENGTH, +HALF_LENGTH}, Palette::BLUE},
            // Second triangle
            {{+HALF_LENGTH, +HALF_LENGTH, +HALF_LENGTH}, Palette::BLUE},
            {{+HALF_LENGTH, +HALF_LENGTH, -HALF_LENGTH}, Palette::BLUE},
            {{-HALF_LENGTH, +HALF_LENGTH, -HALF_LENGTH}, Palette::BLUE},
            // Left face
            // First triangle
            {{-HALF_LENGTH, -HALF_LENGTH, +HALF_LENGTH}, Palette::CYAN},
            {{+HALF_LENGTH, -HALF_LENGTH, +HALF_LENGTH}, Palette::CYAN},
            {{+HALF_LENGTH, +HALF_LENGTH, +HALF_LENGTH}, Palette::CYAN},
            // Second triangle
            {{+HALF_LENGTH, +HALF_LENGTH, +HALF_LENGTH}, Palette::CYAN},
            {{-HALF_LENGTH, +HALF_LENGTH, +HALF_LENGTH}, Palette::CYAN},
            {{-HALF_LENGTH, -HALF_LENGTH, +HALF_LENGTH}, Palette::CYAN},
            // Right face
            // First triangle
            {{+HALF_LENGTH, -HALF_LENGTH, -HALF_LENGTH}, Palette::DARK_CYAN},
            {{+HALF_LENGTH, +HALF_LENGTH, -HALF_LENGTH}, Palette::DARK_CYAN},
            {{+HALF_LENGTH, +HALF_LENGTH, +HALF_LENGTH}, Palette::DARK_CYAN},
            // Second triangle
            {{+HALF_LENGTH, +HALF_LENGTH, +HALF_LENGTH}, Palette::DARK_CYAN},
            {{+HALF_LENGTH, -HALF_LENGTH, +HALF_LENGTH}, Palette::DARK_CYAN},
            {{+HALF_LENGTH, -HALF_LENGTH, -HALF_LENGTH}, Palette::DARK_CYAN},
        };

        constexpr auto VERTICES_COUNT{sizeof(VERTICES) / sizeof(VERTICES[0])};

        constexpr auto MEM_SIZE{sizeof(VERTICES)};

} // namespace qbert::unit::cube
