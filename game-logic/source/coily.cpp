/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- Counterpart header
#include "coily.hpp"
//---- C headers
//---- C++ headers
//---- 3rd party headers
//---- Project headers

namespace qbert
{

        Coily::Coily(const game::Coordinate &position) : Abstract_Character{position}
        {
        }

        auto Coily::interact(Game_Model &game, const Player_Input &) -> void
        {
                Abstract_Character::interact(game);
        }

        auto Coily::animation_frame() const noexcept -> Animation_Frame
        {
                return Animation_Frame::STANDING_LEFT_DOWN;
        }

        auto Coily::is_on_or_above(const game::Coordinate &position) const noexcept -> bool
        {
                // TODO test
                return ((position.x >= m_position.x) && (position.x < m_position.x + 1.0f)) &&
                       (position.y >= m_position.y) &&
                       ((position.z >= m_position.z) && (position.z < m_position.z + 1.0f));
        }

        auto Coily::covers(const game::Coordinate &player_position) const noexcept -> bool
        {
                // TODO test
                return (player_position.x >= m_position.x) && (player_position.x < m_position.x + 1.0f) &&
                       (player_position.y >= m_position.y - 0.01f) && (player_position.y <= m_position.y + 0.01f) &&
                       (player_position.z >= m_position.z) && (player_position.z < m_position.z + 1.0f);
        }

} // namespace qbert
