#include "affine_transformation_matrix.hpp"

namespace qbert
{

        auto Affine_Transformation_Matrix::apply_to(Position &v) const -> void
        {
                const auto x{dot_product(row[0], v)};
                const auto y{dot_product(row[1], v)};
                const auto z{dot_product(row[2], v)};
                v.x = x;
                v.y = y;
                v.z = z;
        }

        auto Affine_Transformation_Matrix::operator()(const Position &v) const -> Position
        {
                return Position{.x = dot_product(row[0], v), .y = dot_product(row[1], v), .z = dot_product(row[2], v)};
        }

} // namespace qbert