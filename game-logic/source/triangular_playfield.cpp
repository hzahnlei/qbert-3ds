/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

#include "triangular_playfield.hpp"

#include <algorithm>
#include <memory>

namespace qbert
{

        using std::any_of;
        using std::begin;
        using std::cbegin;
        using std::cend;
        using std::end;
        using std::find_if;
        using std::move;

        auto Triangular_Playfield::cubes(const Height height) -> vector<Cube>
        {
                vector<Cube> playfield;
                for (auto row{0U}; row < height; row++)
                {
                        const auto cube_count{cube_count_on_row(row)};
                        auto x{cube_count};
                        const auto y{-1.0f * static_cast<float>(row)};
                        auto z{0U};
                        for (auto column{0U}; column < cube_count; column++)
                        {
                                x--;
                                playfield.emplace_back(
                                    game::Coordinate{static_cast<float>(x), y, static_cast<float>(z)});
                                z++;
                        }
                }
                return playfield;
        }

        Triangular_Playfield::Triangular_Playfield(const Height height) : m_height{height}, m_cubes{move(cubes(height))}
        {
        }

        auto Triangular_Playfield::touch(const game::Coordinate &cube_position) -> bool
        {
                auto cube{modifiable_cube_at(cube_position)};
                if (nullptr != cube && cube->touch())
                {
                        m_touch_count++;
                        m_has_been_changed = true;
                        return true;
                }
                else
                {
                        return false;
                }
        }

        auto Triangular_Playfield::untouch(const game::Coordinate &cube_position) -> bool
        {
                auto cube{modifiable_cube_at(cube_position)};
                if (nullptr != cube && cube->untouch())
                {
                        m_touch_count--;
                        m_has_been_changed = true;
                        return true;
                }
                else
                {
                        return false;
                }
        }

        auto Triangular_Playfield::has_been_touched(const game::Coordinate &cube_position) const -> bool
        {
                const auto cube{cube_at(cube_position)};
                return (nullptr != cube) && (cube->has_been_touched());
        }

        auto Triangular_Playfield::has_been_changed() -> bool
        {
                if (m_has_been_changed)
                {
                        m_has_been_changed = false;
                        return true;
                }
                else
                {
                        return false;
                }
        }

        auto Triangular_Playfield::cube_at(const game::Coordinate &player_position) const -> const Cube *
        {
                const auto is_at_same_position{
                    [&player_position](const auto &cube) { return cube.covers(player_position); }};
                const auto cube{find_if(cbegin(m_cubes), cend(m_cubes), is_at_same_position)};
                return (cend(m_cubes) != cube) ? &(*cube) : nullptr;
        }

        auto Triangular_Playfield::is_on_or_above(const game::Coordinate &position) const -> bool
        {
                return any_of(cbegin(m_cubes), cend(m_cubes),
                              [&position](const auto &cube) { return cube.is_on_or_above(position); });
        }

        auto Triangular_Playfield::disc_landing_position(const game::Coordinate &position) const
            -> const game::Coordinate &
        {
                return is_right_of_playfield(position) ? RIGHT_OF_TOP_CUBE : LEFT_OF_TOP_CUBE;
        }

        auto Triangular_Playfield::modifiable_cube_at(const game::Coordinate &player_position) -> Cube *
        {
                const auto is_at_same_position{
                    [&player_position](const auto &cube) { return cube.covers(player_position); }};
                const auto cube{find_if(begin(m_cubes), end(m_cubes), is_at_same_position)};
                return (end(m_cubes) != cube) ? &(*cube) : nullptr;
        }

} // namespace qbert