#pragma once

#include "coily.hpp"
#include "qbert_player.hpp"
#include "textured_vertex.hpp"
#include <map>
#include <vector>

namespace qbert
{

        using std::map;
        using std::vector;

        /*!
         * The texture map is made up of 128x256 pixels.
         */
        namespace texture::map
        {
                constexpr auto PIXEL_COUNT_U{128U};
                constexpr auto PIXEL_COUNT_V{256U};
        } // namespace texture::map

        /*!
         * Font shapes are made up of 8x8 pixels.
         * That devides the texture map into 32x64 tiles.
         */
        namespace font
        {
                constexpr auto PIXEL_COUNT_U{8U};
                constexpr auto PIXEL_COUNT_V{8U};

                constexpr auto DIVISIONS_U{texture::map::PIXEL_COUNT_U / PIXEL_COUNT_U};
                constexpr auto DIVISIONS_V{texture::map::PIXEL_COUNT_V / PIXEL_COUNT_U};

                constexpr auto UNITS_U{1.0f / DIVISIONS_U};
                constexpr auto UNITS_V{1.0f / DIVISIONS_V};

                /*!
                 * The characters are beginning at row 24 (V axis).
                 */
                static constexpr auto V_OFFSET_FOR_CHARS{24};
        } // namespace font

        /*!
         * Small sprites are made up of 16x16 pixels.
         * That devides the texture map into a grid of 16x32 tiles.
         */
        namespace sprite
        {
                constexpr auto PIXEL_COUNT_U{16U};
                constexpr auto PIXEL_COUNT_V{16U};

                constexpr auto DIVISIONS_U{texture::map::PIXEL_COUNT_U / PIXEL_COUNT_U};
                constexpr auto DIVISIONS_V{texture::map::PIXEL_COUNT_V / PIXEL_COUNT_U};

                constexpr auto UNITS_U{1.0f / DIVISIONS_U};
                constexpr auto UNITS_V{1.0f / DIVISIONS_V};
        } // namespace sprite

        using Texture_Coordinates = vector<Texture_Coordinate>;

        namespace qbert
        {
                namespace crouching
                {
                        constexpr auto DOWN_U_MIN{sprite::UNITS_U * 3};
                        constexpr auto DOWN_U_MAX{sprite::UNITS_U * 4};
                        constexpr auto DOWN_V_MIN{sprite::UNITS_V * 10};
                        constexpr auto DOWN_V_MAX{sprite::UNITS_V * 11};

                        static const Texture_Coordinates LEFT_DOWN_TEX_COORDS{// First triangle
                                                                              {DOWN_U_MIN, DOWN_V_MIN},
                                                                              {DOWN_U_MAX, DOWN_V_MIN},
                                                                              {DOWN_U_MAX, DOWN_V_MAX},
                                                                              // Second triangle
                                                                              {DOWN_U_MAX, DOWN_V_MAX},
                                                                              {DOWN_U_MIN, DOWN_V_MAX},
                                                                              {DOWN_U_MIN, DOWN_V_MIN}};

                        constexpr auto UP_U_MIN{sprite::UNITS_U * 2};
                        constexpr auto UP_U_MAX{sprite::UNITS_U * 3};
                        constexpr auto UP_V_MIN{sprite::UNITS_V * 1};
                        constexpr auto UP_V_MAX{sprite::UNITS_V * 2};

                        static const Texture_Coordinates LEFT_UP_TEX_COORDS{// First triangle
                                                                            {UP_U_MIN, UP_V_MIN},
                                                                            {UP_U_MAX, UP_V_MIN},
                                                                            {UP_U_MAX, UP_V_MAX},
                                                                            // Second triangle
                                                                            {UP_U_MAX, UP_V_MAX},
                                                                            {UP_U_MIN, UP_V_MAX},
                                                                            {UP_U_MIN, UP_V_MIN}};
                        static const Texture_Coordinates RIGHT_DOWN_TEX_COORDS{// First triangle
                                                                               {DOWN_U_MAX, DOWN_V_MIN},
                                                                               {DOWN_U_MIN, DOWN_V_MIN},
                                                                               {DOWN_U_MIN, DOWN_V_MAX},
                                                                               // Second triangle
                                                                               {DOWN_U_MIN, DOWN_V_MAX},
                                                                               {DOWN_U_MAX, DOWN_V_MAX},
                                                                               {DOWN_U_MAX, DOWN_V_MIN}};
                        static const Texture_Coordinates RIGHT_UP_TEX_COORDS{// First triangle
                                                                             {UP_U_MAX, UP_V_MIN},
                                                                             {UP_U_MIN, UP_V_MIN},
                                                                             {UP_U_MIN, UP_V_MAX},
                                                                             // Second triangle
                                                                             {UP_U_MIN, UP_V_MAX},
                                                                             {UP_U_MAX, UP_V_MAX},
                                                                             {UP_U_MAX, UP_V_MIN}};

                } // namespace crouching

                namespace standing
                {
                        constexpr auto DOWN_U_MIN{sprite::UNITS_U * 3};
                        constexpr auto DOWN_U_MAX{sprite::UNITS_U * 4};
                        constexpr auto DOWN_V_MIN{sprite::UNITS_V * 9};
                        constexpr auto DOWN_V_MAX{sprite::UNITS_V * 10};

                        static const Texture_Coordinates LEFT_DOWN_TEX_COORDS{// First triangle
                                                                              {DOWN_U_MIN, DOWN_V_MIN},
                                                                              {DOWN_U_MAX, DOWN_V_MIN},
                                                                              {DOWN_U_MAX, DOWN_V_MAX},
                                                                              // Second triangle
                                                                              {DOWN_U_MAX, DOWN_V_MAX},
                                                                              {DOWN_U_MIN, DOWN_V_MAX},
                                                                              {DOWN_U_MIN, DOWN_V_MIN}};

                        constexpr auto UP_U_MIN{sprite::UNITS_U * 2};
                        constexpr auto UP_U_MAX{sprite::UNITS_U * 3};
                        constexpr auto UP_V_MIN{sprite::UNITS_V * 0};
                        constexpr auto UP_V_MAX{sprite::UNITS_V * 1};

                        static const Texture_Coordinates LEFT_UP_TEX_COORDS{// First triangle
                                                                            {UP_U_MIN, UP_V_MIN},
                                                                            {UP_U_MAX, UP_V_MIN},
                                                                            {UP_U_MAX, UP_V_MAX},
                                                                            // Second triangle
                                                                            {UP_U_MAX, UP_V_MAX},
                                                                            {UP_U_MIN, UP_V_MAX},
                                                                            {UP_U_MIN, UP_V_MIN}};

                        static const Texture_Coordinates RIGHT_DOWN_TEX_COORDS{// First triangle
                                                                               {DOWN_U_MAX, DOWN_V_MIN},
                                                                               {DOWN_U_MIN, DOWN_V_MIN},
                                                                               {DOWN_U_MIN, DOWN_V_MAX},
                                                                               // Second triangle
                                                                               {DOWN_U_MIN, DOWN_V_MAX},
                                                                               {DOWN_U_MAX, DOWN_V_MAX},
                                                                               {DOWN_U_MAX, DOWN_V_MIN}};
                        static const Texture_Coordinates RIGHT_UP_TEX_COORDS{// First triangle
                                                                             {UP_U_MAX, UP_V_MIN},
                                                                             {UP_U_MIN, UP_V_MIN},
                                                                             {UP_U_MIN, UP_V_MAX},
                                                                             // Second triangle
                                                                             {UP_U_MIN, UP_V_MAX},
                                                                             {UP_U_MAX, UP_V_MAX},
                                                                             {UP_U_MAX, UP_V_MIN}};

                } // namespace standing

                static const map<Qbert_Player::Animation_Frame, Texture_Coordinates> TEXTURES{
                    make_pair(Qbert_Player::Animation_Frame::CROUCHING_LEFT_DOWN, crouching::LEFT_DOWN_TEX_COORDS),
                    make_pair(Qbert_Player::Animation_Frame::CROUCHING_LEFT_UP, crouching::LEFT_UP_TEX_COORDS),
                    make_pair(Qbert_Player::Animation_Frame::CROUCHING_RIGHT_DOWN, crouching::RIGHT_DOWN_TEX_COORDS),
                    make_pair(Qbert_Player::Animation_Frame::CROUCHING_RIGHT_UP, crouching::RIGHT_UP_TEX_COORDS),

                    make_pair(Qbert_Player::Animation_Frame::STANDING_LEFT_DOWN, standing::LEFT_DOWN_TEX_COORDS),
                    make_pair(Qbert_Player::Animation_Frame::STANDING_LEFT_UP, standing::LEFT_UP_TEX_COORDS),
                    make_pair(Qbert_Player::Animation_Frame::STANDING_RIGHT_DOWN, standing::RIGHT_DOWN_TEX_COORDS),
                    make_pair(Qbert_Player::Animation_Frame::STANDING_RIGHT_UP, standing::RIGHT_UP_TEX_COORDS),
                };
        } // namespace qbert

        constexpr auto COILY_U_MIN{sprite::UNITS_U * 0};
        constexpr auto COILY_U_MAX{sprite::UNITS_U * 1};
        constexpr auto COILY_V_MIN{sprite::UNITS_V * 9};
        constexpr auto COILY_V_MAX{sprite::UNITS_V * 11};

        static const Texture_Coordinates COILY_STANDING_LEFT_DOWN_TEX_COORDS{// First triangle
                                                                             {COILY_U_MIN, COILY_V_MIN},
                                                                             {COILY_U_MAX, COILY_V_MIN},
                                                                             {COILY_U_MAX, COILY_V_MAX},
                                                                             // Second triangle
                                                                             {COILY_U_MAX, COILY_V_MAX},
                                                                             {COILY_U_MIN, COILY_V_MAX},
                                                                             {COILY_U_MIN, COILY_V_MIN}};

        static const map<Coily::Animation_Frame, Texture_Coordinates> COILY_TEXTURES{
            make_pair(Coily::Animation_Frame::STANDING_LEFT_DOWN, COILY_STANDING_LEFT_DOWN_TEX_COORDS)};

        constexpr auto BALLOON_U_MIN{sprite::UNITS_U * 6};
        constexpr auto BALLOON_U_MAX{sprite::UNITS_U * 8};
        constexpr auto BALLOON_V_MIN{sprite::UNITS_V * 9};
        constexpr auto BALLOON_V_MAX{sprite::UNITS_V * 12};

        static const Texture_Coordinates BALLOON_TEX_COORDS{
            // First triangle
            {BALLOON_U_MAX, BALLOON_V_MIN},
            {BALLOON_U_MAX, BALLOON_V_MAX},
            {BALLOON_U_MIN, BALLOON_V_MAX},
            // Second triangle
            {BALLOON_U_MIN, BALLOON_V_MAX},
            {BALLOON_U_MIN, BALLOON_V_MIN},
            {BALLOON_U_MAX, BALLOON_V_MIN},
        };

} // namespace qbert