#pragma once

#include "affine_transformation_matrix.hpp"
#include "colored_vertex.hpp"
#include "disc.hpp"
#include "unit_cube_vertices.hpp"
#include "vertex_pool.hpp"
#include <vector>

namespace qbert::disc_to_scene
{

        using std::vector;

        /*!
         * Align discs at upper edge of cubes.
         */
        static constexpr float DISC_Y_ALIGN{unit::cube::HALF_LENGTH};

        [[nodiscard]] auto rotate_and_translate(const Disc &) -> Affine_Transformation_Matrix;

        auto init_vertices(const vector<Disc> &, Vertex_Pool<Colored_Vertex> &) -> void;

} // namespace qbert::disc_to_scene
