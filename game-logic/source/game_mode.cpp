#include "game_mode.hpp"

namespace qbert
{

        auto Game_Mode::rotate_scene_clockwise() -> void
        {
                if (m_scene_angle_y > MIN_SCENE_ROTATION_Y)
                {
                        m_scene_angle_y--;
                }
        }

        auto Game_Mode::rotate_scene_counter_clockwise() -> void
        {
                if (m_scene_angle_y < MAX_SCENE_ROTATION_Y)
                {
                        m_scene_angle_y++;
                }
        }

        auto Game_Mode::reset_scene_rotation() -> void
        {
                m_scene_angle_y = DEFAULT_SCENE_ROTATION_Y;
        }

} // namespace qbert
