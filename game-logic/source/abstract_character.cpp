#include "abstract_character.hpp"

namespace qbert
{

        Abstract_Character::Abstract_Character(const game::Coordinate &position) : m_position{position}
        {
        }

        auto Abstract_Character::interact(Game_Model &, const Player_Input &) -> void
        {
                m_animation_counter++;
        }

        auto Abstract_Character::is_falling_off_playfield(const Game_Model &game) const -> bool
        {
                return !game.is_within_playfield(m_position);
        }

} // namespace qbert
