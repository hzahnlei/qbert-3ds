#include "game_model_delegating_impl.hpp"

#include <cstdlib>
#include <cstring>

namespace qbert
{

        Game_Model_Delegating_Impl::Game_Model_Delegating_Impl()
        {
                m_current_mode = &m_attract_mode;
        }

        auto Game_Model_Delegating_Impl::rotate_scene_clockwise() -> void
        {
                m_current_mode->rotate_scene_clockwise();
        }

        auto Game_Model_Delegating_Impl::rotate_scene_counter_clockwise() -> void
        {
                m_current_mode->rotate_scene_counter_clockwise();
        }

        auto Game_Model_Delegating_Impl::reset_scene_rotation() -> void
        {
                m_current_mode->reset_scene_rotation();
        }

        auto Game_Model_Delegating_Impl::update(const Player_Input &player_input) -> void
        {
                if (player_input.is_pressed(Button::START))
                {
                        start_new_game();
                }
                else
                {
                        m_current_mode->update(player_input);
                }
        }

        auto Game_Model_Delegating_Impl::start_new_game() -> void
        {
                if (!playing_already())
                {
                        m_state = State::PLAY;
                        m_game_mode.reset_scene_rotation();
                        m_current_mode = &m_game_mode;
                }
        }

} // namespace qbert
