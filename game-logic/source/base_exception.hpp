/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

#pragma once

//---- C headers
//---- C++ headers
#include <exception>
#include <ostream>
#include <string>
//---- 3rd party headers
//---- Project headers

namespace qbert
{

        using std::exception;
        using std::ostream;
        using std::string;

        /*!
         * \brief A exception that distinguishes (short) message (and what) and pretty_message.
         */
        class Base_Exception : public exception
        {

                //-------------------------------------------------------------------------------------------------------------
                // General/static declarations
                //-------------------------------------------------------------------------------------------------------------
            public:
                using Message = string;

                //-------------------------------------------------------------------------------------------------------------
                // Data elements
                //-------------------------------------------------------------------------------------------------------------
            private:
                const Message m_message;

                //-------------------------------------------------------------------------------------------------------------
                // Construction/destruction
                //-------------------------------------------------------------------------------------------------------------
            public:
                Base_Exception() = delete;
                explicit Base_Exception(const Message &);
                explicit Base_Exception(Base_Exception &) = delete;
                explicit Base_Exception(const Base_Exception &) = delete;

                //-------------------------------------------------------------------------------------------------------------
                // Public interface
                //-------------------------------------------------------------------------------------------------------------
            public:
                auto operator=(const Base_Exception &) -> Base_Exception & = delete;
                auto operator=(Base_Exception &&) -> Base_Exception & = delete;

                [[nodiscard]] auto what() const noexcept -> const char * final override;
                [[nodiscard]] auto message() const noexcept -> const Message &;
                [[nodiscard]] virtual auto pretty_message() const -> Message = 0;

                //-------------------------------------------------------------------------------------------------------------
                // Internal helper methods
                //-------------------------------------------------------------------------------------------------------------
        };

        auto operator<<(ostream &, const Base_Exception &) -> ostream &;
        auto operator<<(ostream &, const Base_Exception *const) -> ostream &;

} // namespace qbert
