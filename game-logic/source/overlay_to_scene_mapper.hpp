#pragma once

#include "game_model.hpp"
#include "texture_mappings.hpp"
#include "textured_colored_vertex.hpp"
#include "vertex_pool.hpp"

namespace qbert::overlay_to_scene
{

        using Shape_Id = unsigned int;

        [[nodiscard]] inline static constexpr auto u_coordinate_of_ascii_char(const char ascii_char) -> float
        {
                return (ascii_char % font::DIVISIONS_U) * font::UNITS_U;
        }

        [[nodiscard]] inline static constexpr auto v_coordinate_of_ascii_char(const char ascii_char) -> float
        {
                return ((ascii_char / font::DIVISIONS_U) + font::V_OFFSET_FOR_CHARS) * font::UNITS_V;
        }

        [[nodiscard]] inline static constexpr auto u_coordinate_of_shape(const Shape_Id shape_id) -> float
        {
                return (shape_id % sprite::DIVISIONS_U) * sprite::UNITS_U;
        }

        [[nodiscard]] inline static constexpr auto v_coordinate_of_shape(const Shape_Id shape_id) -> float
        {
                return (shape_id / sprite::DIVISIONS_U) * sprite::UNITS_V;
        }

        [[nodiscard]] auto number_of_digits(unsigned int) -> unsigned int;

        auto init_vertices(const Game_Model &, Vertex_Pool<Textured_Colored_Vertex> &) -> void;

} // namespace qbert::overlay_to_scene
