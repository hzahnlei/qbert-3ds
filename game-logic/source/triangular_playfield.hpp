/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "playfield.hpp"
#include <vector>

namespace qbert
{

        using std::size_t;
        using std::vector;

        class Triangular_Playfield final : public Playfield
        {
            public:
                using Height = size_t;
                static constexpr Height DEFAULT_HEIGHT{7U};
                using Row_Index = Height;
                /*!
                 * Side_Length to be expected to be > 0.
                 */
                [[nodiscard]] inline static constexpr auto cube_count(const Height) -> size_t;
                [[nodiscard]] inline static constexpr auto cube_count_on_row(const Row_Index) -> size_t;
                [[nodiscard]] static auto cubes(const Height) -> vector<Cube>;

                static constexpr game::Coordinate RIGHT_OF_TOP_CUBE{0.0f, 0.0f, -1.0f - GAP};
                static constexpr game::Coordinate LEFT_OF_TOP_CUBE{-1.0f - GAP, 0.0f, 0.0f};

            private:
                const Height m_height;
                vector<Cube> m_cubes;
                size_t m_touch_count{0U};
                bool m_has_been_changed{false};

            public:
                Triangular_Playfield(const Height = DEFAULT_HEIGHT);

            public:
                auto touch(const game::Coordinate &) -> bool override;
                auto untouch(const game::Coordinate &) -> bool override;
                [[nodiscard]] auto has_been_touched(const game::Coordinate &) const -> bool override;
                [[nodiscard]] inline auto all_cubes_were_touched() const -> bool override;

                [[nodiscard]] auto has_been_changed() -> bool override;

                [[nodiscard]] inline auto cubes() const noexcept -> const vector<Cube> & override;

                [[nodiscard]] inline auto row_count() const noexcept -> size_t override;

                [[nodiscard]] auto cube_at(const game::Coordinate &) const -> const Cube * override;

                [[nodiscard]] auto is_on_or_above(const game::Coordinate &) const -> bool override;

                /*!
                 * If position is not right from playfield, then it is assumed to be left from playfield.
                 */
                [[nodiscard]] inline auto is_right_of_playfield(const game::Coordinate &) const -> bool override;

                [[nodiscard]] auto disc_landing_position(const game::Coordinate &) const
                    -> const game::Coordinate & override;

            private:
                [[nodiscard]] auto modifiable_cube_at(const game::Coordinate &) -> Cube *;

                // [[nodiscard]] inline auto is_right_of_playfield(const game::Coordinate &) const -> bool;
        };

        constexpr auto Triangular_Playfield::cube_count(const Height heigth) -> size_t
        {
                return 1 == heigth ? 1 : heigth + cube_count(heigth - 1);
        }

        constexpr auto Triangular_Playfield::cube_count_on_row(const Row_Index row) -> size_t
        {
                return row + 1;
        }

        auto Triangular_Playfield::all_cubes_were_touched() const -> bool
        {
                return m_touch_count >= m_cubes.size();
        }

        auto Triangular_Playfield::cubes() const noexcept -> const vector<Cube> &
        {
                return m_cubes;
        }

        auto Triangular_Playfield::row_count() const noexcept -> size_t
        {
                return m_height;
        }

        auto Triangular_Playfield::is_right_of_playfield(const game::Coordinate &position) const -> bool
        {
                return position.x >= 0.0f && position.z < 0.0f;
        }

} // namespace qbert