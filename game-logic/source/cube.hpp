#pragma once

#include "game_coordinate.hpp"

namespace qbert
{

        class Cube final
        {
            private:
                const game::Coordinate m_position;
                bool m_touched{false};

            public:
                Cube(const game::Coordinate &, const bool touched = false);

            public:
                [[nodiscard]] inline auto position() const noexcept -> const game::Coordinate &;
                [[nodiscard]] inline auto has_been_touched() const noexcept -> bool;
                auto touch() noexcept -> bool;
                auto untouch() noexcept -> bool;
                [[nodiscard]] auto operator==(const Cube &) const noexcept -> bool;
                [[nodiscard]] auto covers(const game::Coordinate &) const noexcept -> bool;
                [[nodiscard]] auto is_on_or_above(const game::Coordinate &) const noexcept -> bool;
        };

        auto Cube::position() const noexcept -> const game::Coordinate &
        {
                return m_position;
        }

        auto Cube::has_been_touched() const noexcept -> bool
        {
                return m_touched;
        }

} // namespace qbert
