#include "affine_transformation_matrix.hpp"
#include "colored_vertex.hpp"
#include "cube.hpp"
#include "vertex_pool.hpp"
#include <vector>

namespace qbert::playfield_to_scene
{

        using std::vector;

        auto init_vertices(const vector<Cube> &, Vertex_Pool<Colored_Vertex> &) -> void;

} // namespace qbert::playfield_to_scene