/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "cube.hpp"
#include "game_coordinate.hpp"
#include <cstdlib>
#include <vector>

namespace qbert
{

        using std::size_t;
        using std::vector;

        class Playfield
        {
            public:
                /*!
                 * Discs should not sit right next to a cube. A small gap looks better.
                 */
                static constexpr auto GAP{0.2f};

            public:
                Playfield() = default;
                Playfield(const Playfield &) = delete;
                Playfield(Playfield &&) noexcept = delete;
                Playfield &operator=(const Playfield &) = delete;
                Playfield &operator=(Playfield &&) noexcept = delete;
                virtual ~Playfield() = default;

            public:
                virtual auto touch(const game::Coordinate &) -> bool = 0;
                virtual auto untouch(const game::Coordinate &) -> bool = 0;
                [[nodiscard]] virtual auto has_been_touched(const game::Coordinate &) const -> bool = 0;
                [[nodiscard]] virtual auto all_cubes_were_touched() const -> bool = 0;
                [[nodiscard]] virtual auto has_been_changed() -> bool = 0;

                [[nodiscard]] virtual auto cubes() const noexcept -> const vector<Cube> & = 0;

                [[nodiscard]] virtual auto row_count() const noexcept -> size_t = 0;

                [[nodiscard]] virtual auto cube_at(const game::Coordinate &) const -> const Cube * = 0;

                [[nodiscard]] virtual auto is_on_or_above(const game::Coordinate &) const -> bool = 0;

                [[nodiscard]] virtual auto is_right_of_playfield(const game::Coordinate &) const -> bool = 0;
                [[nodiscard]] virtual auto disc_landing_position(const game::Coordinate &) const
                    -> const game::Coordinate & = 0;
        };

} // namespace qbert