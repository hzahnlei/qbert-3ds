#pragma once

#include "player_input.hpp"
#include "transformations.hpp"
#include "triangular_playfield.hpp"
#include <cstddef>
#include <cstdint>
#include <vector>

namespace qbert
{

        using std::vector;

        // Forward declaration
        class Disc;
        class Qbert_Player;
        class Coily;

        class Game_Model
        {

            public:
                using Scene_Angle = int;
                using Score = unsigned int;
                using Level_Number = unsigned int;
                using Round_Number = unsigned int;
                using Player_Count = unsigned int;
                enum class State
                {
                        ATTRACT_MODE,
                        HALL_OF_FAME,
                        START_LEVEL,
                        PLAY
                };

            public:
                static constexpr Scene_Angle DEFAULT_SCENE_ROTATION_X{45_deg};
                static constexpr Scene_Angle DEFAULT_SCENE_ROTATION_Y{-45_deg};
                static constexpr Scene_Angle DEFAULT_SCENE_ROTATION_Z{0_deg};
                static constexpr Scene_Angle MIN_SCENE_ROTATION_Y{-90_deg};
                static constexpr Scene_Angle MAX_SCENE_ROTATION_Y{0_deg};

            public:
                Game_Model() = default;
                Game_Model(const Game_Model &) = delete;
                Game_Model(Game_Model &&) noexcept = delete;
                Game_Model &operator=(const Game_Model &) = delete;
                Game_Model &operator=(Game_Model &&) noexcept = delete;
                virtual ~Game_Model() = default;

            public:
                virtual auto rotate_scene_clockwise() -> void = 0;
                virtual auto rotate_scene_counter_clockwise() -> void = 0;
                virtual auto reset_scene_rotation() -> void = 0;
                [[nodiscard]] virtual auto scene_angle_x() const noexcept -> Scene_Angle = 0;
                [[nodiscard]] virtual auto scene_angle_y() const noexcept -> Scene_Angle = 0;
                [[nodiscard]] virtual auto scene_angle_z() const noexcept -> Scene_Angle = 0;

                [[nodiscard]] virtual auto playfield() const noexcept -> const Playfield & = 0;

                [[nodiscard]] virtual auto discs() const noexcept -> const vector<Disc> & = 0;
                [[nodiscard]] virtual auto disc_at(const game::Coordinate &) const -> const Disc * = 0;
                virtual auto attempt_to_attach_player_to(const Disc &) -> bool = 0;
                virtual auto have_qbert_jump_off_disc_to_playfield() -> void = 0;

                [[nodiscard]] virtual auto player() const noexcept -> const Qbert_Player & = 0;
                [[nodiscard]] virtual auto player_count() const noexcept -> Player_Count = 0;
                virtual auto player_died() noexcept -> void = 0;

                [[nodiscard]] virtual auto coily() const noexcept -> const Coily & = 0;

                [[nodiscard]] virtual auto score() const noexcept -> Score = 0;
                [[nodiscard]] virtual auto hi_score() const noexcept -> Score = 0;

                [[nodiscard]] virtual auto level_number() const noexcept -> Level_Number = 0;
                [[nodiscard]] virtual auto round_number() const noexcept -> Round_Number = 0;

                [[nodiscard]] virtual auto state() const noexcept -> State = 0;

                virtual auto touch(const Cube &) -> bool = 0;

                [[nodiscard]] virtual auto is_within_playfield(const game::Coordinate &) const -> bool = 0;

                virtual auto update(const Player_Input &) -> void = 0;
        };

} // namespace qbert
