#pragma once

#include "affine_transformation_matrix.hpp"
#include <cmath>

namespace qbert
{

        using Degrees = float;

        constexpr float operator"" _deg(long double degrees)
        {
                return static_cast<Degrees>(degrees);
        }

        constexpr int operator"" _deg(unsigned long long degrees)
        {
                return static_cast<int>(degrees);
        }

        using Radians = float;

        constexpr float operator"" _rad(long double radians)
        {
                return static_cast<Radians>(radians);
        }

        static constexpr auto TAU{2.0f * M_PI};

        [[nodiscard]] inline static constexpr auto radians(const Degrees degrees) -> Radians
        {
                return degrees * TAU / 360.0f;
        }

        [[nodiscard]] inline static constexpr auto radians(const int degrees)
        {
                return radians(static_cast<Degrees>(degrees));
        }

        namespace transform
        {
                [[nodiscard]] auto rotation_x(const Degrees) -> Affine_Transformation_Matrix;
                [[nodiscard]] auto rotation_y(const Degrees) -> Affine_Transformation_Matrix;
                [[nodiscard]] auto rotation_z(const Degrees) -> Affine_Transformation_Matrix;
                [[nodiscard]] auto translation(const Position &) -> Affine_Transformation_Matrix;
        } // namespace transform

} // namespace qbert
