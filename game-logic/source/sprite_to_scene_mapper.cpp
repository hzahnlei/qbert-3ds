#include "sprite_to_scene_mapper.hpp"
#include "game_mode.hpp"
#include "screen_dimensions.hpp"
#include "texture_mappings.hpp"
#include "transformations.hpp"
#include "unit_cube_vertices.hpp"
#include "unit_sprite_vertices.hpp"

namespace qbert::sprite_to_scene
{

        constexpr auto COS_X{cosf(radians(Game_Mode::DEFAULT_SCENE_ROTATION_X))};
        constexpr auto SIN_X{sinf(radians(Game_Mode::DEFAULT_SCENE_ROTATION_X))};

        constexpr Affine_Transformation_Matrix TILT_X{{
            {1.0f, 0.0f, 0.0f, 0.0f},
            {0.0f, COS_X, SIN_X, 0.0f},
            {0.0f, -SIN_X, COS_X, 0.0f},
        }};

        constexpr auto COS_Y{cosf(radians(-Game_Mode::DEFAULT_SCENE_ROTATION_Y))};
        constexpr auto SIN_Y{sinf(radians(-Game_Mode::DEFAULT_SCENE_ROTATION_Y))};

        constexpr Affine_Transformation_Matrix TILT_Y{{
            {COS_Y, 0.0f, SIN_Y, 0.0f},
            {0.0f, 1.0f, 0.0f, 0.0f},
            {-SIN_Y, 0.0f, COS_Y, 0.0f},
        }};

        /*!
         * Supposed to let texture look more pixelated. I want each texture pixel to be mapped to one screen pixle.
         * This is actually hard to achieve because of perspective etc.
         */
        constexpr auto SMALL_SPRITE_SCALE{0.9f};

        auto translate_small_sprite(const game::Coordinate &pos) noexcept -> Affine_Transformation_Matrix
        {
                return Affine_Transformation_Matrix{{
                    {SMALL_SPRITE_SCALE, 0.0f, 0.0f, pos.x * screen::SCALING_FACTOR},
                    {0.0f, SMALL_SPRITE_SCALE, 0.0f, (pos.y + 1.0f) * screen::SCALING_FACTOR + screen::Y_OFFSET},
                    {0.0f, 0.0f, SMALL_SPRITE_SCALE, pos.z * screen::SCALING_FACTOR},
                }};
        }

        inline auto balloon_would_be_outside_screen(const game::Coordinate &player_pos) noexcept
        {
                return player_pos.y >= 0.0f;
        }

        auto translate_balloon_besides(const game::Coordinate &player_pos) noexcept
        {
                return translate_small_sprite({player_pos.x - 0.4f, player_pos.y + 0.2f, player_pos.z - 1.4f});
        }

        auto translate_balloon_above(const game::Coordinate &player_pos) noexcept
        {
                return translate_small_sprite({player_pos.x - 0.4f, player_pos.y + 1.2f, player_pos.z - 0.4f});
        }

        auto translate_balloon_to_player_pos(const game::Coordinate &player_pos) noexcept
            -> Affine_Transformation_Matrix
        {
                return balloon_would_be_outside_screen(player_pos) ? translate_balloon_besides(player_pos)
                                                                   : translate_balloon_above(player_pos);
        }

        auto init_qbert_vertices_dead(const Qbert_Player &qbert, Vertex_Pool<Textured_Vertex> &textured_vertices)
        {
                const auto translate_to_player_pos{translate_small_sprite(qbert.position())};
                auto vertices{textured_vertices.acquire_vertices(unit::sprite::VERTICES_COUNT)};

                for (auto i{0U}; i < unit::sprite::VERTICES_COUNT; i++)
                {
                        vertices[i].position =
                            translate_to_player_pos(TILT_Y(TILT_X(unit::sprite::VERTICES[i].position)));
                        vertices[i].texture_coordinate =
                            qbert::TEXTURES.at(Qbert_Player::Animation_Frame::STANDING_LEFT_DOWN)[i];
                }

                const auto translate_balloon{translate_balloon_to_player_pos(qbert.position())};

                auto balloon_vertices{textured_vertices.acquire_vertices(unit::balloon_sprite::VERTICES_COUNT)};
                for (auto i{0U}; i < unit::balloon_sprite::VERTICES_COUNT; i++)
                {
                        balloon_vertices[i].position =
                            translate_balloon(TILT_Y(TILT_X(unit::balloon_sprite::VERTICES[i].position)));
                        balloon_vertices[i].texture_coordinate = BALLOON_TEX_COORDS[i];
                }
        }

        auto init_qbert_vertices_according_to_animation_frame(const Qbert_Player &qbert,
                                                              Vertex_Pool<Textured_Vertex> &textured_vertices)
        {
                const auto translate_to_player_pos{translate_small_sprite(qbert.position())};
                auto vertices{textured_vertices.acquire_vertices(unit::sprite::VERTICES_COUNT)};
                for (auto i{0U}; i < unit::sprite::VERTICES_COUNT; i++)
                {
                        vertices[i].position =
                            translate_to_player_pos(TILT_Y(TILT_X(unit::sprite::VERTICES[i].position)));
                        vertices[i].texture_coordinate = qbert::TEXTURES.at(qbert.animation_frame())[i];
                }
        }

        auto init_qbert_vertices(const Qbert_Player &qbert, Vertex_Pool<Textured_Vertex> &textured_vertices) -> void
        {
                switch (qbert.animation_frame())
                {
                        case Qbert_Player::Animation_Frame::DEAD:
                                init_qbert_vertices_dead(qbert, textured_vertices);
                                break;
                        case Qbert_Player::Animation_Frame::CROUCHING_LEFT_DOWN:
                        case Qbert_Player::Animation_Frame::CROUCHING_LEFT_UP:
                        case Qbert_Player::Animation_Frame::CROUCHING_RIGHT_DOWN:
                        case Qbert_Player::Animation_Frame::CROUCHING_RIGHT_UP:
                        case Qbert_Player::Animation_Frame::STANDING_LEFT_DOWN:
                        case Qbert_Player::Animation_Frame::STANDING_LEFT_UP:
                        case Qbert_Player::Animation_Frame::STANDING_RIGHT_DOWN:
                        case Qbert_Player::Animation_Frame::STANDING_RIGHT_UP:
                                init_qbert_vertices_according_to_animation_frame(qbert, textured_vertices);
                                break;
                }
        }

        /*!
         * Supposed to let texture look more pixelated. I want each texture pixel to be mapped to one screen pixle. This
         * is actually hard to achieve because of perspective etc.
         */
        constexpr auto TALL_SPRITE_SCALE{0.85f};

        constexpr auto TALL_SPRITE_Y_CORRECT{unit::cube::HALF_LENGTH};

        auto translate_tall_sprite(const game::Coordinate &pos) noexcept -> Affine_Transformation_Matrix
        {
                return Affine_Transformation_Matrix{{
                    {TALL_SPRITE_SCALE, 0.0f, 0.0f, pos.x * screen::SCALING_FACTOR},
                    {0.0f, TALL_SPRITE_SCALE, 0.0f,
                     pos.y * screen::SCALING_FACTOR + screen::Y_OFFSET + TALL_SPRITE_Y_CORRECT},
                    {0.0f, 0.0f, TALL_SPRITE_SCALE, (pos.z - 1.0f) * screen::SCALING_FACTOR},
                }};
        }

        auto init_coily_vertices(const Coily &coily, Vertex_Pool<Textured_Vertex> &textured_vertices) -> void
        {
                const auto translate_to_coily_pos{translate_tall_sprite(coily.position())};
                auto vertices{textured_vertices.acquire_vertices(unit::tall_sprite::VERTICES_COUNT)};
                for (auto i{0U}; i < unit::tall_sprite::VERTICES_COUNT; i++)
                {
                        vertices[i].position =
                            translate_to_coily_pos(TILT_Y(TILT_X(unit::tall_sprite::VERTICES[i].position)));
                        vertices[i].texture_coordinate = COILY_TEXTURES.at(coily.animation_frame())[i];
                }
        }

} // namespace qbert::sprite_to_scene
