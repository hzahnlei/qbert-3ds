#include "cube.hpp"

namespace qbert
{

        Cube::Cube(const game::Coordinate &position, const bool touched) : m_position{position}, m_touched{touched}
        {
        }

        auto Cube::touch() noexcept -> bool
        {
                if (m_touched)
                {
                        return false;
                }
                else
                {
                        m_touched = true;
                        return true;
                }
        }

        auto Cube::untouch() noexcept -> bool
        {
                if (m_touched)
                {
                        m_touched = false;
                        return true;
                }
                else
                {
                        return false;
                }
        }

        auto Cube::operator==(const Cube &other) const noexcept -> bool
        {
                return (this->m_position == other.m_position) && (this->m_touched == other.m_touched);
        }

        auto Cube::covers(const game::Coordinate &player_position) const noexcept -> bool
        {
                return (player_position.x >= m_position.x) && (player_position.x < m_position.x + 1.0f) &&
                       (player_position.y > m_position.y - 1.0f) && (player_position.y <= m_position.y) &&
                       (player_position.z >= m_position.z) && (player_position.z < m_position.z + 1.0f);
        }

        auto Cube::is_on_or_above(const game::Coordinate &position) const noexcept -> bool
        {
                return ((position.x >= m_position.x) && (position.x < m_position.x + 1.0f)) &&
                       (position.y >= m_position.y) &&
                       ((position.z >= m_position.z) && (position.z < m_position.z + 1.0f));
        }

} // namespace qbert
