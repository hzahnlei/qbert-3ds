/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

#pragma once

//---- C headers
//---- C++ headers
#include <string>
//---- 3rd party headers
//---- Project headers
#include "base_exception.hpp"

namespace qbert
{

        using std::size_t;
        using std::string;

        class Assertion_Violation final : public Base_Exception
        {
                //-------------------------------------------------------------------------------------------------------------
                // General/static declarations
                //-------------------------------------------------------------------------------------------------------------
            public:
                using File_Name = string;
                using Line_Number = size_t;

                //-------------------------------------------------------------------------------------------------------------
                // Data elements
                //-------------------------------------------------------------------------------------------------------------
            private:
                const File_Name m_file;
                const Line_Number m_line;

                //-------------------------------------------------------------------------------------------------------------
                // Construction/destruction
                //-------------------------------------------------------------------------------------------------------------
            public:
                Assertion_Violation() = delete;
                Assertion_Violation(const File_Name &, const Line_Number, const Message &);
                explicit Assertion_Violation(const Assertion_Violation &) = delete;

                //-------------------------------------------------------------------------------------------------------------
                // Public interface
                //-------------------------------------------------------------------------------------------------------------
            public:
                auto operator=(const Assertion_Violation &) -> Assertion_Violation & = delete;
                auto operator=(Assertion_Violation &&) -> Assertion_Violation & = delete;

                auto pretty_message() const -> Message override;

                [[nodiscard]] auto file() const noexcept -> const File_Name &;
                [[nodiscard]] auto line() const noexcept -> Line_Number;

                //-------------------------------------------------------------------------------------------------------------
                // Internal helper methods
                //-------------------------------------------------------------------------------------------------------------
        };

} // namespace qbert

#ifdef NDEBUG
#define DEV_ASSERT(condition, message) ((void)0)
#else
#define DEV_ASSERT(condition, message)                                                                                 \
        if (!(condition))                                                                                              \
        {                                                                                                              \
                throw qbert::Assertion_Violation{__FILE__, __LINE__, message};                                         \
        }
#endif
