#
# Q*bert for Nintendo 3DS
#
# (c) 2021 Holger Zahnleiter, All rights reserved
#

set (LIB_SOURCE_DIR ${PROJECT_SOURCE_DIR}/product)

include_directories ("${CMAKE_SOURCE_DIR}")

add_library (game_logic
    abstract_character.cpp
    abstract_playable_mode.cpp
    affine_transformation_matrix.cpp
    assertion.cpp
    attract_mode.cpp
    base_exception.cpp
    coily.cpp
    controller.cpp
    cube.cpp
    disc.cpp
    disc_to_scene_mapper.cpp
    game_coordinate.cpp
    game_mode.cpp
    game_model_delegating_impl.cpp
    model_to_scene_mapper.cpp
    overlay_to_scene_mapper.cpp
    playfield_to_scene_mapper.cpp
    qbert_player.cpp
    sprite_to_scene_mapper.cpp
    transformations.cpp
    triangular_playfield.cpp
)

#------------------------------------------------------------------------------
# Coverage related
#------------------------------------------------------------------------------
target_compile_options (game_logic PUBLIC -Wall -pedantic -Wextra -Werror)
target_link_libraries (game_logic PUBLIC coverage_config)
