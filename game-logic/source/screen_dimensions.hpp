#pragma once

namespace qbert::screen
{

        namespace top
        {
                static constexpr auto WIDTH{400U};
                static constexpr auto HEIGHT{240U};
        } // namespace top

        /*!
         * Offset to have playfield and game objects centered on screen.
         */
        static constexpr auto Y_OFFSET{1.0f};

        /*!
         * Screen coordinate = Game coordinate * factor (+ offset)
         */
        static constexpr auto SCALING_FACTOR{0.2f};

} // namespace qbert::screen
