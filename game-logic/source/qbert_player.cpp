/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

//---- Counterpart header
#include "qbert_player.hpp"
//---- C headers
//---- C++ headers
//---- 3rd party headers
//---- Project headers
#include "assertion.hpp"

namespace qbert
{

        constexpr int8_t LEFT_THERSHOLD{-50};
        constexpr int8_t RIGHT_THERSHOLD{50};
        constexpr int8_t DOWN_THERSHOLD{-50};
        constexpr int8_t UP_THERSHOLD{50};

        auto Qbert_Player::desired_move(const Player_Input &player_input) noexcept -> Desired_Move
        {
                if (player_input.c_pad.dx <= LEFT_THERSHOLD && player_input.c_pad.dy <= DOWN_THERSHOLD)
                {
                        return Desired_Move::LEFT_DOWN;
                }
                else if (player_input.c_pad.dx <= LEFT_THERSHOLD && player_input.c_pad.dy >= UP_THERSHOLD)
                {
                        return Desired_Move::LEFT_UP;
                }
                else if (player_input.c_pad.dx >= RIGHT_THERSHOLD && player_input.c_pad.dy <= DOWN_THERSHOLD)
                {
                        return Desired_Move::RIGHT_DOWN;
                }
                else if (player_input.c_pad.dx >= RIGHT_THERSHOLD && player_input.c_pad.dy >= UP_THERSHOLD)
                {
                        return Desired_Move::RIGHT_UP;
                }
                else
                {
                        return Desired_Move::NONE;
                }
        }

        Qbert_Player::Qbert_Player(const game::Coordinate &position)
            : Abstract_Character{position},
              m_behaviors{
                  {State::WAITING, [this](auto &, const auto &input) { wait(input); }},

                  {State::FALLING, [this](auto &game, const auto &) { fall(game); }},

                  {State::JUMPING_LEFT_DOWN, [this](auto &game, const auto &) { jump_left_down(game); }},
                  {State::JUMPING_LEFT_UP, [this](auto &game, const auto &) { jump_left_up(game); }},
                  {State::JUMPING_RIGHT_DOWN, [this](auto &game, const auto &) { jump_right_down(game); }},
                  {State::JUMPING_RIGHT_UP, [this](auto &game, const auto &) { jump_right_up(game); }},

                  {State::START_JUMPING_LEFT_DOWN, [this](auto &, const auto &) { start_jumping_left_down(); }},
                  {State::START_JUMPING_LEFT_UP, [this](auto &, const auto &) { start_jumping_left_up(); }},
                  {State::START_JUMPING_RIGHT_DOWN, [this](auto &, const auto &) { start_jumping_right_down(); }},
                  {State::START_JUMPING_RIGHT_UP, [this](auto &, const auto &) { start_jumping_right_up(); }},

                  {State::DYING, [this](auto &game, const auto &) { dying(game); }},

                  {State::DEAD, [this](auto &, const auto &) { /* Intentionally left blank */ }},

                  {State::SURFING_ON_DISC, [this](auto &, const auto &) { surfing_on_disc(); }},
              }
        {
        }

        auto Qbert_Player::interact(Game_Model &game, const Player_Input &player_input) -> void
        {
                const auto behave{m_behaviors.at(m_state)};
                DEV_ASSERT(nullptr != behave, "Unknown behavior");
                behave(game, player_input);
                Abstract_Character::interact(game);
        }

        auto Qbert_Player::animation_frame() const noexcept -> Animation_Frame
        {
                if (State::DEAD == m_state)
                {
                        return Animation_Frame::DEAD;
                }
                else if (State::WAITING == m_state)
                {
                        switch (m_orientation)
                        {
                                case Orientation::LEFT_DOWN:
                                        return Animation_Frame::CROUCHING_LEFT_DOWN;
                                case Orientation::LEFT_UP:
                                        return Animation_Frame::CROUCHING_LEFT_UP;
                                case Orientation::RIGHT_DOWN:
                                        return Animation_Frame::CROUCHING_RIGHT_DOWN;
                                case Orientation::RIGHT_UP:
                                        return Animation_Frame::CROUCHING_RIGHT_UP;
                        }
                }
                else
                {
                        switch (m_orientation)
                        {
                                case Orientation::LEFT_DOWN:
                                        return Animation_Frame::STANDING_LEFT_DOWN;
                                case Orientation::LEFT_UP:
                                        return Animation_Frame::STANDING_LEFT_UP;
                                case Orientation::RIGHT_DOWN:
                                        return Animation_Frame::STANDING_RIGHT_DOWN;
                                case Orientation::RIGHT_UP:
                                        return Animation_Frame::STANDING_RIGHT_UP;
                        }
                }
                return Animation_Frame::CROUCHING_LEFT_DOWN;
        }

        auto Qbert_Player::is_on_or_above(const game::Coordinate &position) const noexcept -> bool
        {
                // TODO test
                return ((position.x >= m_position.x) && (position.x < m_position.x + 1.0f)) &&
                       (position.y >= m_position.y) &&
                       ((position.z >= m_position.z) && (position.z < m_position.z + 1.0f));
        }

        auto Qbert_Player::covers(const game::Coordinate &player_position) const noexcept -> bool
        {
                // TODO test
                return (player_position.x >= m_position.x) && (player_position.x < m_position.x + 1.0f) &&
                       (player_position.y >= m_position.y - 0.01f) && (player_position.y <= m_position.y + 0.01f) &&
                       (player_position.z >= m_position.z) && (player_position.z < m_position.z + 1.0f);
        }

        //---- Behavior: Wait for user Input ----

        auto Qbert_Player::wait(const Player_Input &player_input) -> void
        {
                switch (desired_move(player_input))
                {
                        case Desired_Move::LEFT_DOWN:
                                m_state = State::START_JUMPING_LEFT_DOWN;
                                break;
                        case Desired_Move::LEFT_UP:
                                m_state = State::START_JUMPING_LEFT_UP;
                                break;
                        case Desired_Move::NONE:
                                break;
                        case Desired_Move::RIGHT_DOWN:
                                m_state = State::START_JUMPING_RIGHT_DOWN;
                                break;
                        case Desired_Move::RIGHT_UP:
                                m_state = State::START_JUMPING_RIGHT_UP;
                                break;
                }
        }

        //---- Behavior: Fall down ----

        auto Qbert_Player::fall(Game_Model &game) -> void
        {
                if (const auto cube{has_landed_on_cube(game)})
                {
                        stop_falling();
                        change_cube_color(game, *cube);
                }
                else if (const auto disc{has_landed_on_disc(game)})
                {
                        surf_on_disc(game, *disc);
                }
                else if (is_falling_off_playfield(game))
                {
                        die();
                }
                else
                {
                        continue_falling();
                }
        }

        //---- Behavior: Jumping ----

        auto Qbert_Player::jump_left_down(Game_Model &) -> void // FIXME remove param
        {
                if (m_animation_counter < JUMP_DOWN_FRAME_COUNT)
                {
                        m_position.y += VERTICAL_SPEED;
                        m_position.z += VERTICAL_SPEED;
                }
                else
                {
                        start_falling();
                }
        }

        auto Qbert_Player::jump_left_up(Game_Model &) -> void // FIXME remove param
        {
                if (m_animation_counter < JUMP_UP_FRAME_COUNT)
                {
                        m_position.y += VERTICAL_SPEED;
                        m_position.x -= HORIZONTAL_SPEED;
                }
                else
                {
                        start_falling();
                }
        }

        auto Qbert_Player::jump_right_down(Game_Model &) -> void // FIXME remove param
        {
                if (m_animation_counter < JUMP_DOWN_FRAME_COUNT)
                {
                        m_position.y += VERTICAL_SPEED;
                        m_position.x += VERTICAL_SPEED;
                }
                else
                {
                        start_falling();
                }
        }

        auto Qbert_Player::jump_right_up(Game_Model &) -> void // FIXME remove param
        {
                if (m_animation_counter < JUMP_UP_FRAME_COUNT)
                {
                        m_position.y += VERTICAL_SPEED;
                        m_position.z -= HORIZONTAL_SPEED;
                }
                else
                {
                        start_falling();
                }
        }

        //---- Behavior: Start jumping ----

        auto Qbert_Player::start_jumping_left_down() -> void
        {
                m_animation_counter = 0;
                m_state = State::JUMPING_LEFT_DOWN;
                m_orientation = Orientation::LEFT_DOWN;
        }

        auto Qbert_Player::start_jumping_left_up() -> void
        {
                m_animation_counter = 0;
                m_state = State::JUMPING_LEFT_UP;
                m_orientation = Orientation::LEFT_UP;
        }

        auto Qbert_Player::start_jumping_right_down() -> void
        {
                m_animation_counter = 0;
                m_state = State::JUMPING_RIGHT_DOWN;
                m_orientation = Orientation::RIGHT_DOWN;
        }

        auto Qbert_Player::start_jumping_right_up() -> void
        {
                m_animation_counter = 0;
                m_state = State::JUMPING_RIGHT_UP;
                m_orientation = Orientation::RIGHT_UP;
        }

        //---- Behavior: Dying ----

        auto Qbert_Player::die() -> void
        {
                m_animation_counter = 0;
                m_state = State::DYING;
        }

        auto Qbert_Player::dying(Game_Model &game) -> void
        {
                if (m_animation_counter < 25)
                {
                        continue_falling();
                }
                else
                {
                        dead(game);
                }
        }

        auto Qbert_Player::dead(Game_Model &game) -> void
        {
                m_state = State::DEAD;
                level_out_limited_FP_precision(m_position);
                game.player_died();
        }

        //---- Attached to/flying with disc ----

        auto Qbert_Player::surf_on_disc(Game_Model &game, const Disc &disc) -> void
        {
                if (game.attempt_to_attach_player_to(disc))
                {
                        m_state = State::SURFING_ON_DISC;
                        level_out_limited_FP_precision(m_position);
                        m_surfing_on = &disc;
                }
        }

        auto Qbert_Player::surfing_on_disc() -> void
        {
                m_position.x = m_surfing_on->position().x;
                m_position.y = m_surfing_on->position().y;
                m_position.z = m_surfing_on->position().z;
        }

        auto Qbert_Player::jump_off_disc_to_playfield(const Playfield &playfield) -> void
        {
                m_surfing_on = nullptr;
                level_out_limited_FP_precision(m_position);
                if (playfield.is_right_of_playfield(m_position))
                {
                        start_jumping_left_down();
                }
                else
                {
                        start_jumping_right_down();
                }
        }

} // namespace qbert
