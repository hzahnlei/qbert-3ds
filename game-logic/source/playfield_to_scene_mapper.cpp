#include "playfield_to_scene_mapper.hpp"

#include "screen_dimensions.hpp"
#include "unit_cube_vertices.hpp"

namespace qbert::playfield_to_scene
{

        static constexpr auto VERTICE_COUNT_FOR_ON_CUBE_FACE{6U};

        /*!
         * Populates one cube on the playfield by copying a unit cube and moving it to the appropriate location on the
         * playfield.
         */
        inline auto init_vertices_of_single_cube(Colored_Vertex *targets, const Cube &cube)
        {
                const auto cube_pos{cube.position()};
                auto vertex_index{0U};
                for (; vertex_index < VERTICE_COUNT_FOR_ON_CUBE_FACE; vertex_index++)
                {
                        auto &target{targets[vertex_index]};
                        const auto &source{unit::cube::VERTICES[vertex_index]};
                        target.color = cube.has_been_touched() ? Palette::YELLOW : source.color;
                        target.position.x = source.position.x + (unit::cube::SIDE_LENGTH * cube_pos.x);
                        target.position.y =
                            source.position.y + (unit::cube::SIDE_LENGTH * cube_pos.y) + screen::Y_OFFSET;
                        target.position.z = source.position.z + (unit::cube::SIDE_LENGTH * cube_pos.z);
                }
                for (; vertex_index < unit::cube::VERTICES_COUNT; vertex_index++)
                {
                        auto &target{targets[vertex_index]};
                        const auto &source{unit::cube::VERTICES[vertex_index]};
                        target.color = source.color;
                        target.position.x = source.position.x + (unit::cube::SIDE_LENGTH * cube_pos.x);
                        target.position.y =
                            source.position.y + (unit::cube::SIDE_LENGTH * cube_pos.y) + screen::Y_OFFSET;
                        target.position.z = source.position.z + (unit::cube::SIDE_LENGTH * cube_pos.z);
                }
        }

        inline auto init_vertices_of_all_cubes(const vector<Cube> &cubes, Colored_Vertex *vertices) -> void
        {
                auto cube_index{0U};
                for (const auto &cube : cubes)
                {
                        auto vertices_of_curr_cube{&vertices[cube_index * unit::cube::VERTICES_COUNT]};
                        init_vertices_of_single_cube(vertices_of_curr_cube, cube);
                        cube_index++;
                }
        }

        auto init_vertices(const vector<Cube> &cubes, Vertex_Pool<Colored_Vertex> &colored_vertices) -> void
        {
                const auto vertex_count{cubes.size() * unit::cube::VERTICES_COUNT};
                auto vertices{colored_vertices.acquire_vertices(vertex_count)};
                init_vertices_of_all_cubes(cubes, vertices);
        }

} // namespace qbert::playfield_to_scene
