#pragma once

#include <cstdint>

namespace qbert
{

        using std::int16_t;
        using std::uint32_t;

        using Buttons = uint32_t;

        inline static constexpr auto bit(const unsigned char n) -> Buttons
        {
                return 1U << n;
        }

        enum class Button : Buttons
        {
                NONE = 0,
                A = bit(0),
                B = bit(1),
                SELECT = bit(2),
                START = bit(3),
                D_PAD_RIGHT = bit(4),
                D_PAD_LEFT = bit(5),
                D_PAD_UP = bit(6),
                D_PAD_DOWN = bit(7),
                R = bit(8),
                L = bit(9),
                KEY_X = bit(10),
                KEY_Y = bit(11),
                ZL = bit(14),
                ZR = bit(15),
                TOUCH = bit(20),
                C_STICK_RIGHT = bit(24),
                C_STICK_LEFT = bit(25),
                C_STICK_UP = bit(26),
                C_STICK_DOWN = bit(27),
                C_PAD_RIGHT = bit(28),
                C_PAD_LEFT = bit(29),
                C_PAD_UP = bit(30),
                C_PAD_DOWN = bit(31),
                // Generic catch-all directions
                ANY_UP = D_PAD_UP | C_PAD_UP,
                ANY_DOWN = D_PAD_DOWN | C_PAD_DOWN,
                ANY_LEFT = D_PAD_LEFT | C_PAD_LEFT,
                ANY_RIGHT = D_PAD_RIGHT | C_PAD_RIGHT,
        };

        struct Player_Input final
        {
                Buttons pressed;
                Buttons held;
                Buttons released;
                struct C_Pad
                {
                        int16_t dx, dy;
                } c_pad;
                float stereoscopic_3d_slider;

                inline auto is_pressed(const Button) const noexcept -> bool;
                inline auto is_held(const Button) const noexcept -> bool;
                inline auto is_released(const Button) const noexcept -> bool;
        };

        auto Player_Input::is_pressed(const Button button) const noexcept -> bool
        {
                return pressed & static_cast<Buttons>(button);
        }

        auto Player_Input::is_held(const Button button) const noexcept -> bool
        {
                return held & static_cast<Buttons>(button);
        }

        auto Player_Input::is_released(const Button button) const noexcept -> bool
        {
                return released & static_cast<Buttons>(button);
        }

        static constexpr Player_Input NO_PLAYER_INPUT{.pressed = static_cast<Buttons>(Button::NONE),
                                                      .held = static_cast<Buttons>(Button::NONE),
                                                      .released = static_cast<Buttons>(Button::NONE),
                                                      .c_pad =
                                                          {
                                                              .dx = 0,
                                                              .dy = 0,
                                                          },
                                                      .stereoscopic_3d_slider = 0.0f};

} // namespace qbert