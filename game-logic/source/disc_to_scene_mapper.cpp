#include "disc_to_scene_mapper.hpp"

#include "screen_dimensions.hpp"
#include "unit_disc_vertices.hpp"
#include <cmath>

namespace qbert::disc_to_scene
{

        auto rotate_and_translate(const Disc &disc) -> Affine_Transformation_Matrix
        {
                const auto rotation_rad{disc.rotation()};
                const auto cos_y{cosf(rotation_rad)};
                const auto sin_y{sinf(rotation_rad)};
                const auto x{disc.position().x * screen::SCALING_FACTOR};
                const auto y{disc.position().y * screen::SCALING_FACTOR + screen::Y_OFFSET + DISC_Y_ALIGN};
                const auto z{disc.position().z * screen::SCALING_FACTOR};
                return Affine_Transformation_Matrix{.row = {
                                                        {.x = cos_y, .y = 0.0f, .z = sin_y, .w = x},
                                                        {.x = 0.0f, .y = 1.0f, .z = 0.0f, .w = y},
                                                        {.x = -sin_y, .y = 0.0f, .z = cos_y, .w = z},
                                                    }};
        }

        inline auto init_vertices_of_single_disc(const Disc &disc, Vertex_Pool<Colored_Vertex> &colored_vertices)
        {
                auto vertices{colored_vertices.acquire_vertices(unit::disc::VERTICES_COUNT)};
                const auto transform{rotate_and_translate(disc)};
                for (auto i{0U}; i < unit::disc::VERTICES_COUNT; i++)
                {
                        vertices[i].color = unit::disc::VERTICES[i].color;
                        vertices[i].position = transform(unit::disc::VERTICES[i].position);
                }
        }

        auto init_vertices(const vector<Disc> &discs, Vertex_Pool<Colored_Vertex> &colored_vertices) -> void
        {
                for (auto const &disc : discs)
                {
                        init_vertices_of_single_disc(disc, colored_vertices);
                }
        }

} // namespace qbert::disc_to_scene