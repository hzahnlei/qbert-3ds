#pragma once

#include "affine_transformation_matrix.hpp"
#include "colored_vertex.hpp"
#include "disc_to_scene_mapper.hpp"
#include "game_model_delegating_impl.hpp"
#include "texture_mappings.hpp"
#include "textured_colored_vertex.hpp"
#include "textured_vertex.hpp"
#include "vertex_pool.hpp"
#include <string>

namespace qbert
{

        using std::string;

        class Model_to_Scene_Mapper final
        {

            private:
                Game_Model &m_game;
                Vertex_Pool<Colored_Vertex> &m_colored_vertices;
                Vertex_Pool<Textured_Vertex> &m_textured_vertices;
                Vertex_Pool<Textured_Colored_Vertex> &m_overlay_vertices;

            public:
                Model_to_Scene_Mapper(Game_Model &, Vertex_Pool<Colored_Vertex> &, Vertex_Pool<Textured_Vertex> &,
                                      Vertex_Pool<Textured_Colored_Vertex> &);

            public:
                auto update_scene() -> void;

            private:
                //--- init scene ---
                auto init_scene() -> void;
        };

} // namespace qbert