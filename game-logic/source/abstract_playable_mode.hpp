#pragma once

#include "coily.hpp"
#include "disc.hpp"
#include "game_model.hpp"
#include "position.hpp"
#include "qbert_player.hpp"
#include <vector>

namespace qbert
{

        using std::vector;

        class Abstract_Playable_Mode : public Game_Model
        {
            protected:
                Score m_score{0U};
                Score m_hi_score{0U};
                Triangular_Playfield m_playfield;
                vector<Disc> m_discs{
                    Disc{game::Coordinate{+3.0f, -3.0f, -1.0f - Playfield::GAP}},
                    Disc{game::Coordinate{-1.0f - Playfield::GAP, -3.0f, +3.0f}},
                };
                Qbert_Player m_player{game::Coordinate{0.0f, 2.0f, 0.0f}};
                Player_Count m_player_count{3};
                Coily m_coily{game::Coordinate{1.0f, -4.0f, 5.0f}};

            public:
                [[nodiscard]] inline auto scene_angle_x() const noexcept -> Scene_Angle final override;
                [[nodiscard]] inline auto scene_angle_z() const noexcept -> Scene_Angle final override;

                [[nodiscard]] inline auto playfield() const noexcept -> const Playfield & final override;

                [[nodiscard]] inline auto discs() const noexcept -> const vector<Disc> & final override;
                [[nodiscard]] auto disc_at(const game::Coordinate &) const -> const Disc * final override;
                auto attempt_to_attach_player_to(const Disc &) -> bool final override;
                auto have_qbert_jump_off_disc_to_playfield() -> void final override;

                [[nodiscard]] inline auto player() const noexcept -> const Qbert_Player & final override;
                [[nodiscard]] inline auto player_count() const noexcept -> Player_Count final override;
                auto player_died() noexcept -> void final override;

                [[nodiscard]] inline auto coily() const noexcept -> const Coily & final override;

                [[nodiscard]] inline auto score() const noexcept -> Score final override;
                [[nodiscard]] inline auto hi_score() const noexcept -> Score final override;

                [[nodiscard]] inline auto level_number() const noexcept -> Level_Number final override;
                [[nodiscard]] inline auto round_number() const noexcept -> Round_Number final override;

                auto touch(const Cube &) -> bool final override;

                [[nodiscard]] auto is_within_playfield(const game::Coordinate &) const -> bool override;

                virtual auto update(const Player_Input &) -> void override;

                auto set_hi_score(const Score) -> void;

            private:
                [[nodiscard]] inline auto disc_landing_position(const Disc &) const -> const game::Coordinate &;
        };

        auto Abstract_Playable_Mode::scene_angle_x() const noexcept -> Scene_Angle
        {
                return DEFAULT_SCENE_ROTATION_X;
        }

        auto Abstract_Playable_Mode::scene_angle_z() const noexcept -> Scene_Angle
        {
                return DEFAULT_SCENE_ROTATION_Z;
        }

        auto Abstract_Playable_Mode::playfield() const noexcept -> const Playfield &
        {
                return m_playfield;
        }

        auto Abstract_Playable_Mode::discs() const noexcept -> const vector<Disc> &
        {
                return m_discs;
        }

        auto Abstract_Playable_Mode::player() const noexcept -> const Qbert_Player &
        {
                return m_player;
        }

        auto Abstract_Playable_Mode::player_count() const noexcept -> Player_Count
        {
                return m_player_count;
        }

        auto Abstract_Playable_Mode::coily() const noexcept -> const Coily &
        {
                return m_coily;
        }

        auto Abstract_Playable_Mode::score() const noexcept -> Score
        {
                return m_score;
        }

        auto Abstract_Playable_Mode::hi_score() const noexcept -> Score
        {
                return m_hi_score;
        }

        auto Abstract_Playable_Mode::level_number() const noexcept -> Level_Number
        {
                return 1;
        }

        auto Abstract_Playable_Mode::round_number() const noexcept -> Round_Number
        {
                return 1;
        }

        auto Abstract_Playable_Mode::disc_landing_position(const Disc &disc) const -> const game::Coordinate &
        {
                return playfield().disc_landing_position(disc.position());
        }

} // namespace qbert