#include "attract_mode.hpp"

namespace qbert
{

        auto Attract_Mode::rotate_scene_clockwise() -> void
        {
                // Intentionally left blank
        }

        auto Attract_Mode::rotate_scene_counter_clockwise() -> void
        {
                // Intentionally left blank
        }

        auto Attract_Mode::reset_scene_rotation() -> void
        {
                // Intentionally left blank
        }

        auto Attract_Mode::update(const Player_Input &) -> void
        {
                const Player_Input ai_player_input{NO_PLAYER_INPUT}; // TODO determine AI user input
                Abstract_Playable_Mode::update(ai_player_input);
        }

} // namespace qbert
