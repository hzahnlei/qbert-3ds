#include "transformations.hpp"

namespace qbert::transform
{

        auto rotation_x(const Degrees degrees) -> Affine_Transformation_Matrix
        {
                const auto sin_val{sinf(radians(degrees))};
                const auto cos_val{cosf(radians(degrees))};
                return Affine_Transformation_Matrix{{
                    {1.0f, 0.0f, 0.0f, 0.0f},
                    {0.0f, cos_val, -sin_val, 0.0f},
                    {0.0f, sin_val, cos_val, 0.0f},
                }};
        }

        auto rotation_y(const Degrees degrees) -> Affine_Transformation_Matrix
        {
                const auto sin_val{sinf(radians(degrees))};
                const auto cos_val{cosf(radians(degrees))};
                return Affine_Transformation_Matrix{{
                    {cos_val, 0.0f, sin_val, 0.0f},
                    {0.0f, 1.0f, 0.0f, 0.0f},
                    {-sin_val, 0.0f, cos_val, 0.0f},
                }};
        }

        auto rotation_z(const Degrees degrees) -> Affine_Transformation_Matrix
        {
                const auto sin_val{sinf(radians(degrees))};
                const auto cos_val{cosf(radians(degrees))};
                return Affine_Transformation_Matrix{{
                    {cos_val, -sin_val, 0.0f, 0.0f},
                    {sin_val, cos_val, 0.0f, 0.0f},
                    {0.0f, 0.0f, 1.0f, 0.0f},
                }};
        }

        auto translation(const Position &position) -> Affine_Transformation_Matrix
        {
                return Affine_Transformation_Matrix{{
                    {1.0f, 0.0f, 0.0f, position.x},
                    {0.0f, 1.0f, 0.0f, position.y},
                    {0.0f, 0.0f, 1.0f, position.z},
                }};
        }

} // namespace qbert::transform