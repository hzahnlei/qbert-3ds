#pragma once

#include "affine_transformation_matrix.hpp"
#include "coily.hpp"
#include "game_coordinate.hpp"
#include "qbert_player.hpp"
#include "textured_vertex.hpp"
#include "vertex_pool.hpp"

namespace qbert::sprite_to_scene
{

        [[nodiscard]] auto translate_small_sprite(const game::Coordinate &) noexcept -> Affine_Transformation_Matrix;

        [[nodiscard]] auto translate_balloon_to_player_pos(const game::Coordinate &) noexcept
            -> Affine_Transformation_Matrix;

        auto init_qbert_vertices(const Qbert_Player &, Vertex_Pool<Textured_Vertex> &) -> void;

        [[nodiscard]] auto translate_tall_sprite(const game::Coordinate &) noexcept -> Affine_Transformation_Matrix;

        auto init_coily_vertices(const Coily &, Vertex_Pool<Textured_Vertex> &) -> void;

} // namespace qbert::sprite_to_scene
