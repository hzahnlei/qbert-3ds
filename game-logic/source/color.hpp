/*!
 * Q*bert for Nintendo 3DS
 *
 * (c) 2021 Holger Zahnleiter, All rights reserved
 */

#pragma once

namespace qbert
{
        struct Color final
        {
                float r, g, b, a;
        };

        /*!
         * I am trying to limit colors so that a restricted 8 bit appeal is maintained.
         */
        struct Palette final
        {
                static constexpr Color TRANSPARENT{0.0f, 0.0f, 0.0f, 0.0f};

                static constexpr Color RED{1.0f, 0.0f, 0.0f, 1.0f};
                static constexpr Color GREEN{0.0f, 0.8125f, 0.0f, 1.0f};
                static constexpr Color BLUE{0.0f, 0.0f, 1.0f, 1.0f};
                static constexpr Color YELLOW{1.0f, 1.0f, 0.0f, 1.0f};
                static constexpr Color MAGENTA{1.0f, 0.0f, 1.0f, 1.0f};
                static constexpr Color CYAN{0.0f, 1.0f, 1.0f, 1.0f};

                static constexpr Color ORANGE{1.0f, 0.6f, 0.0f, 1.0f};
                static constexpr Color PURPLE{0.5f, 0.0f, 0.8f, 1.0f};
                static constexpr Color BROWN{0.5f, 0.3f, 0.0f, 1.0f};

                static constexpr Color BLACK{0.0f, 0.0f, 0.0f, 1.0f};
                static constexpr Color DARK_GREY{0.25f, 0.25f, 0.25f, 1.0f};
                static constexpr Color MEDIUM_GREY{0.5f, 0.5f, 0.5f, 1.0f};
                static constexpr Color LIGHT_GREY{0.75f, 0.75f, 0.75f, 1.0f};
                static constexpr Color WHITE{1.0f, 1.0f, 1.0f, 1.0f};

                static constexpr Color DARK_RED{0.5f, 0.0f, 0.0f, 1.0f};
                static constexpr Color DARK_GREEN{0.0f, 0.5f, 0.0f, 1.0f};
                static constexpr Color LIGHT_BLUE{0.33f, 0.33f, 1.0f, 1.0f};
                static constexpr Color DARK_BLUE{0.0f, 0.0f, 0.66f, 1.0f};
                static constexpr Color LIGHT_YELLOW{1.0f, 1.0f, 0.5f, 1.0f};
                static constexpr Color DARK_CYAN{0.0f, 0.66f, 0.66f, 1.0f};

                static constexpr Color PINK{1.0f, 0.5f, 0.5f, 1.0f};
        };

} // namespace qbert