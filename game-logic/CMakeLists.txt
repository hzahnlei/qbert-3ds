#
# Q*bert for Nintendo 3DS
#
# (c) 2021 Holger Zahnleiter, All rights reserved
#

cmake_minimum_required (VERSION 3.19.4)

set (CMAKE_CXX_STANDARD 20)
set (CMAKE_CXX_STANDARD_REQUIRED ON)
# add_definitions ("-std=c++20")
set (CMAKE_CXX_EXTENSIONS ON)

project (game_logic LANGUAGES CXX)

if (WIN32)
    add_compile_options (/W4 /WX)
else ()
    add_compile_options (-Wall -Wextra -pedantic -Werror)
endif ()


#------------------------------------------------------------------------------
# Coverage/debug build vs. release build
#------------------------------------------------------------------------------
add_library (coverage_config INTERFACE)
add_library (release_config INTERFACE)

option (CODE_COVERAGE "Enable coverage reporting" OFF)
if (CODE_COVERAGE AND CMAKE_CXX_COMPILER_ID MATCHES "GNU|Clang")
    if (CMAKE_CXX_COMPILER_ID MATCHES "GNU|Clang")
        target_compile_options (coverage_config INTERFACE -O0 -g --coverage -fno-inline -fno-inline-small-functions -fno-default-inline)
        # Does not help covering header files:
        #target_compile_options (coverage_config INTERFACE -O0 -g --coverage -fno-inline -fno-inline-small-functions -fno-default-inline -fprofile-arcs -ftest-coverage)
        if (CMAKE_VERSION VERSION_GREATER_EQUAL 3.19.4)
            target_link_options (coverage_config INTERFACE --coverage)
        else ()
            target_link_libraries (coverage_config INTERFACE --coverage)
        endif ()
    endif ()
else ()
    if (CMAKE_CXX_COMPILER_ID MATCHES "GNU|Clang")
        target_compile_options (release_config INTERFACE -Os)
    endif ()
endif ()

add_subdirectory (source)

# Usually I am using Conan to manage my dependencies. However, no 3DS packages
# are provided. Therefore I am retreating to locally installed Catch2 on my
# development machine and in the Docker image for CI builds.
find_package (Catch2)
enable_testing ()
include (CTest)
add_subdirectory (test)

target_include_directories (game_logic PUBLIC
                            "${PROJECT_BINARY_DIR}"
                            "${PROJECT_SOURCE_DIR}/source"
)

set_property (TARGET game_logic PROPERTY CXX_STANDARD 20)
