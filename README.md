![banner](doc/banner.png "banner")

# 3ds-q-bert

A (not screen accurate) remake of the classic arcade game Q*bert for Nintendo 3DS consoles.
I am trying to make it appear three dimensional with all the depth the 3DS' stereoscopic display can provide while preserving the eight bit look and feel.

## Documentation

*  [Organisation of code](doc/organisation_of_code.md)
*  [How the game engine works](doc/game_engine.md)
*  [Rendering vertices and texturing](doc/rendering_and_texturing.md)

## Building the executable

### Prerequisites

   *  Build tools: `make` and `cmake`
   *  Image converter: ImmageMagick's `convert`, for converting images to 24 bit B-G-R
   *  FTP client: `gftp`, FTP tool for uploading executables to 3DS
   *  C++ compiler and 3DS Libraries: devkitARM

### Building and uploading to 3DS

On the commandline invoke `make`.
This will build the executable and upload it to the 3DS via FTP.
(Very likely you have to adjust the 3DS' IP address in the `Makefile`.)

If you just want to build without uploading, type: `make clean prepre build`.

Uploading an existing executable: `make upload`.

## Credits

These are the open source and/or free frameworks, tools and services I am using:

*  devkitPro (devkitARM, libctru, citro2d, citro3d)
*  GCC/Clang
*  Visual Studio Code
*  CMake/GNU Make
*  gftp
*  Clang format
*  GitLab
*  ImageMagick
*  Docker
*  Debian Linux
*  gcov/lcov
*  Catch2
*  And certainly others (I may even not be aware of)

## Disclaimer

Q\*bert was originally developed by Gottlieb in 1982.
Q\*bert is a trademark of Gottlieb.

"Nintendo" and "Nintendo 3DS" are trademarks of Nintendo (see for example https://trademarks.justia.com/850/80/nintendo-85080097.html).

This project is in no way associated with Nintendo nor Gottlieb.
It is a private, free, open source and non-profit project.

![footer](doc/footer.png "footer")
