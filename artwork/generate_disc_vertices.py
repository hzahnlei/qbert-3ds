#!/usr/bin/env python3

#
# Q*bert for Nintendo 3DS
#
# A script that generates the vertices to make up the colored discs Q*bert can use to lure Coily into the abyss.
# The vertices are described as C++ code to be pasted into the application.
#
# (c) 2021 Holger Zahnleiter, All rights reserved
#

from math import sqrt, pi, sin, cos

DISC_COLOR_LUT = ["GREEN", "RED", "BLUE", "YELLOW"]
RIM_COLOR_LUT = ["DARK_GREEN", "DARK_RED", "DARK_BLUE", "BROWN"]
SEGMENT_COUNT_PER_COLOR = 3
SEGMENT_COUNT = len(DISC_COLOR_LUT) * SEGMENT_COUNT_PER_COLOR
DEGREES_PER_SEGMENT = (2*pi)/SEGMENT_COUNT
DISC_RADIUS = 0.1
DISC_HIGHT_RATION = 0.3


def print_header():
    print("/*!")
    print(" * Q*bert for Nintendo 3DS")
    print(" *")
    print(" * (c) 2021 Holger Zahnleiter, All rights reserved")
    print(" */")
    print()
    print("#pragma once")
    print()
    print("#include \"colored_vertex.hpp\"")
    print()
    print("namespace qbert::unit::disc")
    print("{")
    print()
    print(f"\tconstexpr auto RADIUS{{{DISC_RADIUS}f}};")
    print(f"\tconstexpr auto HIGHT{{RADIUS * {DISC_HIGHT_RATION}f}};")
    print()


def print_vertices_for_single_segment(i, segment):
    x_1 = 0.0
    y_1 = 0.0
    z_1 = 0.0
    rad_2 = DEGREES_PER_SEGMENT*segment
    x_2 = sin(rad_2)
    y_2 = 0.0
    z_2 = cos(rad_2)
    rad_3 = DEGREES_PER_SEGMENT*(segment+1)
    x_3 = sin(rad_3)
    y_3 = 0.0
    z_3 = cos(rad_3)
    disc_color = DISC_COLOR_LUT[int(
        segment/(SEGMENT_COUNT/len(DISC_COLOR_LUT)))]
    rim_color = RIM_COLOR_LUT[int(
        segment/(SEGMENT_COUNT/len(RIM_COLOR_LUT)))]
    print(f"\t\t//---- Segment #{i+1} ----")
    print(f"\t\t//---- DISC ----")
    print(
        f"\t\t{{{{{x_1:.4f}f*RADIUS, {y_1:.4f}f, {z_1:.4f}f*RADIUS}}, Palette::{disc_color}}},")
    print(
        f"\t\t{{{{{x_2:.4f}f*RADIUS, {y_2:.4f}f, {z_2:.4f}f*RADIUS}}, Palette::{disc_color}}},")
    print(
        f"\t\t{{{{{x_3:.4f}f*RADIUS, {y_3:.4f}f, {z_3:.4f}f*RADIUS}}, Palette::{disc_color}}},")
    print(f"\t\t//---- RIM ----")
    print(
        f"\t\t{{{{{x_2:.4f}f*RADIUS, {y_2:.4f}f, {z_2:.4f}f*RADIUS}}, Palette::{rim_color}}},")
    print(
        f"\t\t{{{{{x_2:.4f}f*RADIUS, {y_2:.4f}f-HIGHT, {z_2:.4f}f*RADIUS}}, Palette::{rim_color}}},")
    print(
        f"\t\t{{{{{x_3:.4f}f*RADIUS, {y_3:.4f}f, {z_3:.4f}f*RADIUS}}, Palette::{rim_color}}},")
    print(f"\t\t//---- ")
    print(
        f"\t\t{{{{{x_3:.4f}f*RADIUS, {y_1:.4f}f-HIGHT, {z_3:.4f}f*RADIUS}}, Palette::{rim_color}}},")
    print(
        f"\t\t{{{{{x_3:.4f}f*RADIUS, {y_3:.4f}f, {z_3:.4f}f*RADIUS}}, Palette::{rim_color}}},")
    print(
        f"\t\t{{{{{x_2:.4f}f*RADIUS, {y_2:.4f}f-HIGHT, {z_2:.4f}f*RADIUS}}, Palette::{rim_color}}},")


def print_vertices_for_all_segments():
    print("\tconstexpr Colored_Vertex VERTICES[]{")
    for i, segment in enumerate(range(SEGMENT_COUNT)):
        print_vertices_for_single_segment(i, segment)
    print("\t};")
    print()
    print(
        "\tconstexpr auto VERTICES_COUNT{sizeof(VERTICES) / sizeof(VERTICES[0])};")
    print()
    print("\tconstexpr auto MEM_SIZE{sizeof(VERTICES)};")


def print_footer():
    print()
    print("} // namespace qbert::unit::disc")
    print()


print_header()
print_vertices_for_all_segments()
print_footer()
