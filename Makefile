#
# 3DS Template
#
# Holger Zahnleiter, 2021-04-16
#

.PHONY: all clean prepare build upload

# ---- Local build for the targed (3DS) ----
all: clean prepare build upload

clean:
	rm -rf build

prepare:
	cmake -DCMAKE_TOOLCHAIN_FILE=cmake-support/DevkitArm3DS.cmake -S . -B build

build:
	cmake --build build

# ---- Upload to 3DS via FTP ----
upload:
	echo 'binary\n put ./build/3ds_qbert.3dsx 3ds/3ds-q-bert.3dsx\n bye' | gftp --debug --no-login --no-edit 192.168.2.2 5000
